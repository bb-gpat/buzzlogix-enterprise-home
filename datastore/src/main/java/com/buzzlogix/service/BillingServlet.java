/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.service;

import com.google.api.client.util.Strings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.buzzlogix.dao.billing.BillingDaoImpl;
import com.buzzlogix.entity.Billing;
import com.buzzlogix.urls.BillingURL;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author administrator
 */
public class BillingServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(BillingServlet.class.getName());

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) {
        String result = null;
        resp.setContentType("application/json");

        try {
            PrintWriter out = resp.getWriter();

            switch (req.getPathInfo()) {
                case BillingURL.READ:
                    result = read(req.getParameter("id"));
                    break;
            }

            out.print(result);
            out.flush();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public final void doPost(final HttpServletRequest req, final HttpServletResponse resp) {
        String result = null;
        resp.setContentType("application/json");

        try {
            PrintWriter out = resp.getWriter();

            switch (req.getPathInfo()) {
                case BillingURL.UPDATE:
                    result = update(req.getReader());
                    break;
                case BillingURL.DELETE:
                    result = delete(req.getParameter("id"));
                    break;
            }

            out.print(result);
            out.flush();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        }
    }

    private String read(final String billingId) {
        String result = null;

        logger.log(Level.INFO, "billing Id={0}", billingId);

        try {
            if (!Strings.isNullOrEmpty(billingId)) {
                final Map<String, Object> parameters = new HashMap<String, Object>() {
                    {
                        put("id", billingId);
                    }
                };

                Billing b = new BillingDaoImpl().getSingleByQuery("com.buzzlogix.entity.Billing.findById", parameters);

                if (b != null) {
                    result = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(b);
                } else {
                    logger.log(Level.INFO, "cannot read the billing");
                }
            } else {
                logger.log(Level.INFO, "Must given a billing id");
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }

    private String update(BufferedReader reader) {
        String result = null;
        String billing = "", line;

        try {
            if (reader.ready()) {
                while ((line = reader.readLine()) != null) {
                    billing += line;
                }

                if (!Strings.isNullOrEmpty(billing)) {
                    logger.log(Level.INFO, "update billing={0}", billing);

                    Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

                    Billing b = gson.fromJson(billing, Billing.class);

                    if (b != null) {
                        b = new BillingDaoImpl().upsert(b);

                        if (b != null) {
                            result = gson.toJson(b);
                        } else {
                            logger.log(Level.INFO, "cannot update the billing");
                        }
                    } else {
                        logger.log(Level.INFO, "isn't correct the billing format json");
                    }
                }
            } else {
                logger.log(Level.INFO, "Must given a billing format json");
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }

    private String delete(final String billingId) {
        String result = null;

        logger.log(Level.INFO, "delete billing with id={0}", billingId);

        try {
            if (!Strings.isNullOrEmpty(billingId)) {
                final Map<String, Object> parameters = new HashMap<String, Object>() {
                    {
                        put("id", billingId);
                    }
                };

                Integer deleted = new BillingDaoImpl().delete("com.buzzlogix.entity.Billing.findById", parameters);

                if (deleted != null) {
                    result = deleted.toString();
                    logger.log(Level.INFO, "deleted the billing with id={0}", billingId);
                } else {
                    logger.log(Level.INFO, "cannot deleted the billing");
                }
            } else {
                logger.log(Level.INFO, "Must given a billing id");
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }
}

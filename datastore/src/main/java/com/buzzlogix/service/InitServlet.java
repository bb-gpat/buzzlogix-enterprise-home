package com.buzzlogix.service;

import java.util.logging.Logger;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class InitServlet implements ServletContextListener {

    private static final Logger logger = Logger.getLogger(InitServlet.class.getName());

    public InitServlet() {
    }

    @Override
    public void contextDestroyed(ServletContextEvent arg0) {
    }

    @Override
    public void contextInitialized(ServletContextEvent arg0) {
    }
}

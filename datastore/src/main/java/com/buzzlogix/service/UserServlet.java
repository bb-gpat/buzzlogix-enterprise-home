package com.buzzlogix.service;

import com.buzzlogix.dao.account.AccountDaoImpl;
import com.google.api.client.util.Strings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.buzzlogix.dao.user.UserDaoImpl;
import com.buzzlogix.entity.Account;
import com.buzzlogix.entity.User;
import com.buzzlogix.urls.UserURL;
import com.buzzlogix.utils.impl.LoggerImpl;
import com.buzzlogix.utils.impl.Security;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author administrator
 *
 * Entry point that initializes the application Pub/Sub resources.
 */
@SuppressWarnings("serial")
public class UserServlet extends HttpServlet {

    private static final Logger logger = new LoggerImpl().getLoggerByHandler(UserServlet.class.getName(), new ConsoleHandler());

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) {
        String result = null;
        resp.setContentType("application/json");

        try {
            PrintWriter out = resp.getWriter();

            logger.log(Level.INFO, "{0}", req.getPathInfo());

            switch (req.getPathInfo()) {
                case UserURL.LOGIN:
                    result = login(req.getParameter("username"), req.getParameter("password"));
                    break;
                case UserURL.LOGIN_EXIST:
                    result = loginExist(req.getParameter("username"), req.getParameter("email"));
                    break;
                case UserURL.LOGIN_EXIST_EMAIL:
                    result = loginExistEmail(req.getParameter("email"));
                    break;
                case UserURL.READ:
                    result = read(req.getParameter("id"));
                    break;
                case UserURL.READ_ACCOUNT:
                    result = readAccount(req.getParameter("user-id"));
                    break;
            }

            out.print(result);
            out.flush();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public final void doPost(final HttpServletRequest req, final HttpServletResponse resp) {
        String result = null;
        resp.setContentType("application/json");

        try {
            PrintWriter out = resp.getWriter();

            switch (req.getPathInfo()) {
                case UserURL.UPDATE:
                    result = update(req.getReader());
                    break;
                case UserURL.DELETE:
                    result = delete(req.getParameter("id"));
                    break;
            }

            out.print(result);
            out.flush();
        } catch (IOException ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        }
    }

    private Account getAcountById(final String accountId) {
        Account account = null;

        if (!Strings.isNullOrEmpty(accountId)) {
            final Map<String, Object> parameters = new HashMap<String, Object>() {
                {
                    put("id", accountId);
                }
            };

            account = new AccountDaoImpl().getSingleByQuery("com.buzzlogix.entity.Account.findById", parameters);
            
            if (account == null) {
                logger.log(Level.SEVERE, "no read account for account id", accountId);
            }
        } else {
            logger.log(Level.SEVERE, "the account id is null or empty");
        }
        
        return account;
    }

    private String login(final String username, final String password) {
        String result = null;

        logger.log(Level.INFO, "username={0}, password={1}", new Object[]{username, password});

        try {
            if (!Strings.isNullOrEmpty(username) && !Strings.isNullOrEmpty(password)) {
                final Map<String, Object> parameters = new HashMap<String, Object>() {
                    {
                        put("username", username);
                        put("password", Security.encryption(Security.SHA256, password));
                    }
                };

                User user = new UserDaoImpl().getSingleByQuery("com.buzzlogix.entity.User.findUserByCredentials", parameters);

                if (user != null) {
                    if (user.getAccount() == null) {
                        user.setAccount(getAcountById(user.getAccountId()));
                    }
                    result = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(user);
                } else {
                    logger.log(Level.INFO, "cannot read the user");
                }
            } else {
                logger.log(Level.INFO, "Must given an username and a password");
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }

    private String loginExist(final String username, final String email) {
        String result = null;

        logger.log(Level.INFO, "username={0}, email={1}", new Object[]{username, email});

        try {
            if (!Strings.isNullOrEmpty(username) && !Strings.isNullOrEmpty(email)) {
                final Map<String, Object> parameters = new HashMap<String, Object>() {
                    {
                        put("username", username);
                        put("email", email);
                    }
                };

                User user = new UserDaoImpl().getSingleByQuery("com.buzzlogix.entity.User.findUserUsernameEmail", parameters);

                if (user != null) {
                    result = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(user);
                } else {
                    logger.log(Level.INFO, "cannot read the user");
                }
            } else {
                logger.log(Level.INFO, "Must given an username and a email");
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }
    
    private String loginExistEmail(final String email) {
        String result = null;

        logger.log(Level.INFO, "email={0}", email);

        try {
            if (!Strings.isNullOrEmpty(email)) {
                final Map<String, Object> parameters = new HashMap<String, Object>() {
                    {
                        put("email", email);
                    }
                };

                User user = new UserDaoImpl().getSingleByQuery("com.buzzlogix.entity.User.findUserByEmail", parameters);

                if (user != null) {
                    result = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(user);
                } else {
                    logger.log(Level.INFO, "cannot read the user");
                }
            } else {
                logger.log(Level.INFO, "Must given an email");
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }

    private String read(final String userId) {
        String result = null;

        logger.log(Level.INFO, "userId={0}", userId);

        try {
            if (!Strings.isNullOrEmpty(userId)) {
                final Map<String, Object> parameters = new HashMap<String, Object>() {
                    {
                        put("id", userId);
                    }
                };

                User u = new UserDaoImpl().getSingleByQuery("com.buzzlogix.entity.User.findById", parameters);

                if (u != null) {
                    result = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(u);
                } else {
                    logger.log(Level.INFO, "cannot read the user");
                }
            } else {
                logger.log(Level.INFO, "Must given an user id");
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }

    private String readAccount(final String userId) {
        String result = null;

        logger.log(Level.INFO, "userId={0}", userId);

        try {
            if (!Strings.isNullOrEmpty(userId)) {
                final Map<String, Object> parameters = new HashMap<String, Object>() {
                    {
                        put("id", userId);
                    }
                };

                User u = new UserDaoImpl().getSingleByQuery("com.buzzlogix.entity.User.findById", parameters);

                if (u != null) {
                    result = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(u);
                } else {
                    logger.log(Level.INFO, "cannot read the user");
                }
            } else {
                logger.log(Level.INFO, "Must given an user id");
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }

    private String update(BufferedReader reader) {
        String result = null;
        String user = "", line;

        try {
            if (reader.ready()) {
                while ((line = reader.readLine()) != null) {
                    user += line;
                }

                if (!Strings.isNullOrEmpty(user)) {
                    logger.log(Level.INFO, "update user={0}", user);

                    Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                    User u = gson.fromJson(user, User.class);

                    if (u != null) {
                        u = new UserDaoImpl().upsert(u);

                        if (u != null) {
                            result = gson.toJson(u);
                        } else {
                            logger.log(Level.INFO, "cannot update the user");
                        }
                    } else {
                        logger.log(Level.INFO, "cannot read the user");
                    }
                } else {
                    logger.log(Level.INFO, "Must given an user format json");
                }
            }
        } catch (IOException | JsonSyntaxException ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }

    private String delete(final String userId) {
        String result = null;

        logger.log(Level.INFO, "delete user with id={0}", userId);

        try {
            if (!Strings.isNullOrEmpty(userId)) {
                final Map<String, Object> parameters = new HashMap<String, Object>() {
                    {
                        put("id", userId);
                    }
                };

                Integer deleted = new UserDaoImpl().delete("com.buzzlogix.entity.User.findById", parameters);

                if (deleted != null) {
                    result = deleted.toString();
                    logger.log(Level.INFO, "deleted the user with id={0}", userId);
                } else {
                    logger.log(Level.INFO, "cannot deleted the user");
                }
            } else {
                logger.log(Level.INFO, "Must given a user id");
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }
}

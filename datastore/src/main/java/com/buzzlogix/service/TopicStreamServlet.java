/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.service;

import com.google.api.client.util.Strings;
import com.google.appengine.api.modules.ModulesServiceFactory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.buzzlogix.dao.topic.TopicStreamDaoImpl;
import com.buzzlogix.entity.TopicStream;
import com.buzzlogix.enums.StreamStatusEnum;
import com.buzzlogix.urls.TopicStreamURL;
import com.buzzlogix.utils.impl.HttpClientImpl;
import com.buzzlogix.utils.impl.LoggerImpl;
import com.google.gson.JsonSyntaxException;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author administrator
 */
public class TopicStreamServlet extends HttpServlet {

    private final Logger logger = new LoggerImpl().getLoggerByHandler(TopicStreamServlet.class.getName(), new ConsoleHandler());

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) {
        String result = null;
        resp.setContentType("application/json");

        try {
            PrintWriter out = resp.getWriter();

            switch (req.getPathInfo()) {
                case TopicStreamURL.READ_ALL:
                    result = readAll();
                    break;
                case TopicStreamURL.READ:
                    result = read(req.getParameter("id"));
                    break;
            }

            out.print(result);
            out.flush();
        } catch (IOException ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } catch (NumberFormatException ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public final void doPost(final HttpServletRequest req, final HttpServletResponse resp) {
        String result = null;
        resp.setContentType("application/json");

        try {
            PrintWriter out = resp.getWriter();

            switch (req.getPathInfo()) {
                case TopicStreamURL.UPDATE:
                    result = update(req.getReader());
                    break;
                case TopicStreamURL.START:
                    result = startTopicStream(req.getReader());
                    break;
                case TopicStreamURL.STOP:
                    result = stopTopicStream(req.getReader());
                    break;
                case TopicStreamURL.DELETE:
                    result = delete(req.getReader());
                    break;
            }

            out.print(result);
            out.flush();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getMessage());
        }
    }

    private String read(final String topicId) {
        String result = null;

        logger.log(Level.ALL, "topic stream={0}", topicId);

        try {
            if (!Strings.isNullOrEmpty(topicId)) {
                final Map<String, Object> parameters = new HashMap<String, Object>() {
                    {
                        put("id", topicId);
                    }
                };

                TopicStream ts = new TopicStreamDaoImpl().getSingleByQuery("com.buzzlogix.entity.TopicStream.findById", parameters);

                if (ts != null) {
                    result = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(ts);
                } else {
                    result = "cannot read the topic stream";
                }
            } else {
                return "Must given a topic stream id";
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getMessage());
        } finally {
            return result;
        }
    }

    private String readAll() {
        List<TopicStream> streams = new TopicStreamDaoImpl().getListByQuery("com.buzzlogix.entity.TopicStream.findAll", null, 0, 10);
        return new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(streams);
    }

    private String update(BufferedReader reader) {
        String result = null;
        String topic = "", line;

        try {
            if (reader.ready()) {
                while ((line = reader.readLine()) != null) {
                    topic += line;
                }

                if (!Strings.isNullOrEmpty(topic)) {
                    logger.log(Level.INFO, "update topic stream={0}", topic);

                    Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

                    TopicStream ts = gson.fromJson(topic, TopicStream.class);

                    if (ts != null) {
                        ts = new TopicStreamDaoImpl().upsert(ts);

                        if (ts != null) {
                            result = gson.toJson(ts);
                        } else {
                            result = "cannot update the topic stream";
                        }
                    } else {
                        result = "isn't correct the topic stream format json";
                    }
                }
            } else {
                return "Must given a topic stream format json";
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getMessage());
        } finally {
            return result;
        }
    }

    private BufferedReader convertstringToReader(String json) {
        InputStream in = new ByteArrayInputStream(json.getBytes());

        return new BufferedReader(new InputStreamReader(in));
    }

    private String startTopicStream(BufferedReader reader) {
        String result = "0";
        String topic = "", line;

        try {
            if (reader.ready()) {
                while ((line = reader.readLine()) != null) {
                    topic += line;
                }

                if (!Strings.isNullOrEmpty(topic)) {
                    logger.log(Level.INFO, "start topic stream={0}", topic);

                    Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                    TopicStream ts = gson.fromJson(topic, TopicStream.class);
                    String json = read(ts.getId());

                    if (!Strings.isNullOrEmpty(json)) {
                        String target_host = ModulesServiceFactory.getModulesService().getVersionHostname("distributor", "v1");
                        String protocol = (Boolean.valueOf(System.getProperty("Local"))) ? "http://" : "https://";
                        String response = new HttpClientImpl().post(new URL(protocol + target_host + "/topicStream/handle"), json, null);

                        if (!Strings.isNullOrEmpty(response) && Boolean.valueOf(response).equals(Boolean.TRUE)) {
                            ts = gson.fromJson(json, TopicStream.class);
                            if (ts != null) {
                                json = gson.toJson(new TopicStream.Builder()
                                .setId(ts.getId())
                                .setStastus(StreamStatusEnum.STARTING)
                                .build());
                                reader = (!Strings.isNullOrEmpty(json)) ? convertstringToReader(json) : null;
                                if (reader != null) {
                                    update(reader);
                                }
                            }

                            result = "1";
                        }
                    }
                }
            } else {
                logger.log(Level.INFO, "Must given a topic stream format json");

                result = "0";
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }

    private String stopTopicStream(BufferedReader reader) {
        String result = "0";
        String topic = "", line;

        try {
            if (reader.ready()) {
                while ((line = reader.readLine()) != null) {
                    topic += line;
                }

                if (!Strings.isNullOrEmpty(topic)) {
                    logger.log(Level.INFO, "stop topic stream={0}", topic);

                    String target_host = ModulesServiceFactory.getModulesService().getVersionHostname("distributor", "v1");
                    String protocol = (Boolean.valueOf(System.getProperty("Local"))) ? "http://" : "https://";
                    new HttpClientImpl().post(new URL(protocol + target_host + "/topicStream/stop"), topic, null);

                    result = "1";
                }
            } else {
                logger.log(Level.INFO, "Must given a topic stream format json");

                result = "0";
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }

    private String delete(BufferedReader reader) {
        String result = null;
        String json = "", line;

        try {
            if (reader.ready()) {
                while ((line = reader.readLine()) != null) {
                    json += line;
                }

                if (!Strings.isNullOrEmpty(json)) {
                    logger.log(Level.INFO, "delete topic stream={0}", json);

                    Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                    final TopicStream topic = gson.fromJson(json, TopicStream.class);

                    if (topic != null) {
                        final Map<String, Object> parameters = new HashMap<String, Object>() {
                            {
                                put("id", topic.getId());
                            }
                        };

                        Integer deleted = new TopicStreamDaoImpl().delete("com.buzzlogix.entity.TopicStream.findById", parameters);

                        if (deleted != null) {
                            GenericPostServlet servlet = new GenericPostServlet();
                            
                            servlet.deleteTable(topic);
                            result = deleted.toString();
                            
                            logger.log(Level.INFO, "deleted the topic with id={0}", topic.getId());
                        } else {
                            logger.log(Level.INFO, "cannot deleted the topic");
                        }
                    } else {
                        logger.log(Level.INFO, "No correct topic stream json format");
                    }
                } else {
                    logger.log(Level.INFO, "Must given a topic id");
                }
            }
        } catch (IOException | JsonSyntaxException ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }
}

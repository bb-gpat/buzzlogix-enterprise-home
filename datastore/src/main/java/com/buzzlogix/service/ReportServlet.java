/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.service;

import com.google.api.client.util.Strings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.buzzlogix.dao.report.ReportDaoImpl;
import com.buzzlogix.entity.Report;
import com.buzzlogix.urls.ReportURL;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author administrator
 */
public class ReportServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(ReportServlet.class.getName());

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) {
        String result = null;
        resp.setContentType("application/json");

        try {
            PrintWriter out = resp.getWriter();

            switch (req.getPathInfo()) {
                case ReportURL.READ:
                    result = read(req.getParameter("id"));
                    break;
            }

            out.print(result);
            out.flush();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public final void doPost(final HttpServletRequest req, final HttpServletResponse resp) {
        String result = null;
        resp.setContentType("application/json");

        try {
            PrintWriter out = resp.getWriter();

            switch (req.getPathInfo()) {
                case ReportURL.UPDATE:
                    result = update(req.getReader());
                    break;
                case ReportURL.DELETE:
                    result = delete(req.getParameter("id"));
                    break;
            }

            out.print(result);
            out.flush();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        }
    }

    private String read(final String reportId) {
        String result = null;

        logger.log(Level.INFO, "report={0}", reportId);

        try {
            if (!Strings.isNullOrEmpty(reportId)) {
                final Map<String, Object> parameters = new HashMap<String, Object>() {
                    {
                        put("id", reportId);
                    }
                };

                Report p = new ReportDaoImpl().getSingleByQuery("com.buzzlogix.entity.Report.findById", parameters);

                if (p != null) {
                    result = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(p);
                } else {
                    logger.log(Level.INFO, "cannot read the report");
                }
            } else {
                logger.log(Level.INFO, "Must given a report id");
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }

    private String update(BufferedReader reader) {
        String result = null;
        String report = "", line;

        try {
            if (reader.ready()) {
                while ((line = reader.readLine()) != null) {
                    report += line;
                }

                if (!Strings.isNullOrEmpty(report)) {
                    logger.log(Level.INFO, "update report={0}", report);

                    Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

                    Report r = gson.fromJson(report, Report.class);

                    if (r != null) {
                        r = new ReportDaoImpl().upsert(r);

                        if (r != null) {
                            result = gson.toJson(r);
                        } else {
                            logger.log(Level.INFO, "cannot update the report");
                        }
                    } else {
                        logger.log(Level.INFO, "isn't correct the report format json");
                    }
                }
            } else {
                logger.log(Level.INFO, "Must given a report format json");
            }
        } catch (IOException | JsonSyntaxException ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }

    private String delete(final String reportId) {
        String result = null;

        logger.log(Level.INFO, "delete report with id={0}", reportId);

        try {
            if (!Strings.isNullOrEmpty(reportId)) {
                final Map<String, Object> parameters = new HashMap<String, Object>() {
                    {
                        put("id", reportId);
                    }
                };

                Integer deleted = new ReportDaoImpl().delete("com.buzzlogix.entity.Report.findById", parameters);

                if (deleted != null) {
                    result = deleted.toString();
                    logger.log(Level.INFO, "deleted the report with id={0}", reportId);
                } else {
                    logger.log(Level.INFO, "cannot deleted the report");
                }
            } else {
                logger.log(Level.INFO, "Must given a report id");
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }
}

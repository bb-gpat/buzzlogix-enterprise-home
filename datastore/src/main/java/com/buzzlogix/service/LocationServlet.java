/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.service;

import com.buzzlogix.dao.location.LocationDaoImpl;
import com.buzzlogix.entity.Location;
import com.buzzlogix.urls.LocationURL;
import com.buzzlogix.utils.gson.RegisterTypeAdapter;
import com.buzzlogix.utils.impl.LoggerImpl;
import com.google.api.client.util.Strings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author administrator
 */
public class LocationServlet extends HttpServlet {

    private static final Logger logger = new LoggerImpl().getLoggerByHandler(LocationServlet.class.getName(), new ConsoleHandler());

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) {
        String result = null;
        resp.setContentType("application/json");

        try {
            PrintWriter out = resp.getWriter();

            switch (req.getPathInfo()) {
                case LocationURL.READ_ALL:
                    result = readAll();
                    break;
            }

            out.print(result);
            out.flush();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public final void doPost(final HttpServletRequest req, final HttpServletResponse resp) {
        String result = null;
        resp.setContentType("application/json");

        try {
            PrintWriter out = resp.getWriter();

            switch (req.getPathInfo()) {
                case LocationURL.UPSERT:
                    result = upsert(req.getReader());
                    break;
                case LocationURL.DELETE:
                    result = delete(req.getParameter("id"));
                    break;
            }

            out.print(result);
            out.flush();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        }
    }

    private String readAll() {
        String result = null;

        try {
            List<Location> l = new LocationDaoImpl().getListByQuery("com.buzzlogix.entity.Location.findAll", null, null, null);

            if (l != null) {
                result = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(l);
            } else {
                logger.log(Level.INFO, "cannot read the location");
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }

    private String upsert(BufferedReader reader) {
        String result = null;
        String location = "", line;

        try {
            if (reader.ready()) {
                while ((line = reader.readLine()) != null) {
                    location += line;
                }

                if (!Strings.isNullOrEmpty(location)) {
                    logger.log(Level.INFO, "upsert location={0}", location);

                    Gson gson = new GsonBuilder()
                            .excludeFieldsWithoutExposeAnnotation()
                            .registerTypeAdapter(Date.class, RegisterTypeAdapter.deserDate)
                            .create();
                    Location l = gson.fromJson(location, Location.class);

                    if (l != null) {
                        l = new LocationDaoImpl().upsert(l);

                        if (l != null) {
                            result = gson.toJson(l);
                        } else {
                            logger.log(Level.INFO, "cannot upsert the location");
                        }
                    } else {
                        logger.log(Level.INFO, "isn't correct the location format json");
                    }
                } else {
                    logger.log(Level.INFO, "Must given a location format json");
                }
            } else {
                logger.log(Level.INFO, "Cannot read json data");
            }
        } catch (IOException | JsonSyntaxException ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }

    private String delete(final String locationId) {
        String result = null;

        logger.log(Level.INFO, "delete location with id={0}", locationId);

        try {
            if (!Strings.isNullOrEmpty(locationId)) {
                final Map<String, Object> parameters = new HashMap<String, Object>() {
                    {
                        put("id", locationId);
                    }
                };

                Integer deleted = new LocationDaoImpl().delete("com.buzzlogix.entity.Location.findById", parameters);

                if (deleted != null) {
                    result = deleted.toString();
                    logger.log(Level.INFO, "deleted the location with id={0}", locationId);
                } else {
                    logger.log(Level.INFO, "cannot deleted the location");
                }
            } else {
                logger.log(Level.INFO, "Must given a location id");
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }
}

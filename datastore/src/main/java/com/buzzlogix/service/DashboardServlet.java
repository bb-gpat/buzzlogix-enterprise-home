/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.service;

import com.google.api.client.util.Strings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.buzzlogix.dao.dashboard.DashboardDaoImpl;
import com.buzzlogix.entity.Dashboard;
import com.buzzlogix.urls.DashboardURL;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author administrator
 */
public class DashboardServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(DashboardServlet.class.getName());

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) {
        String result = null;
        resp.setContentType("application/json");

        try {
            PrintWriter out = resp.getWriter();

            switch (req.getPathInfo()) {
                case DashboardURL.READ:
                    result = read(req.getParameter("id"));
                    break;
            }

            out.print(result);
            out.flush();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public final void doPost(final HttpServletRequest req, final HttpServletResponse resp) {
        String result = null;
        resp.setContentType("application/json");

        try {
            PrintWriter out = resp.getWriter();

            switch (req.getPathInfo()) {
                case DashboardURL.UPDATE:
                    result = update(req.getReader());
                    break;
                case DashboardURL.DELETE:
                    result = delete(req.getParameter("id"));
                    break;
            }

            out.print(result);
            out.flush();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        }
    }

    private String read(final String dashboardId) {
        String result = null;

        logger.log(Level.INFO, "dashboardId={0}", dashboardId);

        try {
            if (!Strings.isNullOrEmpty(dashboardId)) {
                final Map<String, Object> parameters = new HashMap<String, Object>() {
                    {
                        put("id", dashboardId);
                    }
                };

                Dashboard d = new DashboardDaoImpl().getSingleByQuery("com.buzzlogix.entity.Dashboard.findById", parameters);

                if (d != null) {
                    result = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(d);
                } else {
                    logger.log(Level.INFO, "cannot read the dashboard");
                }
            } else {
                logger.log(Level.INFO, "Must given a dashboard id");
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }

    private String update(BufferedReader reader) {
        String result = null;
        String dashboard = "", line;

        try {
            if (reader.ready()) {
                while ((line = reader.readLine()) != null) {
                    dashboard += line;
                }

                if (!Strings.isNullOrEmpty(dashboard)) {
                    logger.log(Level.INFO, "update dashboard={0}", dashboard);

                    Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

                    Dashboard d = gson.fromJson(dashboard, Dashboard.class);

                    if (d != null) {
                        d = new DashboardDaoImpl().upsert(d);

                        if (d != null) {
                            result = gson.toJson(d);
                        } else {
                            logger.log(Level.INFO, "cannot update the dashboard");
                        }
                    } else {
                        logger.log(Level.INFO, "isn't correct the dashboard format json");
                    }
                }
            } else {
                logger.log(Level.INFO, "Must given a dashboard format json");
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }

    private String delete(final String dashboardId) {
        String result = null;

        logger.log(Level.INFO, "delete dashboard with id={0}", dashboardId);

        try {
            if (!Strings.isNullOrEmpty(dashboardId)) {
                final Map<String, Object> parameters = new HashMap<String, Object>() {
                    {
                        put("id", dashboardId);
                    }
                };

                Integer deleted = new DashboardDaoImpl().delete("com.buzzlogix.entity.Dashboard.findById", parameters);

                if (deleted != null) {
                    result = deleted.toString();
                    logger.log(Level.INFO, "deleted the dashboard with id={0}", dashboardId);
                } else {
                    logger.log(Level.INFO, "cannot deleted the dashboard");
                }
            } else {
                logger.log(Level.INFO, "Must given a dashboard id");
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }
}

package com.buzzlogix.service;

import com.google.common.base.Strings;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.buzzlogix.dao.genericpost.GenericPostDaoImpl;
import com.buzzlogix.entity.TopicStream;
import com.buzzlogix.entity.bigquery.Counters;
import com.buzzlogix.entity.bigquery.GenericPost;
import com.buzzlogix.urls.GenericPostURL;
import com.buzzlogix.utils.gson.RegisterTypeAdapter;
import com.buzzlogix.utils.impl.LoggerImpl;
import com.google.appengine.api.datastore.Text;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.ConsoleHandler;

public class GenericPostServlet extends HttpServlet {

    private static final Logger logger = new LoggerImpl().getLoggerByHandler(GenericPostServlet.class.getName(), new ConsoleHandler());

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) {
        String result = null;
        resp.setContentType("application/json; charset=UTF-8");

        try {
            PrintWriter out = resp.getWriter();

            switch (req.getPathInfo()) {
                case GenericPostURL.READ:
                    String streamId = req.getParameter("streamId");
                    String filter = req.getParameter("filter");
                    String order = req.getParameter("order");
                    String page = req.getParameter("page");
                    String size = req.getParameter("size");

                    streamId = (streamId != null && streamId.equals("null")) ? null : streamId;
                    filter = (filter != null && filter.equals("null")) ? null : filter;
                    order = (order != null && order.equals("null")) ? null : order;
                    page = (page != null && page.equals("null")) ? null : page;
                    size = (size != null && size.equals("null")) ? null : size;
                    result = read(streamId, filter, order, page, size);
                    break;
                case GenericPostURL.COUNT:
                    streamId = req.getParameter("streamId");
                    filter = req.getParameter("filter");

                    streamId = (streamId != null && streamId.equals("null")) ? null : streamId;
                    filter = (filter != null && filter.equals("null")) ? null : filter;
                    result = count(streamId, filter);
                    break;
                case GenericPostURL.DELETE:
                    result = deleteAll();
                    break;
            }

            out.print(result);
            out.flush();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        }
    }

    @Override
    @SuppressWarnings({"unchecked"})
    public final void doPost(final HttpServletRequest req, final HttpServletResponse resp) throws IOException {
        String result = null;
        resp.setContentType("application/json");

        try {
            switch (req.getPathInfo()) {
                case GenericPostURL.UPSERT:
                    String genericPost = req.getParameter("genericPost");
                    String streamId = req.getParameter("streamId");

                    result = upsertGenericPost(genericPost, streamId);
                    break;
                case GenericPostURL.UPSERT_LOCAL:
                    result = upsertGenericPost(req.getReader(), req.getHeader("streamId"));
                    break;
                case GenericPostURL.COUNT:
                    break;
            }

            logger.log(Level.INFO, "{0}", result);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        }
    }

    public String createTable(TopicStream topic) {
        String result = null;
        
        try {
            GenericPostDaoImpl post = new GenericPostDaoImpl();

            result = post.createTable("Stream_" + topic.getId().replace("-", "_"));

            if (Strings.isNullOrEmpty(result) || !result.equals("OK")) {
                logger.log(Level.INFO, "Error to create big query table for the topic[{0}]:{1}", new Object[]{topic.getId(), result});
            }
        } catch (IOException | InterruptedException ex) {
            logger.log(Level.SEVERE, null, ex);
        }

        return result;
    }
    
    public String deleteTable(TopicStream topic) {
        String result = null;
        try {
            GenericPostDaoImpl post = new GenericPostDaoImpl();

            result = post.deleteTable("Stream_" + topic.getId().replace("-", "_"));

            if (Strings.isNullOrEmpty(result) || !result.equals("OK")) {
                logger.log(Level.INFO, "Error to delete big query table for the topic[{0}]:{1}", new Object[]{topic.getId(), result});
            }
        } catch (IOException | InterruptedException ex) {
            logger.log(Level.SEVERE, null, ex);
        }

        return result;
    }

    private String read(final String streamId, final String filter, final String order, String page, String size) {
        logger.log(Level.INFO, "{0},{1},{2},{3},{4}", new Object[]{streamId, filter, order, page, size});

        String result = null;
        Integer dbpage = (Strings.isNullOrEmpty(page) || page.equals("1")) ? 0 : new Integer(page);
        Long dbsize = (Strings.isNullOrEmpty(size)) ? 10 : Long.valueOf(size);
        String query = null;

        if (!Strings.isNullOrEmpty(streamId)) {
            if (!Strings.isNullOrEmpty(filter) && !Strings.isNullOrEmpty(order)) {
                query = "SELECT * FROM gap.Stream_" + streamId.replace("-", "_") + " WHERE " + filter + " ORDER BY " + order;
            } else if (!Strings.isNullOrEmpty(filter)) {
                query = "SELECT * FROM gap.Stream_" + streamId.replace("-", "_") + " WHERE " + filter + " ORDER BY published DESC";
            } else if (!Strings.isNullOrEmpty(order)) {
                query = "SELECT * FROM gap.Stream_" + streamId.replace("-", "_") + " ORDER BY " + order;
            } else {
                query = "SELECT * FROM gap.Stream_" + streamId.replace("-", "_") + " ORDER BY published DESC";
            }

            logger.log(Level.INFO, "query = {0}", query);

            if (!Strings.isNullOrEmpty(query)) {
                try {
                    result = new GenericPostDaoImpl().runQuery(query, dbpage, dbsize);
                } catch (IOException | InterruptedException ex) {
                    logger.log(Level.SEVERE, null, ex);
                }
            }
        } else {
            logger.log(Level.INFO, "no valid stream id: {0}", streamId);
        }

        return result;
    }

    private String getCounters(final String query, final String streamId) {
        String result = null;

        try {
            if (!Strings.isNullOrEmpty(query)) {
                Counters counters = new Counters();

                counters.setTotal(new GenericPostDaoImpl().countQuery(query));
//                int posSource = query.indexOf("source IN (");
//                int posSentiment = query.indexOf("sentiment IN (");
//                int posLanguage = query.indexOf("language IN (");
//                int posTags = query.indexOf("tags IN (");
//
//                if (posSource > 0) {
//                    int firstSource = (posSource > 0) ? posSource + "source IN (".length() : -1;
//                    int lastSource = (firstSource > 0) ? query.substring(firstSource).indexOf(")") : -1;
//                    String source = (firstSource > 0 && lastSource > 0) ? query.substring(firstSource, lastSource) : null;
//                    String[] sources = (!Strings.isNullOrEmpty(source)) ? source.split(",") : null;
//                    
//                    if (sources != null && sources.length > 0) {
//                        Map<String, Long> resultSource = new HashMap<>();
//                        
//                        for (String field : sources) {
//                            String querySource = "SELECT count(*) FROM gap.Stream_" + streamId.replace("-", "_") + " WHERE source='source:" + field + "'";
//                            
//                            resultSource.put(field, new GenericPostDaoImpl().countQuery(querySource));
//                        }
//                        
//                        counters.setSource(resultSource);
//                    }
//                }
//                if (posSentiment > 0) {
//
//                }
//                if (posLanguage > 0) {
//
//                }
//                if (posTags > 0) {
//
//                }

                result = new Gson().toJson(counters);
            }
        } catch (IOException | InterruptedException ex) {
            logger.log(Level.SEVERE, null, ex);
        }

        return result;
    }

    private String count(final String streamId, final String filter) {
        logger.log(Level.INFO, "{0},{1}", new Object[]{streamId, filter});

        String result = null;
        String query = null;

        if (!Strings.isNullOrEmpty(streamId)) {
            if (!Strings.isNullOrEmpty(filter)) {
                query = "SELECT count(*) FROM gap.Stream_" + streamId.replace("-", "_") + " WHERE " + filter;
            } else {
                query = "SELECT count(*) FROM gap.Stream_" + streamId.replace("-", "_");
            }

            logger.log(Level.INFO, "query = {0}", query);

            if (!Strings.isNullOrEmpty(query)) {
                result = getCounters(query, streamId);
            }
        } else {
            logger.log(Level.INFO, "no valid stream id: {0}", streamId);
        }

        return result;
    }

    private String upsertGenericPost(BufferedReader reader, String streamId) {
        String result = null;
        String data = "", line;

        try {
            if (reader.ready()) {
                while ((line = reader.readLine()) != null) {
                    data += line;
                }
                reader.close();

                if (!Strings.isNullOrEmpty(data) && !Strings.isNullOrEmpty(streamId)) {
                    Set<GenericPost> clearGenericPosts = null;
                    GenericPost[] genericPosts = new GsonBuilder()
                                  .registerTypeAdapter(Text.class, new RegisterTypeAdapter().deser)
                                  .create()
                                  .fromJson(data, GenericPost[].class);
                    String tableId = "Stream_" + streamId.replace("-", "_");

                    if (genericPosts != null && genericPosts.length > 0) {
                        clearGenericPosts = new HashSet<>();

                        for (GenericPost gp : genericPosts) {
                            String query = "SELECT count(*) FROM gap." + tableId + " WHERE author='" + gp.getAuthor() + "' AND published='" + gp.getPublished() + "'";
                            Long count = new GenericPostDaoImpl().countQuery(query);

                            if (count.equals(0l)) {
                                clearGenericPosts.add(gp);
                            } else {
                                logger.log(Level.INFO, "found same tweet {0},{1}:{2}", new Object[]{gp.getAuthor(), gp.getPublished(), count});
                            }
                        }
                        if (clearGenericPosts != null && !clearGenericPosts.isEmpty()) {
                            data = new GsonBuilder()
                            .registerTypeAdapter(Text.class, new RegisterTypeAdapter().ser)
                            .create()
                            .toJson(clearGenericPosts.toArray(), GenericPost[].class);

                            new GenericPostDaoImpl().insert(tableId, data);

                            result = "OK";
                        }
                    }
                } else {
                    result = "Must given a generic post format json: " + data;
                }
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }

    private String upsertGenericPost(String jsonGenericPost, String streamId) {
        String result = null;

        try {
            if (!Strings.isNullOrEmpty(jsonGenericPost) && !Strings.isNullOrEmpty(streamId)) {
                String tableId = "Stream_" + streamId.replace("-", "_");
                Set<GenericPost> clearGenericPosts = null;
                GenericPost[] genericPosts = new GsonBuilder()
                              .registerTypeAdapter(Text.class, new RegisterTypeAdapter().deser)
                              .create()
                              .fromJson(jsonGenericPost, GenericPost[].class);

                if (genericPosts != null && genericPosts.length > 0) {
                    clearGenericPosts = new HashSet<>();

                    for (GenericPost gp : genericPosts) {
                        String query = "SELECT count(*) FROM gap." + tableId + " WHERE author='" + gp.getAuthor() + "' AND published='" + gp.getPublished() + "'";
                        Long count = new GenericPostDaoImpl().countQuery(query);

                        if (count.equals(0l)) {
                            clearGenericPosts.add(gp);
                        } else {
                            logger.log(Level.INFO, "found same tweet {0},{1}:{2}", new Object[]{gp.getAuthor(), gp.getPublished(), count});
                        }
                    }
                    if (clearGenericPosts != null && !clearGenericPosts.isEmpty()) {
                        jsonGenericPost = new GsonBuilder()
                        .registerTypeAdapter(Text.class, new RegisterTypeAdapter().ser)
                        .create()
                        .toJson(clearGenericPosts.toArray(), GenericPost[].class);

                        new GenericPostDaoImpl().insert(tableId, jsonGenericPost);

                        result = "OK";
                    }
                }
            } else {
                result = "Must given a generic post format json: " + jsonGenericPost;
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }

    private String deleteAll() {
        Integer count = 0;//new GenericPostDaoImpl().deleteByQuery("com.gap.entity.GenericPost.findAll", null);

        return count.toString();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.service;

import com.google.api.client.util.Strings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.buzzlogix.dao.licence.LicenceDaoImpl;
import com.buzzlogix.urls.LicenceURL;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.buzzlogix.entity.Licence;

/**
 *
 * @author administrator
 */
public class LicenceServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(LicenceServlet.class.getName());

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) {
        String result = null;
        resp.setContentType("application/json");

        try {
            PrintWriter out = resp.getWriter();

            switch (req.getPathInfo()) {
                case LicenceURL.READ:
                    result = read(req.getParameter("id"));
                    break;
            }

            out.print(result);
            out.flush();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public final void doPost(final HttpServletRequest req, final HttpServletResponse resp) {
        String result = null;
        resp.setContentType("application/json");

        try {
            PrintWriter out = resp.getWriter();

            switch (req.getPathInfo()) {
                case LicenceURL.UPDATE:
                    result = update(req.getReader());
                    break;
                case LicenceURL.DELETE:
                    result = delete(req.getParameter("id"));
                    break;
            }

            out.print(result);
            out.flush();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        }
    }

    private String read(final String licenceId) {
        String result = null;

        logger.log(Level.INFO, "licenceId={0}", licenceId);

        try {
            if (!Strings.isNullOrEmpty(licenceId)) {
                final Map<String, Object> parameters = new HashMap<String, Object>() {
                    {
                        put("id", licenceId);
                    }
                };

                Licence l = new LicenceDaoImpl().getSingleByQuery("com.buzzlogix.entity.Licence.findById", parameters);

                if (l != null) {
                    result = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(l);
                } else {
                    logger.log(Level.INFO, "cannot read the licence");
                }
            } else {
                logger.log(Level.INFO, "Must given a licence id");
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }

    private String update(BufferedReader reader) {
        String result = null;
        String licence = "", line;

        try {
            if (reader.ready()) {
                while ((line = reader.readLine()) != null) {
                    licence += line;
                }

                if (!Strings.isNullOrEmpty(licence)) {
                    logger.log(Level.INFO, "update licence={0}", licence);

                    Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

                    Licence l = gson.fromJson(licence, Licence.class);

                    if (l != null) {
                        l = new LicenceDaoImpl().upsert(l);

                        if (l != null) {
                            result = gson.toJson(l);
                        } else {
                            logger.log(Level.INFO, "cannot update the licence");
                        }
                    } else {
                        logger.log(Level.INFO, "isn't correct the licence format json");
                    }
                }
            } else {
                return "Must given a licence format json";
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }

    private String delete(final String licenceId) {
        String result = null;

        logger.log(Level.INFO, "delete licence with id={0}", licenceId);

        try {
            if (!Strings.isNullOrEmpty(licenceId)) {
                final Map<String, Object> parameters = new HashMap<String, Object>() {
                    {
                        put("id", licenceId);
                    }
                };

                Integer deleted = new LicenceDaoImpl().delete("com.buzzlogix.entity.Licence.findById", parameters);

                if (deleted != null) {
                    result = deleted.toString();
                    logger.log(Level.INFO, "deleted the licence with id={0}", licenceId);
                } else {
                    logger.log(Level.INFO, "cannot deleted the licence");
                }
            } else {
                logger.log(Level.INFO, "Must given a licence id");
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.service;

import com.google.api.client.util.Strings;
import com.google.appengine.api.modules.ModulesServiceFactory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.buzzlogix.dao.group.GroupDaoImpl;
import com.buzzlogix.dao.user.UserDaoImpl;
import com.buzzlogix.entity.Group;
import com.buzzlogix.entity.TopicStream;
import com.buzzlogix.entity.TopicStreamConfig;
import com.buzzlogix.entity.User;
import com.buzzlogix.enums.SourceTypeEnum;
import com.buzzlogix.urls.GroupURL;
import com.buzzlogix.utils.gson.RegisterTypeAdapter;
import com.buzzlogix.utils.impl.HttpClientImpl;
import com.buzzlogix.utils.impl.LoggerImpl;
import com.buzzlogix.utils.impl.Security;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author administrator
 */
public class GroupServlet extends HttpServlet {

    private static final Logger logger = new LoggerImpl().getLoggerByHandler(GroupServlet.class.getName(), new ConsoleHandler());

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) {
        String result = null;
        resp.setContentType("application/json");

        try {
            PrintWriter out = resp.getWriter();

            switch (req.getPathInfo()) {
                case GroupURL.READ:
                    result = read(req.getParameter("id"));
                    break;
            }

            out.print(result);
            out.flush();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public final void doPost(final HttpServletRequest req, final HttpServletResponse resp) {
        String result = null;
        resp.setContentType("application/json");

        try {
            PrintWriter out = resp.getWriter();

            switch (req.getPathInfo()) {
                case GroupURL.UPDATE:
                    result = update(req.getReader());
                    break;
                case GroupURL.DELETE:
                    result = delete(req.getParameter("id"));
                    break;
            }

            out.print(result);
            out.flush();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        }
    }

    private String read(final String groupId) {
        String result = null;

        logger.log(Level.ALL, "group id={0}", groupId);

        try {
            if (!Strings.isNullOrEmpty(groupId)) {
                final Map<String, Object> parameters = new HashMap<String, Object>() {
                    {
                        put("id", groupId);
                    }
                };

                Group g = new GroupDaoImpl().getSingleByQuery("com.buzzlogix.entity.Group.findById", parameters);

                logger.log(Level.INFO, "{0}", g.toString());

                if (g != null) {
                    result = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(g);

                    logger.log(Level.INFO, "{0}", result);
                } else {
                    logger.log(Level.INFO, "cannot read the group");
                }
            } else {
                logger.log(Level.INFO, "Must given a group id");
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }
    
    private Group createTopic(Group g, Gson gson) throws MalformedURLException {
        GenericPostServlet servlet = new GenericPostServlet();
        TopicStream topic = g.getTopics().iterator().next();
        TopicStreamConfig config = topic.getTopicConfig();

        if (config != null) {
            List<SourceTypeEnum> sources = config.getSources();

            if (sources != null && !sources.isEmpty()) {
                String created = servlet.createTable(topic);

                if (!Strings.isNullOrEmpty(created) && created.equals("OK")) {
                    topic.setDateCreated(new Date());
                    g = new GroupDaoImpl().upsert(g);

                    if (g != null) {
                        String target_host = ModulesServiceFactory.getModulesService().getVersionHostname("distributor", "v1");
                        String protocol = (Boolean.valueOf(System.getProperty("Local"))) ? "http://" : "https://";
                        String data = gson.toJson(topic, TopicStream.class);

                        new HttpClientImpl().post(new URL(protocol + target_host + "/topicStream/handle"), data, null);
                    }
                    
                    return g;
                }
            } else {
                logger.log(Level.INFO, "no found any source");
            }
        } else {
            logger.log(Level.INFO, "no found topic stream config");
        }
        
        return null;
    }

    private Group createUser(Group g) {
        final User user = g.getUsers().iterator().next();

        user.setPassword(Security.encryption(Security.SHA256, user.getPassword()));
        final Map<String, Object> parametersUsername = new HashMap<String, Object>() {
            {
                put("username", user.getUsername());
            }
        };
        final Map<String, Object> parametersEmail = new HashMap<String, Object>() {
            {
                put("email", user.getEmail());
            }
        };

        User dbuserUsername = new UserDaoImpl().getSingleByQuery("com.buzzlogix.entity.User.findUserByUsername", parametersUsername);
        User dbuserEmail = new UserDaoImpl().getSingleByQuery("com.buzzlogix.entity.User.findUserByEmail", parametersEmail);

        return (dbuserUsername != null || dbuserEmail != null) ? null : new GroupDaoImpl().upsert(g);
    }

    private String update(BufferedReader reader) {
        String result = null;
        String group = "", line;

        try {
            if (reader.ready()) {
                while ((line = reader.readLine()) != null) {
                    group += line;
                }

                if (!Strings.isNullOrEmpty(group)) {
                    logger.log(Level.INFO, "update group={0}", group);

                    Gson gson = new GsonBuilder()
                            .excludeFieldsWithoutExposeAnnotation()
                            .registerTypeAdapter(Date.class, RegisterTypeAdapter.deserDate)
                            .create();

                    Group g = gson.fromJson(group, Group.class);

                    if (g != null) {
                        Set<TopicStream> tss = g.getTopics();
                        Set<User> users = g.getUsers();

                        if (tss != null && !tss.isEmpty()) {
                            g = createTopic(g, gson);
                        } else if (users != null && !users.isEmpty()) {
                            g = createUser(g);
                        } else {
                            new GroupDaoImpl().upsert(g);
                        }
                        if (g != null) {
                            result = gson.toJson(g);
                        } else {
                            logger.log(Level.INFO, "cannot update the group");
                        }
                    } else {
                        logger.log(Level.INFO, "isn't correct the group format json");
                    }
                }
            } else {
                logger.log(Level.INFO, "Must given a group format json");
            }
        } catch (IOException | JsonSyntaxException ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }

    private String delete(final String groupId) {
        String result = null;

        logger.log(Level.INFO, "delete group with id={0}", groupId);

        try {
            if (!Strings.isNullOrEmpty(groupId)) {
                final Map<String, Object> parameters = new HashMap<String, Object>() {
                    {
                        put("id", groupId);
                    }
                };

                Integer deleted = new GroupDaoImpl().delete("com.buzzlogix.entity.Group.findById", parameters);

                if (deleted != null) {
                    result = deleted.toString();
                    logger.log(Level.INFO, "deleted the group with id={0}", groupId);
                } else {
                    logger.log(Level.INFO, "cannot deleted the group");
                }
            } else {
                logger.log(Level.INFO, "Must given a group id");
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }
}

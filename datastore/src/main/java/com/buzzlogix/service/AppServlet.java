package com.buzzlogix.service;

import com.buzzlogix.dao.location.LocationDaoImpl;
import com.google.api.client.util.Strings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.buzzlogix.dao.user.UserDaoImpl;
import com.buzzlogix.entity.Channel;
import com.buzzlogix.entity.Dashboard;
import com.buzzlogix.entity.Group;
import com.buzzlogix.entity.Location;
import com.buzzlogix.entity.Publisher;
import com.buzzlogix.entity.TopicStream;
import com.buzzlogix.entity.User;
import com.buzzlogix.entity.Widget;
import com.buzzlogix.enums.StreamStatusEnum;
import com.buzzlogix.urls.RootURL;
import com.buzzlogix.utils.impl.LoggerImpl;
import com.buzzlogix.utils.impl.Security;
import com.buzzlogix.utils.impl.XMLConfigurationImpl;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author administrator
 *
 * Entry point that initializes the application Pub/Sub resources.
 */
@SuppressWarnings("serial")
public class AppServlet extends HttpServlet {

    private static final Logger logger = new LoggerImpl().getLoggerByHandler(AppServlet.class.getName(), new ConsoleHandler());

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) {
        String result = null;

        try {
            PrintWriter out = resp.getWriter();

            switch (req.getPathInfo()) {
                case RootURL.ADD_ADMIN:
                    result = addAdmin(req.getReader());
                    break;
                case RootURL.ADD_LOCATIONS:
                    result = addLocation(req.getReader());
                    break;
            }

            out.print(result);
            out.flush();
        } catch (RuntimeException | IOException ex) {
            logger.log(Level.SEVERE, "{0}", ex);
        }
    }

    private String addAdmin(BufferedReader reader) {
        String result = null;

        try {
            XMLConfigurationImpl confing = new XMLConfigurationImpl("META-INF/datastore-data.xml");
            String user = "", line;

            if (reader.ready()) {
                while ((line = reader.readLine()) != null) {
                    user += line;
                }
            }
            if (!Strings.isNullOrEmpty(user)) {
                User admin = readAdmin(user, confing.getString("admin"));

                if (admin != null) {
                    admin = upsertUser(admin);

                    if (admin != null) {
                        result = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(admin);
                    } else {
                        result = "{}";
                    }
                }
            }

            return result;
        } catch (IOException ex) {
            logger.log(Level.SEVERE, null, ex);
        }

        return result;
    }

    private User upsertUser(User admin) {
        return (admin != null) ? new UserDaoImpl().upsert(admin) : null;
    }

    private Location upsertLocation(Location location) {
        return (location != null) ? new LocationDaoImpl().upsert(location) : null;
    }

    private User readAdmin(String _user, String _admin) {
        logger.log(Level.INFO, "Admin={0}\n{1}", new Object[]{_user, _admin});

        @SuppressWarnings("UnusedAssignment")
        User admin = null;

        try {
            if (!Strings.isNullOrEmpty(_admin)) {
                GsonBuilder gsonBuilder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();

                gsonBuilder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {

                    @Override
                    public Date deserialize(JsonElement json, Type type, JsonDeserializationContext jdc) throws
                            JsonParseException {
                        return new Date(json.getAsJsonPrimitive().getAsLong());
                    }
                });

                Gson gson = gsonBuilder.create();
                final User user = gson.fromJson(_user, User.class);

                if (user != null && !Strings.isNullOrEmpty(user.getName()) && !Strings.isNullOrEmpty(user.getSurname())
                        && !Strings.isNullOrEmpty(user.getUsername()) && !Strings.isNullOrEmpty(user.getPassword())) {
                    final Map<String, Object> parametersUsername = new HashMap<String, Object>() {
                        {
                            put("username", user.getUsername());
                        }
                    };
                    final Map<String, Object> parametersEmail = new HashMap<String, Object>() {
                        {
                            put("email", user.getEmail());
                        }
                    };

                    User dbuserUsername = new UserDaoImpl().getSingleByQuery("com.buzzlogix.entity.User.findUserByUsername", parametersUsername);
                    User dbuserEmail = new UserDaoImpl().getSingleByQuery("com.buzzlogix.entity.User.findUserByEmail", parametersEmail);

                    if (dbuserUsername != null || dbuserEmail != null) {
                        logger.log(Level.SEVERE, "The user is exist: username={0}, password={1}",
                                new Object[]{user.getUsername(), user.getPassword()});

                        return null;
                    }

                    admin = gson.fromJson(_admin, User.class);

                    if (admin != null) {
                        if (user != null) {
                            admin.setEmail(user.getEmail());
                            admin.setName(user.getName());
                            admin.setPassword(user.getPassword());
                            admin.setSurname(user.getSurname());
                            admin.setUsername(user.getUsername());
                        }
                        admin.setPassword(Security.encryption(Security.SHA256, admin.getPassword()));
                        if (admin.getAccount() != null) {
                            admin.getAccount().setId(UUID.randomUUID().toString());
                            if (user.getAccount() != null) {
                                admin.getAccount().setName(user.getAccount().getName());
                            }
                            if (admin.getAccount().getBilling() != null) {
                                admin.getAccount().getBilling().setId(UUID.randomUUID().toString());
                                if (admin.getAccount().getBilling().getLicence() != null) {
                                    admin.getAccount().getBilling().getLicence().setId(UUID.randomUUID().toString());
                                }
                                if (admin.getAccount().getBilling().getLimit() != null) {
                                    admin.getAccount().getBilling().getLimit().setId(UUID.randomUUID().toString());
                                }
                            }
                            Set<Group> groups = admin.getAccount().getGroups();

                            if (groups != null && !groups.isEmpty()) {
                                for (Group group : groups) {
                                    group.setId(UUID.randomUUID().toString());
//                                    if (user.getAccount() != null) {
//                                        Set<Group> userGroups = user.getAccount().getGroups();
//
//                                        if (userGroups != null && !userGroups.isEmpty()) {
//                                            group.setName(userGroups.iterator().next().getName());
//                                        }
//                                    }
                                    Set<User> users = group.getUsers();
                                    Set<Channel> channels = group.getChannels();
                                    Set<Dashboard> dashboards = group.getDashboards();
                                    Set<Publisher> publishers = group.getPublishers();
                                    Set<TopicStream> topics = group.getTopics();

                                    if (users != null && !users.isEmpty()) {
                                        for (User subuser : users) {
                                            subuser.setId(UUID.randomUUID().toString());
                                            subuser.setPassword(Security.encryption(Security.SHA256, user.getPassword()));
                                        }
                                    }
                                    if (channels != null && !channels.isEmpty()) {
                                        for (Channel channel : channels) {
                                            channel.setId(UUID.randomUUID().toString());
                                        }
                                    }
                                    if (dashboards != null && !dashboards.isEmpty()) {
                                        for (Dashboard dashboard : dashboards) {
                                            dashboard.setId(UUID.randomUUID().toString());
                                            if (dashboard.getReport() != null) {
                                                dashboard.getReport().setId(UUID.randomUUID().toString());
                                            }
                                            Set<Widget> widgets = dashboard.getWidgets();

                                            if (widgets != null && !widgets.isEmpty()) {
                                                for (Widget widget : widgets) {
                                                    widget.setId(UUID.randomUUID().toString());
                                                }
                                            }
                                        }
                                    }
                                    if (topics != null && !topics.isEmpty()) {
                                        for (TopicStream topic : topics) {
                                            topic.setId(UUID.randomUUID().toString());
                                            topic.setStatus(StreamStatusEnum.STOPPED);
                                        }
                                    }
                                    if (publishers != null && !publishers.isEmpty()) {
                                        for (Publisher publisher : publishers) {
                                            publisher.setId(UUID.randomUUID().toString());
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, null, ex);
        }

        return admin;
    }

    private String addLocation(BufferedReader reader) {
        String result = null;

        try {
            String location = "", line;

            if (reader.ready()) {
                while ((line = reader.readLine()) != null) {
                    location += line;
                }
            }
            if (!Strings.isNullOrEmpty(location)) {
                Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                Location[] locations = gson.fromJson(location, Location[].class);

                logger.log(Level.INFO, "Size location={0}", locations.length);
                
                if (locations != null && locations.length > 0) {
                    Integer count = 1;
                    
                    for (Location l : locations) {
                        Location dbl = upsertLocation(l);

                        if (dbl != null) {
                            result += gson.toJson(dbl) + "\n";
                            count++;
                        }
                    }
                    
                    logger.log(Level.INFO, "db inserted locations={0}", count);
                }
            } else {
                logger.log(Level.INFO, "no read any location");
            }

            return result;
        } catch (IOException ex) {
            logger.log(Level.SEVERE, null, ex);
        }

        return result;
    }
}

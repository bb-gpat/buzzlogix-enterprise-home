/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.service;

import com.google.api.client.util.Strings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.buzzlogix.dao.account.AccountDaoImpl;
import com.buzzlogix.entity.Account;
import com.buzzlogix.urls.AccountURL;
import com.buzzlogix.utils.gson.RegisterTypeAdapter;
import com.buzzlogix.utils.impl.LoggerImpl;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author administrator
 */
public class AccountServlet extends HttpServlet {

    private static final Logger logger = new LoggerImpl().getLoggerByHandler(AccountServlet.class.getName(), new ConsoleHandler());

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) {
        String result = null;
        resp.setContentType("application/json");

        try {
            PrintWriter out = resp.getWriter();

            switch (req.getPathInfo()) {
                case AccountURL.READ:
                    result = read(req.getParameter("id"));
                    break;
            }

            out.print(result);
            out.flush();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public final void doPost(final HttpServletRequest req, final HttpServletResponse resp) {
        String result = null;
        resp.setContentType("application/json");

        try {
            PrintWriter out = resp.getWriter();

            switch (req.getPathInfo()) {
                case AccountURL.UPDATE:
                    result = update(req.getReader());
                    break;
                case AccountURL.DELETE:
                    result = delete(req.getParameter("id"));
                    break;
            }

            out.print(result);
            out.flush();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        }
    }

    private String read(final String accountId) {
        String result = null;

        logger.log(Level.INFO, "accountKey={0}", accountId);

        try {
            if (!Strings.isNullOrEmpty(accountId)) {
                final Map<String, Object> parameters = new HashMap<String, Object>() {
                    {
                        put("id", accountId);
                    }
                };

                Account a = new AccountDaoImpl().getSingleByQuery("com.buzzlogix.entity.Account.findById", parameters);

                if (a != null) {
                    result = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(a);
                } else {
                    logger.log(Level.INFO, "cannot read the account");
                }
            } else {
                logger.log(Level.INFO, "Must given a account key");
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }

    private String update(BufferedReader reader) {
        String result = null;
        String account = "", line;

        try {
            if (reader.ready()) {
                while ((line = reader.readLine()) != null) {
                    account += line;
                }

                if (!Strings.isNullOrEmpty(account)) {
                    logger.log(Level.INFO, "update account={0}", account);

                    Gson gson = new GsonBuilder()
                            .excludeFieldsWithoutExposeAnnotation()
                            .registerTypeAdapter(Date.class, RegisterTypeAdapter.deserDate)
                            .create();
                    Account a = gson.fromJson(account, Account.class);

                    if (a != null) {
                        a = new AccountDaoImpl().upsert(a);

                        if (a != null) {
                            result = gson.toJson(a);
                        } else {
                            logger.log(Level.INFO, "cannot update the account");
                        }
                    } else {
                        logger.log(Level.INFO, "isn't correct the account format json");
                    }
                } else {
                    logger.log(Level.INFO, "Must given a account format json");
                }
            } else {
                logger.log(Level.INFO, "Cannot read json data");
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }

    private String delete(final String accountId) {
        String result = null;

        logger.log(Level.INFO, "delete account with id={0}", accountId);

        try {
            if (!Strings.isNullOrEmpty(accountId)) {
                final Map<String, Object> parameters = new HashMap<String, Object>() {
                    {
                        put("id", accountId);
                    }
                };

                Integer deleted = new AccountDaoImpl().delete("com.buzzlogix.entity.Account.findById", parameters);

                if (deleted != null) {
                    result = deleted.toString();
                    logger.log(Level.INFO, "deleted the account with id={0}", accountId);
                } else {
                    logger.log(Level.INFO, "cannot deleted the account");
                }
            } else {
                logger.log(Level.INFO, "Must given a account id");
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }
}

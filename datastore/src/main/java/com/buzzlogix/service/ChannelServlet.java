/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.service;

import com.google.api.client.util.Strings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.buzzlogix.dao.channel.ChannelDaoImpl;
import com.buzzlogix.entity.Channel;
import com.buzzlogix.urls.ChannelURL;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author administrator
 */
public class ChannelServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(ChannelServlet.class.getName());

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) {
        String result = null;
        resp.setContentType("application/json");

        try {
            PrintWriter out = resp.getWriter();

            switch (req.getPathInfo()) {
                case ChannelURL.READ:
                    result = read(req.getParameter("id"));
                    break;
            }

            out.print(result);
            out.flush();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public final void doPost(final HttpServletRequest req, final HttpServletResponse resp) {
        String result = null;
        resp.setContentType("application/json");

        try {
            PrintWriter out = resp.getWriter();

            switch (req.getPathInfo()) {
                case ChannelURL.UPDATE:
                    result = update(req.getReader());
                    break;
                case ChannelURL.DELETE:
                    result = delete(req.getParameter("id"));
                    break;
            }

            out.print(result);
            out.flush();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        }
    }

    private String read(final String channelId) {
        String result = null;

        logger.log(Level.ALL, "channel Id={0}", channelId);

        try {
            if (!Strings.isNullOrEmpty(channelId)) {
                final Map<String, Object> parameters = new HashMap<String, Object>() {
                    {
                        put("id", channelId);
                    }
                };

                Channel c = new ChannelDaoImpl().getSingleByQuery("com.buzzlogix.entity.Channel.findById", parameters);

                if (c != null) {
                    result = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(c);
                } else {
                    logger.log(Level.INFO, "cannot read the channel");
                }
            } else {
                logger.log(Level.INFO, "Must given a channel id");
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }

    private String update(BufferedReader reader) {
        String result = null;
        String channel = "", line;

        try {
            if (reader.ready()) {
                while ((line = reader.readLine()) != null) {
                    channel += line;
                }

                if (!Strings.isNullOrEmpty(channel)) {
                    logger.log(Level.INFO, "update channel={0}", channel);
                    Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

                    Channel c = gson.fromJson(channel, Channel.class);

                    if (c != null) {
                        c = new ChannelDaoImpl().upsert(c);

                        if (c != null) {
                            result = gson.toJson(c);
                        } else {
                            logger.log(Level.INFO, "cannot update the channel");
                        }
                    } else {
                        logger.log(Level.INFO, "isn't correct the channel format json");
                    }
                }
            } else {
                logger.log(Level.INFO, "Must given a channel format json");
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }

    private String delete(final String channelId) {
        String result = null;

        logger.log(Level.INFO, "delete channel with id={0}", channelId);

        try {
            if (!Strings.isNullOrEmpty(channelId)) {
                final Map<String, Object> parameters = new HashMap<String, Object>() {
                    {
                        put("id", channelId);
                    }
                };

                Integer deleted = new ChannelDaoImpl().delete("com.buzzlogix.entity.Channel.findById", parameters);

                if (deleted != null) {
                    result = deleted.toString();
                    logger.log(Level.INFO, "deleted the channel with id={0}", channelId);
                } else {
                    logger.log(Level.INFO, "cannot deleted the channel");
                }
            } else {
                logger.log(Level.INFO, "Must given a channel id");
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }
}

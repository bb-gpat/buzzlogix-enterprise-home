package com.buzzlogix.service;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.cache.Cache;
import javax.cache.CacheException;
import javax.cache.CacheFactory;
import javax.cache.CacheManager;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.modules.ModulesServiceFactory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.buzzlogix.dao.topic.TopicStreamDaoImpl;
import com.buzzlogix.entity.TopicStream;
import com.buzzlogix.urls.KeepAliveURL;
import com.buzzlogix.utils.impl.HttpClientImpl;
import com.buzzlogix.utils.impl.LoggerImpl;
import java.io.PrintWriter;
import java.util.logging.ConsoleHandler;

public class KeepAliveServlet extends HttpServlet {

    private Logger logger = new LoggerImpl().getLoggerByHandler(this.getClass().getName(), new ConsoleHandler());

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, MalformedURLException {
        String result = null;
        resp.setContentType("application/json");

        try {
            PrintWriter out = resp.getWriter();

            switch (req.getPathInfo()) {
                case KeepAliveURL.DISTRINUTOR: result = startDistributor();
                    break;
            }

            out.print(result);
            out.flush();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        }
    }

    private String startDistributor() {
        String result = "OK";
        List<TopicStream> all_streams = new TopicStreamDaoImpl().getListByQuery("com.buzzlogix.entity.TopicStream.findAll", null, 0, 10);
        Cache cache = null;

        try {
            CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
            cache = cacheFactory.createCache(Collections.emptyMap());
        } catch (CacheException e) {
        }

        if (cache != null) {
            for (TopicStream stream : all_streams) {
                if (!cache.containsKey(stream.getId())) {
                    try {
                        
                        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                        String target_host = ModulesServiceFactory.getModulesService().getVersionHostname("distributor", "v1");
                        String protocol = (Boolean.valueOf(System.getProperty("Local"))) ? "http://" : "https://";
                        new HttpClientImpl().post(new URL(protocol + target_host + "/topicStream/handle"), gson.toJson(stream), null);

                        //logger.log(Level.INFO, "target for /topicStream/handle URL is "+target_host );
                        //TODO Then the Distributor should keep updating the value with timeout set in memcache as long as teh stream is running
                        //cache.put(stream.getId(), new String("Some VM Instance").getBytes());
                    } catch (MalformedURLException ex) {
                        logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
                    }
                }
            }
        } else {
            try {
                throw new ServletException("Failed to retrieve cache");
            } catch (ServletException ex) {
                logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
            }
        }

        return result;
    }
}

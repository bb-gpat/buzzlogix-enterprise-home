/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.service;

import com.google.api.client.util.Strings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.buzzlogix.dao.limit.LimitDaoImpl;
import com.buzzlogix.entity.Limit;
import com.buzzlogix.urls.LimitURL;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author administrator
 */
public class LimitServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(LimitServlet.class.getName());

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) {
        String result = null;
        resp.setContentType("application/json");

        try {
            PrintWriter out = resp.getWriter();

            switch (req.getPathInfo()) {
                case LimitURL.READ:
                    result = read(req.getParameter("id"));
                    break;
            }

            out.print(result);
            out.flush();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public final void doPost(final HttpServletRequest req, final HttpServletResponse resp) {
        String result = null;
        resp.setContentType("application/json");

        try {
            PrintWriter out = resp.getWriter();

            switch (req.getPathInfo()) {
                case LimitURL.UPDATE:
                    result = update(req.getReader());
                    break;
                case LimitURL.DELETE:
                    result = delete(req.getParameter("id"));
                    break;
            }

            out.print(result);
            out.flush();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        }
    }

    private String read(final String limitId) {
        String result = null;

        logger.log(Level.INFO, "limitId={0}", limitId);

        try {
            if (!Strings.isNullOrEmpty(limitId)) {
                final Map<String, Object> parameters = new HashMap<String, Object>() {
                    {
                        put("id", limitId);
                    }
                };

                Limit l = new LimitDaoImpl().getSingleByQuery("com.buzzlogix.entity.Limit.findById", parameters);

                if (l != null) {
                    result = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(l);
                } else {
                    logger.log(Level.INFO, "cannot read the limit");
                }
            } else {
                logger.log(Level.INFO, "Must given a limit id");
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }

    private String update(BufferedReader reader) {
        String result = null;
        String limit = "", line;

        try {
            if (reader.ready()) {
                while ((line = reader.readLine()) != null) {
                    limit += line;
                }

                if (!Strings.isNullOrEmpty(limit)) {
                    logger.log(Level.INFO, "update limit={0}", limit);

                    Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

                    Limit l = gson.fromJson(limit, Limit.class);

                    if (l != null) {
                        l = new LimitDaoImpl().upsert(l);

                        if (l != null) {
                            result = gson.toJson(l);
                        } else {
                            logger.log(Level.INFO, "cannot update the limit");
                        }
                    } else {
                        logger.log(Level.INFO, "isn't correct the limit format json");
                    }
                }
            } else {
                logger.log(Level.INFO, "Must given a limit format json");
            }
        } catch (IOException | JsonSyntaxException ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }

    private String delete(final String limitId) {
        String result = null;

        logger.log(Level.INFO, "delete limit with id={0}", limitId);

        try {
            if (!Strings.isNullOrEmpty(limitId)) {
                final Map<String, Object> parameters = new HashMap<String, Object>() {
                    {
                        put("id", limitId);
                    }
                };

                Integer deleted = new LimitDaoImpl().delete("com.buzzlogix.entity.Limit.findById", parameters);

                if (deleted != null) {
                    result = deleted.toString();
                    logger.log(Level.INFO, "deleted the limit with id={0}", limitId);
                } else {
                    logger.log(Level.INFO, "cannot deleted the limit");
                }
            } else {
                logger.log(Level.INFO, "Must given a limit id");
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.service;

import com.google.api.client.util.Strings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.buzzlogix.dao.task.TaskDaoImpl;
import com.buzzlogix.entity.Task;
import com.buzzlogix.urls.TaskURL;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author administrator
 */
public class TaskServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(TaskServlet.class.getName());

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) {
        String result = null;
        resp.setContentType("application/json");

        try {
            PrintWriter out = resp.getWriter();

            switch (req.getPathInfo()) {
                case TaskURL.READ:
                    result = read(req.getParameter("id"));
                    break;
            }

            out.print(result);
            out.flush();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public final void doPost(final HttpServletRequest req, final HttpServletResponse resp) {
        String result = null;
        resp.setContentType("application/json");

        try {
            PrintWriter out = resp.getWriter();

            switch (req.getPathInfo()) {
                case TaskURL.INSERT:
                    result = insert(req.getReader());
                    break;
                case TaskURL.DELETE:
                    result = delete(req.getParameter("id"));
                    break;
            }

            out.print(result);
            out.flush();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getMessage());
        }
    }

    private String read(final String taskId) {
        String result = null;

        logger.log(Level.INFO, "taskId={0}", taskId);

        try {
            if (!Strings.isNullOrEmpty(taskId)) {
                final Map<String, Object> parameters = new HashMap<String, Object>() {
                    {
                        put("id", taskId);
                    }
                };

                Task t = new TaskDaoImpl().getSingleByQuery("com.buzzlogix.entity.Task.findById", parameters);

                if (t != null) {
                    result = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(t);
                } else {
                    logger.log(Level.INFO, "cannot read the task");
                }
            } else {
                logger.log(Level.INFO, "Must given a task id");
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }

    private String insert(BufferedReader reader) {
        String result = null;
        String task = "", line;

        try {
            if (reader.ready()) {
                while ((line = reader.readLine()) != null) {
                    task += line;
                }

                if (!Strings.isNullOrEmpty(task)) {
                    logger.log(Level.INFO, "insert task={0}", task);

                    Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

                    Task t = gson.fromJson(task, Task.class);

                    if (t != null) {
                        t = new TaskDaoImpl().insert(t);

                        if (t != null) {
                            result = gson.toJson(t);
                        } else {
                            logger.log(Level.INFO, "cannot insert the task");
                        }
                    } else {
                        logger.log(Level.INFO, "isn't correct the task format json");
                    }
                }
            } else {
                logger.log(Level.INFO, "Must given a task format json");
            }
        } catch (IOException | JsonSyntaxException ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }

    private String delete(final String taskId) {
        String result = null;

        logger.log(Level.INFO, "delete task with id={0}", taskId);

        try {
            if (!Strings.isNullOrEmpty(taskId)) {
                final Map<String, Object> parameters = new HashMap<String, Object>() {
                    {
                        put("id", taskId);
                    }
                };

                Integer deleted = new TaskDaoImpl().delete("com.buzzlogix.entity.Task.findById", parameters);

                if (deleted != null) {
                    result = deleted.toString();
                    logger.log(Level.INFO, "deleted the task with id={0}", taskId);
                } else {
                    logger.log(Level.INFO, "cannot deleted the task");
                }
            } else {
                logger.log(Level.INFO, "Must given a task id");
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }
}

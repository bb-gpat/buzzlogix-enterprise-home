/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.service;

import com.google.api.client.util.Strings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.buzzlogix.dao.publisher.PublisherDaoImpl;
import com.buzzlogix.entity.Publisher;
import com.buzzlogix.urls.PublisherURL;
import com.buzzlogix.utils.impl.LoggerImpl;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author administrator
 */
public class PublisherServlet extends HttpServlet {

    private static final Logger logger = new LoggerImpl().getLoggerByHandler(PublisherServlet.class.getName(), new ConsoleHandler());

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) {
        String result = null;
        resp.setContentType("application/json");

        try {
            PrintWriter out = resp.getWriter();

            switch (req.getPathInfo()) {
                case PublisherURL.READ:
                    result = read(req.getParameter("id"));
                    break;
            }

            out.print(result);
            out.flush();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public final void doPost(final HttpServletRequest req, final HttpServletResponse resp) {
        String result = null;
        resp.setContentType("application/json");

        try {
            PrintWriter out = resp.getWriter();

            switch (req.getPathInfo()) {
                case PublisherURL.UPDATE:
                    result = update(req.getReader());
                    break;
                case PublisherURL.DELETE:
                    result = delete(req.getParameter("id"));
                    break;
            }

            out.print(result);
            out.flush();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        }
    }

    private String read(final String publisherId) {
        String result = null;

        logger.log(Level.INFO, "get publisher with id={0}", publisherId);

        try {
            if (!Strings.isNullOrEmpty(publisherId)) {
                final Map<String, Object> parameters = new HashMap<String, Object>() {
                    {
                        put("id", publisherId);
                    }
                };

                Publisher p = new PublisherDaoImpl().getSingleByQuery("com.buzzlogix.entity.Publisher.findById", parameters);

                if (p != null) {
                    result = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(p);
                } else {
                    logger.log(Level.INFO, "cannot read the publisher");
                }
            } else {
                logger.log(Level.INFO, "Must given a publisher id");
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }

    private String delete(final String publisherId) {
        String result = null;

        logger.log(Level.INFO, "delete publisher with id={0}", publisherId);

        try {
            if (!Strings.isNullOrEmpty(publisherId)) {
                final Map<String, Object> parameters = new HashMap<String, Object>() {
                    {
                        put("id", publisherId);
                    }
                };

                Integer deleted = new PublisherDaoImpl().delete("com.buzzlogix.entity.Publisher.findById", parameters);

                if (deleted != null) {
                    result = deleted.toString();
                    logger.log(Level.INFO, "deleted the publisher with id={0}", publisherId);
                } else {
                    logger.log(Level.INFO, "cannot deleted the publisher");
                }
            } else {
                logger.log(Level.INFO, "Must given a publisher id");
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }

    private String update(BufferedReader reader) {
        String result = null;
        String publisher = "", line;

        try {
            if (reader.ready()) {
                while ((line = reader.readLine()) != null) {
                    publisher += line;
                }

                if (!Strings.isNullOrEmpty(publisher)) {
                    logger.log(Level.INFO, "update limit={0}", publisher);

                    Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

                    Publisher p = gson.fromJson(publisher, Publisher.class);

                    if (p != null) {
                        p = new PublisherDaoImpl().upsert(p);

                        if (p != null) {
                            result = gson.toJson(p);
                        } else {
                            logger.log(Level.INFO, "cannot update the publisher");
                        }
                    } else {
                        logger.log(Level.INFO, "isn't correct the publisher format json");
                    }
                }
            } else {
                logger.log(Level.INFO, "Must given a publisher format json");
            }
        } catch (IOException | JsonSyntaxException ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }
}

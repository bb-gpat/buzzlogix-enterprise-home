/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.service;

import com.google.api.client.util.Strings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.buzzlogix.dao.user.UserDaoImpl;
import com.buzzlogix.dao.widget.WidgetDaoImpl;
import com.buzzlogix.entity.Widget;
import com.buzzlogix.urls.WidgetURL;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author administrator
 */
public class WidgetServlet extends HttpServlet {
    
    private static final Logger logger = Logger.getLogger(WidgetServlet.class.getName());
    
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) {
        String result = null;
        resp.setContentType("application/json");

        try {
            PrintWriter out = resp.getWriter();

            switch (req.getPathInfo()) {
                case WidgetURL.READ: result = read(req.getParameter("id"));
                    break;
                case WidgetURL.DELETE: result = delete(req.getParameter("id"));
                    break;
            }

            out.print(result);
            out.flush();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public final void doPost(final HttpServletRequest req, final HttpServletResponse resp) {
        String result = null;
        resp.setContentType("application/json");

        try {
            PrintWriter out = resp.getWriter();

            switch (req.getPathInfo()) {
                case WidgetURL.UPDATE: result = update(req.getReader());
                    break;
            }

            out.print(result);
            out.flush();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        }
    }

    private String read(final String widgetId) {
        String result = null;

        logger.log(Level.INFO, "widgetId={0}", widgetId);

        try {
            if (!Strings.isNullOrEmpty(widgetId)) {
                final Map<String, Object> parameters = new HashMap<String, Object>() {
                    {
                        put("id", widgetId);
                    }
                };

                Widget w = new WidgetDaoImpl().getSingleByQuery("com.buzzlogix.entity.Widget.findById", parameters);

                if (w != null) {
                    result = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(w);
                } else {
                    logger.log(Level.INFO, "cannot read the widget");
                }
            } else {
                logger.log(Level.INFO, "Must given a widget id");
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }

    private String update(BufferedReader reader) {
        String result = null;
        String widget = "", line;

        try {
            if (reader.ready()) {
                while ((line = reader.readLine()) != null) {
                    widget += line;
                }

                if (!Strings.isNullOrEmpty(widget)) {
                    logger.log(Level.INFO, "update widget={0}", widget);
                    
                    Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                    Widget w = gson.fromJson(widget, Widget.class);

                    if (w != null) {
                        w = new WidgetDaoImpl().upsert(w);

                        if (w != null) {
                            result = gson.toJson(w);
                        } else {
                            logger.log(Level.INFO, "cannot update the widget");
                        }
                    } else {
                        logger.log(Level.INFO, "isn't correct the widget format json");
                    }
                }
            } else {
                logger.log(Level.INFO, "Must given a widget format json");
            }
        } catch (IOException | JsonSyntaxException ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }
    
    private String delete(final String widgetId) {
        String result = null;

        logger.log(Level.INFO, "delete widget with id={0}", widgetId);

        try {
            if (!Strings.isNullOrEmpty(widgetId)) {
                final Map<String, Object> parameters = new HashMap<String, Object>() {
                    {
                        put("id", widgetId);
                    }
                };

                Integer deleted = new WidgetDaoImpl().delete("com.buzzlogix.entity.Widget.findById", parameters);

                if (deleted != null) {
                    result = deleted.toString();
                    logger.log(Level.INFO, "deleted the widget with id={0}", widgetId);
                } else {
                    logger.log(Level.INFO, "cannot deleted the widget");
                }
            } else {
                logger.log(Level.INFO, "Must given a widget id");
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        } finally {
            return result;
        }
    }
}

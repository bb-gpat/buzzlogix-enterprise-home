/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.urls;

/**
 *
 * @author administrator
 */
public final class UserURL {

    public static final String LOGIN = "/login";
    
    public static final String LOGIN_EXIST = "/login-exist";
    
    public static final String LOGIN_EXIST_EMAIL = "/login-exist-email";

    public static final String CREATE_TASK = "/create-task";
    
    public static final String READ = "/read";
    
    public static final String READ_ACCOUNT = "/read/account";

    public static final String UPDATE = "/update";
    
    public static final String DELETE = "/delete";
}

package com.buzzlogix.urls;

public class GenericPostURL {

    public static final String UPSERT = "/upsert";
    
    public static final String UPSERT_LOCAL = "/upsert-local";
    
    public static final String READ = "/read";
    
    public static final String DELETE = "/delete";
    
    public static final String CREATE = "/create";
    
    public static final String COUNT = "/count";
}

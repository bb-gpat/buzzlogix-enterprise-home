/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.urls;

/**
 *
 * @author administrator
 */
public final class WorkGroupURL {

    public static final String READ_ID = "/read-id";
    
    public static final String READ_NAME = "/read-name";

    public static final String UPDATE = "/update";

    public static final String CREATE_USER = "/create-user";

    public static final String DELETE_USER = "/delete-user";
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.urls;

/**
 *
 * @author administrator
 */
public class LocationURL {

    public static final String READ_ALL = "/read-all";

    public static final String UPSERT = "/upsert";

    public static final String DELETE = "/delete";
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.pubsub;

/**
 *
 * @author administrator
 *
 * Enum representing a resource type of topics
 */
public enum ResourceTypeEnum {

    /**
     * Represents topics.
     */
    TOPIC("topics"),
    /**
     * Represents subscriptions.
     */
    SUBSCRIPTION("subscriptions"),
    /**
     * Channels
     */
    FRONT_END("front-end"),
    DISTRIBUTOR("distributor"),
    PARSER("parser"),
    DATASTORE("datastore"),
    MYTOPIC("mytopic"),
    MYSUBSCRIPTION("mysubscription");

    /**
     * A path representation for the resource.
     */
    private String collectionName;

    /**
     * A constructor.
     *
     * @param collectionName String representation of the resource.
     */
    private ResourceTypeEnum(final String collectionName) {
        this.collectionName = collectionName;
    }

    /**
     * Returns its collection name.
     *
     * @return the collection name.
     */
    public String getCollectionName() {
        return this.collectionName;
    }
}

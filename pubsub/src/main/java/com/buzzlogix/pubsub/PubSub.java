/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.pubsub;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonParser;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.pubsub.Pubsub;
import com.google.api.services.pubsub.model.AcknowledgeRequest;
import com.google.api.services.pubsub.model.ListSubscriptionsResponse;
import com.google.api.services.pubsub.model.ListTopicsResponse;
import com.google.api.services.pubsub.model.PublishRequest;
import com.google.api.services.pubsub.model.PublishResponse;
import com.google.api.services.pubsub.model.PubsubMessage;
import com.google.api.services.pubsub.model.PullRequest;
import com.google.api.services.pubsub.model.PullResponse;
import com.google.api.services.pubsub.model.ReceivedMessage;
import com.google.api.services.pubsub.model.Subscription;
import com.google.api.services.pubsub.model.Topic;
import com.google.common.collect.ImmutableList;
import com.buzzlogix.utils.impl.XMLConfigurationImpl;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author administrator
 *
 * Utility class to interact with the Pub/Sub API.
 *
 */
public class PubSub {

    private static final Logger logger = Logger.getLogger(PubSub.class.getName());

    /**
     * The project name will be attached to the pubsub client.
     */
    private String PROJECT;

    /**
     * Acknowledge deadline in second
     */
    private Integer DEADLINE;

    /**
     * Pull batch size. Collect the message up to the size of MaxEvents
     */
    private Integer BATCH_SIZE;

    private String MESSAGE_CACHE_KEY;

    private String BASE_PACKAGE;

    private PubSubTopic topic;

    private PubSubSubscription subscription;

    /**
     * Prevents instantiation.
     */
    public PubSub(String configPath) {
        XMLConfigurationImpl config = new XMLConfigurationImpl(configPath + "pubsub-config.xml");

        PROJECT = config.getString("project-name");
        DEADLINE = config.getInteger("deadtline");
        BATCH_SIZE = config.getInteger("batchsize");
        MESSAGE_CACHE_KEY = config.getString("message-cache-key");
        BASE_PACKAGE = config.getString("base-package");
        topic = new PubSubTopic();
        subscription = new PubSubSubscription();

        logger.log(Level.INFO,
                   "\nRead Project Parametes\n----------------------\nProjectName:{0}\nDeadLine:{1}\nBatchSize:{2} \nMessageCacheKey:{3}\nBasePackage:{4}\n",
                   new Object[]{PROJECT, DEADLINE, BATCH_SIZE, MESSAGE_CACHE_KEY, BASE_PACKAGE});
    }

    public String getPROJECT() {
        return PROJECT;
    }

    public Integer getDEADLINE() {
        return DEADLINE;
    }

    public Integer getBATCH_SIZE() {
        return BATCH_SIZE;
    }

    public String getMESSAGE_CACHE_KEY() {
        return MESSAGE_CACHE_KEY;
    }

    public String getBASE_PACKAGE() {
        return BASE_PACKAGE;
    }

    public PubSub setBASE_PACKAGE(String BASE_PACKAGE) {
        this.BASE_PACKAGE = BASE_PACKAGE;

        return this;
    }

    public PubSubTopic getTopic() {
        return topic;
    }

    public PubSubSubscription getSubscription() {
        return subscription;
    }

    /**
     * Builds a new Pubsub client with default HttpTransport and JsonFactory and returns it.
     *
     * @return Pubsub client.
     * @throws IOException when we can not get the default credentials.
     * @throws java.security.GeneralSecurityException
     */
    public Pubsub getClient() throws IOException {
        try {
            return getClient(GoogleNetHttpTransport.newTrustedTransport(), JacksonFactory.getDefaultInstance());
        } catch (GeneralSecurityException ex) {
            Logger.getLogger(PubSub.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    /**
     * Builds a new Pubsub client and returns it.
     *
     * @param httpTransport HttpTransport for Pubsub client.
     * @param jsonFactory JsonFactory for Pubsub client.
     * @return Pubsub client.
     * @throws IOException when we can not get the default credentials.
     */
    public Pubsub getClient(final HttpTransport httpTransport, final JacksonFactory jsonFactory) throws IOException {
        GoogleCredential credential = GoogleCredential.getApplicationDefault();
        HttpRequestInitializer initializer = new RetryHttpInitializerWrapper(credential);

        return new Pubsub.Builder(httpTransport, jsonFactory, initializer).setApplicationName("buzzlogix-testing").
                build();
    }

    /**
     * Returns the fully qualified resource name for Pub/Sub.
     *
     * @param resourceType ResourceType.
     * @param resource topic name or subscription name.
     * @return A string in a form of PROJECT_NAME/RESOURCE_NAME
     */
    public String getFullyQualifiedResourceName(final ResourceTypeEnum resourceType, final ResourceTypeEnum resource) {
        return String.format("projects/%s/%s/%s", PROJECT, resourceType.getCollectionName(), resource.
                             getCollectionName());
    }

    public class PubSubTopic {

        /**
         * Creates a new topic with a given name.
         *
         * @param client Cloud Pub/Sub client.
         * @param resource Channel of topic.
         * @throws IOException when Cloud Pub/Sub API calls fail.
         */
        public void createTopic(final Pubsub client, final ResourceTypeEnum resource) throws IOException {
            String topicName = getFullyQualifiedResourceName(ResourceTypeEnum.TOPIC, resource);

            Topic topic = client.projects().topics().create(topicName, new Topic()).execute();

            logger.log(Level.INFO, "Topic{0} was created.\n", topic.getName());
        }

        /**
         * Publishes the given message to the given topic.
         *
         * @param client Cloud Pub/Sub client.
         * @param resource Channel of topic.
         * @param message Message to publish
         * @throws IOException when Cloud Pub/Sub API calls fail.
         */
        public void publishMessage(final Pubsub client, final ResourceTypeEnum resource, final String message) throws IOException {
            String topic = getFullyQualifiedResourceName(ResourceTypeEnum.TOPIC, resource);
            PubsubMessage pubsubMessage = new PubsubMessage().encodeData(message.getBytes("UTF-8"));
            List<PubsubMessage> messages = ImmutableList.of(pubsubMessage);
            PublishRequest publishRequest = new PublishRequest();
            publishRequest.setMessages(messages);
            PublishResponse publishResponse = client.projects().topics().publish(topic, publishRequest).execute();
            List<String> messageIds = publishResponse.getMessageIds();

            if (messageIds != null) {
                for (String messageId : messageIds) {
                    logger.log(Level.INFO, "Published with a message id: {0}" + messageId);
                }
            }
        }

        /**
         * Deletes a topic with the given name.
         *
         * @param client Cloud Pub/Sub client.
         * @param resource Channel of topic.
         * @throws IOException when Cloud Pub/Sub API calls fail.
         */
        public void deleteTopic(final Pubsub client, final ResourceTypeEnum resource) throws IOException {
            String topicName = getFullyQualifiedResourceName(ResourceTypeEnum.TOPIC, resource);

            client.projects().topics().delete(topicName).execute();

            logger.log(Level.INFO, "Topic {0} was deleted.\n", topicName);
        }

        /**
         * Lists existing topics in the project.
         *
         * @param client Cloud Pub/Sub client.
         * @param args Command line arguments.
         * @throws IOException when Cloud Pub/Sub API calls fail.
         */
        public void listTopics(final Pubsub client) throws IOException {
            String nextPageToken = null;
            boolean hasTopics = false;
            Pubsub.Projects.Topics.List listMethod = client.projects().topics().list("projects/" + PROJECT);

            do {
                if (nextPageToken != null) {
                    listMethod.setPageToken(nextPageToken);
                }

                ListTopicsResponse response = listMethod.execute();

                if (!response.isEmpty()) {
                    for (Topic topic : response.getTopics()) {
                        hasTopics = true;

                        logger.log(Level.INFO, "{0}", topic.getName());
                    }
                }
                nextPageToken = response.getNextPageToken();
            } while (nextPageToken != null);

            if (!hasTopics) {
                logger.log(Level.INFO, "There are no topics in the project {0}", PROJECT);
            }
        }
    }

    public class PubSubSubscription {

        /**
         * Creates a new subscription.
         *
         * @param client Cloud Pub/Sub client.
         * @param resourceSubscription Channel of subscription.
         * @param resourceTopic Channel of topic.
         * @throws IOException when Cloud Pub/Sub API calls fail.
         */
        public void createSubscription(final Pubsub client, final ResourceTypeEnum resourceSubscription,
                                       final ResourceTypeEnum resourceTopic) throws IOException {
            String subscriptionName = getFullyQualifiedResourceName(ResourceTypeEnum.SUBSCRIPTION, resourceSubscription);
            String topicName = getFullyQualifiedResourceName(ResourceTypeEnum.TOPIC, resourceTopic);
            Subscription subscription = new Subscription().setTopic(topicName).setAckDeadlineSeconds(DEADLINE);
            subscription = client.projects().subscriptions().create(subscriptionName, subscription).execute();

            logger.log(Level.INFO, "Subscription {0} was created.\n", subscription.getName());
            logger.log(Level.INFO, "{0}", subscription.toPrettyString());
        }

        /**
         * Keeps pulling messages from the given subscription.
         *
         * @param client Cloud Pub/Sub client.
         * @param resource Channel for subscription.
         * @throws IOException when Cloud Pub/Sub API calls fail.
         */
        public void pullMessages(final Pubsub client, final ResourceTypeEnum resource) throws IOException {
            String subscriptionName = getFullyQualifiedResourceName(ResourceTypeEnum.MYSUBSCRIPTION, resource);
            PullRequest pullRequest = new PullRequest().setReturnImmediately(false).setMaxMessages(BATCH_SIZE);

            PullResponse pullResponse;

            pullResponse = client.projects().subscriptions().pull(subscriptionName, pullRequest).execute();
            List<String> ackIds = new ArrayList<>(BATCH_SIZE);
            List<ReceivedMessage> receivedMessages = pullResponse.getReceivedMessages();

            if (receivedMessages != null) {
                for (ReceivedMessage receivedMessage : receivedMessages) {
                    PubsubMessage pubsubMessage = receivedMessage.getMessage();

                    if (pubsubMessage != null && pubsubMessage.decodeData() != null) {
                        logger.log(Level.INFO, new String(pubsubMessage.decodeData(), "UTF-8"));
                    }
                    ackIds.add(receivedMessage.getAckId());
                }
                AcknowledgeRequest ackRequest = new AcknowledgeRequest();
                ackRequest.setAckIds(ackIds);
                client.projects().subscriptions().acknowledge(subscriptionName, ackRequest).execute();
            } else {
                logger.log(Level.INFO, "No receive any message");
            }
        }

        public void pushMessages(final HttpServletRequest req, final HttpServletResponse resp) throws IOException {
            // Validating unique subscription token before processing the message
            String subscriptionToken = System.getProperty(BASE_PACKAGE + ".subscriptionUniqueToken");

            if (!subscriptionToken.equals(req.getParameter("token"))) {
                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                resp.getWriter().close();

                return;
            }

            ServletInputStream inputStream = req.getInputStream();

            // Parse the JSON message to the POJO model class
            JsonParser parser = JacksonFactory.getDefaultInstance().createJsonParser(inputStream);

            parser.skipToKey("message");

            PubsubMessage message = parser.parseAndClose(PubsubMessage.class);

            logger.log(Level.INFO, "message: ", new String(message.decodeData(), "UTF-8"));

            // Acknowledge the message by returning a success code
            resp.setStatus(HttpServletResponse.SC_OK);
            resp.getWriter().close();
        }

        /**
         * Lists existing subscriptions within a project.
         *
         * @param client Cloud Pub/Sub client.
         * @throws IOException when Cloud Pub/Sub API calls fail.
         */
        public void listSubscriptions(final Pubsub client) throws IOException {
            String nextPageToken = null;
            boolean hasSubscriptions = false;
            Pubsub.Projects.Subscriptions.List listMethod = client.projects().subscriptions().
                                               list("projects/" + PROJECT);

            do {
                if (nextPageToken != null) {
                    listMethod.setPageToken(nextPageToken);
                }

                ListSubscriptionsResponse response = listMethod.execute();

                if (!response.isEmpty()) {
                    for (Subscription subscription : response.getSubscriptions()) {
                        hasSubscriptions = true;

                        logger.log(Level.INFO, "{0}\n", subscription.toPrettyString());
                    }
                }
                nextPageToken = response.getNextPageToken();
            } while (nextPageToken != null);

            if (!hasSubscriptions) {
                logger.log(Level.INFO, "There are no subscriptions in the project {0}", PROJECT);
            }
        }

        /**
         * Deletes a subscription with a given name.
         *
         * @param client Cloud Pub/Sub client.
         * @param resource Channel for subscription.
         * @throws IOException when Cloud Pub/Sub API calls fail.
         */
        public void deleteSubscription(final Pubsub client, final ResourceTypeEnum resource) throws IOException {
            String subscriptionName = getFullyQualifiedResourceName(ResourceTypeEnum.SUBSCRIPTION, resource);

            client.projects().subscriptions().delete(subscriptionName).execute();

            logger.log(Level.INFO, "Subscription {0} was deleted.\n", subscriptionName);
        }
    }
}

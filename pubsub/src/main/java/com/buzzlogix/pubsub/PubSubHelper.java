package com.buzzlogix.pubsub;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.pubsub.Pubsub;
import com.google.api.services.pubsub.model.AcknowledgeRequest;
import com.google.api.services.pubsub.model.ListSubscriptionsResponse;
import com.google.api.services.pubsub.model.ListTopicsResponse;
import com.google.api.services.pubsub.model.PublishRequest;
import com.google.api.services.pubsub.model.PublishResponse;
import com.google.api.services.pubsub.model.PubsubMessage;
import com.google.api.services.pubsub.model.PullRequest;
import com.google.api.services.pubsub.model.PullResponse;
import com.google.api.services.pubsub.model.PushConfig;
import com.google.api.services.pubsub.model.ReceivedMessage;
import com.google.api.services.pubsub.model.Subscription;
import com.google.api.services.pubsub.model.Topic;
import com.google.appengine.api.modules.ModulesServiceFactory;
import com.google.common.collect.ImmutableList;
import com.buzzlogix.pubsub.PubSub.PubSubSubscription;
import com.buzzlogix.pubsub.PubSub.PubSubTopic;
import com.buzzlogix.utils.impl.XMLConfigurationImpl;

public class PubSubHelper {
	
	private Pubsub pubSubClient ;
	private final JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
	
	String projectPath = "projects/buzzlogix-development";
	String topicsPath = projectPath +"/topics/";
	String subscriptionsPath = projectPath +"/subscriptions/";
	
	public PubSubHelper()
	{
		init();
	}
	
	public PubSubHelper(String projectPath)
	{
		this.projectPath = projectPath;
		init();
	}
	public boolean init()
	{
		try {
			this.pubSubClient = createPubsubClient();
			return true;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		}
		return false;
	}

	private Pubsub createPubsubClient() throws IOException,
			GeneralSecurityException {
		HttpTransport transport = GoogleNetHttpTransport.newTrustedTransport();
		GoogleCredential credential = GoogleCredential.getApplicationDefault();
		HttpRequestInitializer initializer = new RetryHttpInitializerWrapper(
				credential);
		return new Pubsub.Builder(transport, jsonFactory, initializer)
				.build();

	}
	
	public void addTopic(String topicName) throws IOException
	{
		if (!topicExists(topicName)) {
			Topic newTopic = pubSubClient.projects().topics()
					.create(topicsPath + topicName, new Topic()).execute();
		}
	}
	
	public void addPollingSubscription(String topicName , String subscriptionName) throws IOException
	{
		if (!subscriptionExists(subscriptionName))
		{
			Subscription subscription = new Subscription()
				// The name of the topic from which this subscription receives messages
				.setTopic(topicsPath+topicName)
				// Ackowledgement deadline in second
				.setAckDeadlineSeconds(60);
	
			Subscription newSubscription = pubSubClient.projects()
				.subscriptions()
				.create(subscriptionsPath + subscriptionName, subscription).execute();
			System.out.println("Created: " + newSubscription.getName());
		}
	}
	
	public void addPushSubscription(String topicName ,  String subscriptionName) throws IOException
	{
		if (!subscriptionExists(subscriptionName))
		{
			//TODO Use ModulesServiceFactory to create endpoint.

			ModulesServiceFactory.getModulesService().getCurrentInstanceId();
			ModulesServiceFactory.getModulesService().getCurrentVersion();
			ModulesServiceFactory.getModulesService().getCurrentModule();
			
			String pushEndpoint = "https://myapp.appspot.com/pep";
			PushConfig pushConfig = new
			PushConfig().setPushEndpoint(pushEndpoint);
			Subscription subscription = new Subscription()
				// The name of the topic from which this subscription receives messages
				.setTopic(topicName)
				// Ackowledgement deadline in second
				.setAckDeadlineSeconds(60).setPushConfig(pushConfig);

			Subscription newSubscription = pubSubClient.projects()
				.subscriptions()
				.create(subscriptionName, subscription).execute();
			System.out.println("Created: " + newSubscription.getName());
		}
	}
	
	public boolean topicExists(String topicName) throws IOException
	{
		Pubsub.Projects.Topics.List listMethod = this.pubSubClient
				.projects().topics().list(projectPath);
		String nextPageToken = null;
		List<Topic> topics = null;
		ListTopicsResponse response;
		do {
			if (nextPageToken != null) {
				listMethod.setPageToken(nextPageToken);
			}
			response = listMethod.execute();
			if (topics == null)
				topics = response.getTopics();
			else
				topics.addAll(response.getTopics());

			nextPageToken = response.getNextPageToken();
		} while (nextPageToken != null);

		boolean exists = false;

		if (topics != null)
		for (Topic topic : topics) {
			System.out.println("Found topic: " + topic.getName());
			if (topic.getName().equals(topicsPath + topicName))
				exists = true;
		}
		
		return exists;
	}
	
	public boolean subscriptionExists(String subscriptionName) throws IOException
	{
		Pubsub.Projects.Subscriptions.List listMethod2 = pubSubClient
				.projects().subscriptions().list(projectPath);
		String nextPageToken = null;
		List<Subscription> subscriptions = null;
		ListSubscriptionsResponse response2;
		do {
			if (nextPageToken != null) {
				listMethod2.setPageToken(nextPageToken);
			}
			response2 = listMethod2.execute();
			if (subscriptions == null)
				subscriptions = response2.getSubscriptions();
			else
				subscriptions.addAll(response2.getSubscriptions());

			nextPageToken = response2.getNextPageToken();
		} while (nextPageToken != null);

		boolean exists = false;
		if (subscriptions != null)
		for (Subscription subscription : subscriptions) {
			System.out.println("Found subscription: "+ subscription.getName());
			if (subscription.getName().equals(subscriptionsPath + subscriptionName))
				exists = true;
		}
		return exists;
	}
	
	public void deleteSubscription(String subscriptionName) throws IOException
	{
		if (subscriptionExists(subscriptionName))
		{	pubSubClient.projects().subscriptions().delete(subscriptionsPath + subscriptionName).execute();
			System.out.println("Deleted subscription "+subscriptionsPath+subscriptionName);
		}
		else
			System.out.println("Failed to delete subscription "+subscriptionsPath+subscriptionName +"  subscription was not found");	
	}
	
	public void publishMessage(String message, String outputTopic)
			throws IOException {
		int maxLogMessageLength = 550;
		if (message.length() < maxLogMessageLength) {
			maxLogMessageLength = message.length();
		}
		System.out.println("Message Contents ....\n"
				+ message.substring(0, maxLogMessageLength));

		PubsubMessage pubsubMessage = new PubsubMessage();
		// You need to base64-encode your message with
		// PubsubMessage#encodeData() method.
       pubsubMessage.encodeData(message.getBytes("UTF-8"));

		List<PubsubMessage> messages = ImmutableList.of(pubsubMessage);
		PublishRequest publishRequest = new PublishRequest()
				.setMessages(messages);
		PublishResponse publishResponse = pubSubClient.projects().topics()
				.publish(topicsPath + outputTopic, publishRequest).execute();
		List<String> messageIds = publishResponse.getMessageIds();
		if (messageIds != null) {
			for (String messageId : messageIds) {
				System.out.println("messageId: " + messageId);
			}
		}
	}

	public List<ReceivedMessage> retrieveMessages(String subscriptionString) throws IOException {
		int batchSize = 10;
		PullRequest pullRequest = new PullRequest()
		// Setting ReturnImmediately to false instructs the API to
		// wait to collect the message up to the size of
		// MaxEvents, or until the timeout.
				.setReturnImmediately(true).setMaxMessages(batchSize);
		
		List<ReceivedMessage> receivedMessages = null;
		do {
			PullResponse pullResponse = pubSubClient.projects().subscriptions()
					.pull(subscriptionsPath+subscriptionString, pullRequest).execute();
			List<String> ackIds = new ArrayList<>(batchSize);
			if (receivedMessages == null)
				receivedMessages = pullResponse.getReceivedMessages();
			else
				receivedMessages.addAll(pullResponse.getReceivedMessages());
			if (receivedMessages == null || receivedMessages.isEmpty()) {
				// The result was empty.
				System.out.println("There were no messages for subscription." +subscriptionString);
				continue;
			}
			for (ReceivedMessage receivedMessage : receivedMessages) {
				PubsubMessage pubsubMessage = receivedMessage.getMessage();
				if (pubsubMessage != null) {
					System.out.print("Sent at : "+ pubsubMessage.get("publishTime") +"  Message: ");
					System.out.println(new String(pubsubMessage.decodeData(),
							"UTF-8"));
				}
				ackIds.add(receivedMessage.getAckId());
			}
			// Ack can be done asynchronously if you care about throughput.
			AcknowledgeRequest ackRequest = new AcknowledgeRequest()
					.setAckIds(ackIds);
			pubSubClient.projects().subscriptions()
					.acknowledge(subscriptionsPath + subscriptionString, ackRequest).execute();
			// You can keep pulling messages by changing the condition below.
		} while (false);
		
		return receivedMessages;
	}
	
	
	public List<Topic> getTopics() throws IOException
	{
		Pubsub.Projects.Topics.List listMethod = this.pubSubClient
				.projects().topics().list(projectPath);
		String nextPageToken = null;
		List<Topic> topics = null;
		ListTopicsResponse response;
		do {
			if (nextPageToken != null) {
				listMethod.setPageToken(nextPageToken);
			}
			response = listMethod.execute();
			if (topics == null)
				topics = response.getTopics();
			else
				topics.addAll(response.getTopics());

			nextPageToken = response.getNextPageToken();
		} while (nextPageToken != null);
		
		return topics;
	}
	
	public List<Subscription> getSubscriptions() throws IOException
	{
		Pubsub.Projects.Subscriptions.List listMethod2 = pubSubClient
				.projects().subscriptions().list(projectPath);
		String nextPageToken = null;
		List<Subscription> subscriptions = null;
		ListSubscriptionsResponse response2;
		do {
			if (nextPageToken != null) {
				listMethod2.setPageToken(nextPageToken);
			}
			response2 = listMethod2.execute();
			if (subscriptions == null)
				subscriptions = response2.getSubscriptions();
			else
				subscriptions.addAll(response2.getSubscriptions());

			nextPageToken = response2.getNextPageToken();
		} while (nextPageToken != null);
		return subscriptions;
	}
	
	public Pubsub getClient()
	{
		return this.pubSubClient;
	}
}

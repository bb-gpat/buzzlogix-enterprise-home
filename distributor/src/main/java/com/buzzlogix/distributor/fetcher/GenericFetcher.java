package com.buzzlogix.distributor.fetcher;

import java.util.concurrent.ExecutorService;

import com.buzzlogix.entity.TopicStream;

public abstract class GenericFetcher implements Runnable {

    public final static int FETCHER_TYPE_WEBHOSE = 0;

    public final static int FETCHER_TYPE_TWITTER = 1;

    // Reserve 20% of Task size for Headers and other Params
    protected final static int MAX_ALLOWED_BYTES_WEBHOSE = 82000;

    protected final static int MAX_ALLOWED_BYTES_TWITTER = 70000;

    // How many posts to include per task. 
    // Start with optimistic estimate and progressively
    // decrease if size exceeds MAX_ALLOWED_BYTES

    protected final int DEFAULT_CHUNK_SIZE_WEBHOSE = 10;

    protected final int DEFAULT_CHUNK_SIZE_TWITTER = 14;

    protected ExecutorService pool;

    protected TopicStream topicStream;

    public GenericFetcher(ExecutorService pool, TopicStream topicStream) {
        this.pool = pool;
        this.topicStream = topicStream;
    }

    protected void notifyMemcache() {
        if (!pool.isShutdown()) {
        }
    }

    public abstract int getType();
}

package com.buzzlogix.distributor.fetcher;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.modules.ModulesServiceFactory;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.gson.Gson;
import com.webhose.client.WebhoseClient;
import com.webhose.domain.WebhosePost;
import com.webhose.domain.WebhoseResponse;
import com.buzzlogix.distributor.Distributor;
import com.buzzlogix.entity.TopicStream;
import com.buzzlogix.utils.impl.LoggerImpl;
import com.buzzlogix.utils.list.Chopper;
import com.google.appengine.api.taskqueue.TransientFailureException;
import com.google.apphosting.api.ApiProxy;
import java.net.SocketTimeoutException;
import java.util.Arrays;
import java.util.List;

public class WebhoseFetcher extends GenericFetcher {

    private static final Logger logger = new LoggerImpl().getLoggerByHandler(WebhoseFetcher.class.getName(), new ConsoleHandler());

    public WebhoseFetcher(ExecutorService pool, TopicStream topicStream) {
        super(pool, topicStream);
    }

    @Override
    public int getType() {
        return GenericFetcher.FETCHER_TYPE_WEBHOSE;
    }

    private void storeWebhoseData(WebhoseResponse response, Gson mGson, String queue_target_host, Queue mQueue) {
        List<WebhosePost> parts = response.posts;
        String jsonWebhose = mGson.toJson(parts.toArray(), WebhosePost[].class);

        if (jsonWebhose.getBytes().length <= MAX_ALLOWED_BYTES_WEBHOSE) {
            List<TaskOptions> tasks = Arrays.asList(TaskOptions.Builder
                              .withUrl("/parse_webhose")
                              .header("Host", queue_target_host)
                              .param("stream_id", topicStream.getId())
                              .param("webhose_posts", jsonWebhose));

            mQueue.add(tasks);
        } else {
            logger.log(Level.INFO, " WEBHOSE TOO BIG");

            Integer sizeChunk = jsonWebhose.getBytes().length / MAX_ALLOWED_BYTES_WEBHOSE + 1;
            List<Object>[] choppedParts = Chopper.chopped(parts, sizeChunk);

            if (choppedParts != null && choppedParts.length > 0) {
                for (List<Object> choppedPart : choppedParts) {
                    if (choppedPart != null && !choppedPart.isEmpty()) {
                        jsonWebhose = mGson.toJson(choppedPart.toArray(), WebhosePost[].class);
                        List<TaskOptions> tasks = Arrays.asList(TaskOptions.Builder
                                          .withUrl("/parse_webhose")
                                          .header("Host", queue_target_host)
                                          .param("stream_id", topicStream.getId())
                                          .param("webhose_posts", jsonWebhose));

                        mQueue.add(tasks);
                    }
                }
            }
        }
    }

    @Override
    public void run() {
        int SLEEP_MINS = 1;

        Boolean local = Boolean.valueOf(System.getProperty("Local"));
        Queue mQueue = (local) ? QueueFactory.getQueue("default") : QueueFactory.getQueue("webhose-to-parser");
        String queue_target_host = ModulesServiceFactory.getModulesService().getVersionHostname("parser", "v1");
        Gson mGson = new Gson();
        String API_KEY = Distributor.getInstance().getAPIKeyConfig().getString("WebhoseAPIKey");
        WebhoseClient client = new WebhoseClient(API_KEY);
        WebhoseResponse response = null;

        try {
            response = client.searchSince(topicStream.getTopicConfig().toWebhoseQuery(), System.currentTimeMillis() - 60000l);

            while (!pool.isShutdown()) {
                if (response != null) {
                    try {
                        storeWebhoseData(response, mGson, queue_target_host, mQueue);
                    } catch (TransientFailureException | IllegalArgumentException | ApiProxy.ArgumentException | ApiProxy.RPCFailedException ex) {
                        new Throwable("Incorrect info from Webhose to push in task queue");
                    }
                }

                try {
                    Thread.sleep(SLEEP_MINS * 60000l);

                    response = client.getMore(response);
                } catch (SocketTimeoutException ex) {
                    new Throwable("Wait for more response");
                }
            }
        } catch (IllegalArgumentException | InterruptedException | IOException ex) {
            logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
        }
    }
}

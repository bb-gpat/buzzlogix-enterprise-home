package com.buzzlogix.distributor.fetcher;

import java.util.concurrent.ExecutorService;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import twitter4j.ExtendedMediaEntity;
import twitter4j.HashtagEntity;
import twitter4j.MediaEntity;
import twitter4j.Place;
import twitter4j.Scopes;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.SymbolEntity;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.URLEntity;
import twitter4j.User;
import twitter4j.UserMentionEntity;
import twitter4j.conf.ConfigurationBuilder;
import com.google.appengine.api.modules.ModulesServiceFactory;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.buzzlogix.distributor.Distributor;
import com.buzzlogix.entity.TopicStream;
import com.buzzlogix.entity.TopicStreamConfig;
import com.buzzlogix.utils.gson.InterfaceAdapter;
import com.buzzlogix.utils.impl.LoggerImpl;
import com.buzzlogix.utils.list.Chopper;
import com.google.appengine.api.taskqueue.TransientFailureException;
import com.google.apphosting.api.ApiProxy.ArgumentException;
import com.google.apphosting.api.ApiProxy.RPCFailedException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.TwitterException;

public class TwitterFetcher extends GenericFetcher {

    private static final Logger logger = new LoggerImpl().getLoggerByHandler(TwitterFetcher.class.getName(), new ConsoleHandler());

    private Twitter twitterRest = null;

    private TwitterStream twitterStream = null;

    private Queue mQueue = null;

    private Gson mGson = null;

    private final Set<Status> buffer = new HashSet<>();

    private final String queue_target_host;

    public TwitterFetcher(ExecutorService pool, TopicStream topicStream) {
        super(pool, topicStream);
        mGson = gsonForStatus();
        mQueue = (Boolean.valueOf(System.getProperty("Local"))) ? QueueFactory.getQueue("default") : QueueFactory.getQueue("twitter-to-parser");
        queue_target_host = ModulesServiceFactory.getModulesService().getVersionHostname("parser", "v1");
    }

    @Override
    public void run() {
        try {
            Twitter twitterRest = getTwitterRest();
            TopicStreamConfig config = topicStream.getTopicConfig();

            if (config != null) {
                Query q = config.toTwitterQuery();
                BigDecimal poolTime = config.getPoolTime();
                BigDecimal correctPoolTime = (poolTime == null || poolTime.equals(BigDecimal.ZERO)) ?
                                             BigDecimal.ONE :
                                             ((poolTime.multiply(new BigDecimal(60000)).intValue() < 10) ?
                                              BigDecimal.TEN :
                                              poolTime);
                Long intervalTime = correctPoolTime.multiply(new BigDecimal(60000)).longValue();

                logger.log(Level.INFO, "NEW TWITTER FETCHER FOR STREAM {0} WITH POOL TIME {1} ms", new Object[]{topicStream.getId(), intervalTime});

                while (!pool.isShutdown()) {
                    QueryResult result = twitterRest.search(q);

                    try {
                        flushTwitterRest(result.getTweets());
                    } catch (TransientFailureException | IllegalArgumentException | ArgumentException | RPCFailedException ex) {
                        new Throwable("Incorrect info from Twitter to push in task queue");
                    }

                    Thread.sleep(intervalTime);
                }
            } else {
                logger.log(Level.INFO, "NO FOUND CONFIGURATION FOR STREAM {0}", topicStream.getId());
            }
        } catch (InterruptedException | TwitterException ex) {
            logger.log(Level.INFO, "INTERRUPTED");
        }
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Deprecated
    private void flush() {
        List<Status> parts = null;

        synchronized (this) {
            // Clone buffer, Split cloned buffer into parts of size DEFAULT_CHUNK_SIZE_TWITTER.
            parts = new ArrayList<>();
            parts.addAll(buffer);
            //Remove all elements from actual buffer.
            buffer.clear();
        }
        if (parts != null || !parts.isEmpty()) {
            String twitter_status = mGson.toJson(parts.toArray(), Status[].class);

            if (twitter_status.getBytes().length <= MAX_ALLOWED_BYTES_TWITTER) {
                Set<TaskOptions> tasks = new HashSet<>(Arrays.asList(TaskOptions.Builder
                                 .withUrl("/parse_twitter")
                                 .header("Host", queue_target_host)
                                 .param("stream_id", topicStream.getId())
                                 .param("twitter_status", twitter_status)));

                mQueue.add(tasks);
            } else {
                logger.log(Level.INFO, " TWEET TOO BIG {0}", topicStream.getId());

                Integer sizeChunk = twitter_status.getBytes().length / MAX_ALLOWED_BYTES_TWITTER + 1;
                List<Object>[] choppedParts = Chopper.chopped(parts, sizeChunk);

                if (choppedParts != null && choppedParts.length > 0) {
                    for (List<Object> choppedPart : choppedParts) {
                        if (choppedPart != null && !choppedPart.isEmpty()) {
                            twitter_status = mGson.toJson(choppedPart.toArray(), Status[].class);
                            Set<TaskOptions> tasks = new HashSet<>(Arrays.asList(TaskOptions.Builder
                                             .withUrl("/parse_twitter")
                                             .header("Host", queue_target_host)
                                             .param("stream_id", topicStream.getId())
                                             .param("twitter_status", twitter_status)));

                            mQueue.add(tasks);
                        }
                    }
                }
            }
        }
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private void flushTwitterRest(List<Status> parts) {
        if (parts != null || !parts.isEmpty()) {
            String twitter_status = mGson.toJson(parts.toArray(), Status[].class);

            if (twitter_status.getBytes().length <= MAX_ALLOWED_BYTES_TWITTER) {
                Set<TaskOptions> tasks = new HashSet<>(Arrays.asList(TaskOptions.Builder
                                 .withUrl("/parse_twitter")
                                 .header("Host", queue_target_host)
                                 .param("stream_id", topicStream.getId())
                                 .param("twitter_status", twitter_status)));

                mQueue.add(tasks);
            } else {
                logger.log(Level.INFO, " TWEET TOO BIG " + topicStream.getId());

                Integer sizeChunk = twitter_status.getBytes().length / MAX_ALLOWED_BYTES_TWITTER + 1;
                List<Object>[] choppedParts = Chopper.chopped(parts, sizeChunk);

                if (choppedParts != null && choppedParts.length > 0) {
                    for (List<Object> choppedPart : choppedParts) {
                        if (choppedPart != null && !choppedPart.isEmpty()) {
                            twitter_status = mGson.toJson(choppedPart.toArray(), Status[].class);
                            Set<TaskOptions> tasks = new HashSet<>(Arrays.asList(TaskOptions.Builder
                                             .withUrl("/parse_twitter")
                                             .header("Host", queue_target_host)
                                             .param("stream_id", topicStream.getId())
                                             .param("twitter_status", twitter_status)));

                            mQueue.add(tasks);
                        }
                    }
                }
            }
        }
    }

    @Override
    public int getType() {
        return GenericFetcher.FETCHER_TYPE_TWITTER;
    }

//    @Override
    @Deprecated
    public void onException(Exception arg0) {
        logger.log(Level.INFO, "onException " + arg0.toString());
        //TODO HANDLE..

    }

//    @Override
    @Deprecated
    public void onDeletionNotice(StatusDeletionNotice arg0) {
        //  logger.log(Level.INFO, "onDeletionNotice "+arg0.toString());

    }

//    @Override
    @Deprecated
    public void onScrubGeo(long arg0, long arg1) {
        // TODO Auto-generated method stub

    }

//    @Override
    @Deprecated
    public void onStallWarning(StallWarning arg0) {
        logger.log(Level.INFO, "onStallWarning " + arg0.toString() + topicStream.getId());

    }

//    @Override
    @Deprecated
    public void onStatus(Status arg0) {
        buffer.add(arg0);
        //logger.log(Level.INFO, "onStatus "+arg0.toString());
    }

//    @Override
    @Deprecated
    public void onTrackLimitationNotice(int arg0) {
        logger.log(Level.INFO, "onTrackLimitationNotice " + arg0 + topicStream.getId());

    }

    public Twitter getTwitterRest() {
//        if (twitterRest == null) {
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
                .setOAuthConsumerKey(Distributor.getInstance().getAPIKeyConfig().getString("Twiter_consumerKey"))
                .setOAuthConsumerSecret(Distributor.getInstance().getAPIKeyConfig().getString("Twiter_consumerSecret"))
                .setOAuthAccessToken(Distributor.getInstance().getAPIKeyConfig().getString("Twiter_accessToken"))
                .setOAuthAccessTokenSecret(Distributor.getInstance().getAPIKeyConfig().getString("Twiter_accessTokenSecret"))
                .setJSONStoreEnabled(true);

        TwitterFactory tf = new TwitterFactory(cb.build());
        twitterRest = tf.getInstance();
//        }

        return twitterRest;
    }

    @Deprecated
    public TwitterStream getTwitterStream() {
        if (twitterStream == null) {
            ConfigurationBuilder cb = new ConfigurationBuilder();
            cb.setDebugEnabled(true)
                    .setOAuthConsumerKey(Distributor.getInstance().getAPIKeyConfig().getString("Twiter_consumerKey"))
                    .setOAuthConsumerSecret(Distributor.getInstance().getAPIKeyConfig().getString(
                                    "Twiter_consumerSecret"))
                    .setOAuthAccessToken(Distributor.getInstance().getAPIKeyConfig().getString("Twiter_accessToken"))
                    .setOAuthAccessTokenSecret(Distributor.getInstance().getAPIKeyConfig().getString(
                                    "Twiter_accessTokenSecret"))
                    .setJSONStoreEnabled(true);

            TwitterStreamFactory tf = new TwitterStreamFactory(cb.build());
            twitterStream = tf.getInstance();
        }

        return twitterStream;
    }

    private Gson gsonForStatus() {
        Gson gson = new GsonBuilder()
             .registerTypeAdapter(Status.class, new InterfaceAdapter<Status>())
             .registerTypeAdapter(User.class, new InterfaceAdapter<User>())
             .registerTypeAdapter(UserMentionEntity.class, new InterfaceAdapter<UserMentionEntity>())
             .registerTypeAdapter(MediaEntity.class, new InterfaceAdapter<MediaEntity>())
             .registerTypeAdapter(MediaEntity.Size.class, new InterfaceAdapter<MediaEntity.Size>())
             .registerTypeAdapter(HashtagEntity.class, new InterfaceAdapter<HashtagEntity>())
             .registerTypeAdapter(URLEntity.class, new InterfaceAdapter<URLEntity>())
             .registerTypeAdapter(Place.class, new InterfaceAdapter<Place>())
             .registerTypeAdapter(ExtendedMediaEntity.class, new InterfaceAdapter<ExtendedMediaEntity>())
             .registerTypeAdapter(SymbolEntity.class, new InterfaceAdapter<SymbolEntity>())
             .registerTypeAdapter(Scopes.class, new InterfaceAdapter<Scopes>())
             .create();
        return gson;
    }
}

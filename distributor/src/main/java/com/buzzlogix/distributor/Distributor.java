package com.buzzlogix.distributor;

import java.util.ArrayList;
import java.util.List;

import com.google.appengine.api.datastore.Key;
import com.buzzlogix.distributor.handler.ChannelHandler;
import com.buzzlogix.distributor.handler.StreamHandler;
import com.buzzlogix.entity.Channel;
import com.buzzlogix.entity.TopicStream;
import com.buzzlogix.entity.TopicStreamConfig;
import com.buzzlogix.enums.SourceTypeEnum;
import com.buzzlogix.utils.impl.LoggerImpl;
import com.buzzlogix.utils.impl.XMLConfigurationImpl;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Distributor {
    
    private final static Logger logger = new LoggerImpl().getLoggerByHandler(Distributor.class.getName(), new ConsoleHandler());

    private final List<StreamHandler> streamHandlers = new ArrayList<>();

    private final List<ChannelHandler> channelHandlers = new ArrayList<>();

    private final XMLConfigurationImpl api_key_config = new XMLConfigurationImpl("META-INF/provider_creds.xml");

    private Distributor() {
    }

    private static class SingletonHolder {

        private static final Distributor INSTANCE = new Distributor();

    }

    public static Distributor getInstance() {
        return SingletonHolder.INSTANCE;
    }

    public void handleTopicStream(TopicStream topicStream) {
        StreamHandler streamHandler = new StreamHandler(topicStream);
        streamHandlers.add(streamHandler);
        streamHandler.start();
    }

    public void stopAll() { 
        for (StreamHandler handler : streamHandlers) {
            logger.log(Level.INFO, "stop handler={0}", handler.getTopicStream().getId());
            
            handler.stop();
        }
    }

    public void stopTopicStream(TopicStream stream) {
        Map<String, StreamHandler> map = new HashMap<>();

        for (StreamHandler handler : streamHandlers) {
            map.put(handler.getTopicStream().getId(), handler);
        }
        
        StreamHandler handler = map.get(stream.getId());

        if (handler != null) {
            logger.log(Level.INFO, "stop handler={0}", handler.getTopicStream().getId());
            
            handler.stop();
            streamHandlers.remove(handler);
        }
    }

    public void onStreamConfigChange(Key key, TopicStreamConfig newConfig) {

    }

    public void onStreamNameChange(Key key, String newName) {

    }

    public XMLConfigurationImpl getAPIKeyConfig() {
        return api_key_config;
    }

    public void test() {
        //test code
        ArrayList<String> allTerms = new ArrayList<>();
        allTerms.add("Russia");

        ArrayList<SourceTypeEnum> sources = new ArrayList<>();
//		sources.add(SourceTypeEnum.DISCUSSIONS);
//		sources.add(SourceTypeEnum.BLOGS);
//        sources.add(SourceTypeEnum.NEWS);
        sources.add(SourceTypeEnum.TWITTER);

        TopicStreamConfig fkConfig1 = new TopicStreamConfig.Builder().setAllTerms(allTerms).setSources(sources).build();
        TopicStream fkStream1 = new TopicStream();
        fkStream1.setId("Debug_Stream_ID");
        fkStream1.setTopicConfig(fkConfig1);
        //END test code
        //TODO GET STREAMS & CHANNELS FROM DATASTORE here, for compilation purposes they are currently private memebr lists.
        List<TopicStream> streams = new ArrayList<>();
        streams.add(fkStream1);
        List<Channel> channels = new ArrayList<>();

        for (TopicStream topicStream : streams) {
            StreamHandler streamHandler = new StreamHandler(topicStream);
            streamHandlers.add(streamHandler);
            streamHandler.start();
        }

        for (Channel channel : channels) {
            ChannelHandler channelHandler = new ChannelHandler(channel);
            channelHandlers.add(channelHandler);
        }

    }

}

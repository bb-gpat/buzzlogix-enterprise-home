package com.buzzlogix.distributor;

import java.util.logging.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TestServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(Init.class.getName());

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) {
        Distributor.getInstance().test();
    }

}

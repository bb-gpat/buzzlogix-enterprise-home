/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.distributor.pubsub;

import com.google.api.services.pubsub.Pubsub;
import com.buzzlogix.pubsub.PubSub;
import com.buzzlogix.pubsub.ResourceTypeEnum;

import java.io.IOException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author administrator
 * 
 * Publishes messages to the application topic.
 */
@SuppressWarnings("serial")
public class SendMessageServlet extends HttpServlet {

    @Override
    public final void doPost(final HttpServletRequest req, final HttpServletResponse resp) throws IOException {
        PubSub pubsub = new PubSub("META-INF/");
        Pubsub client = pubsub.getClient();
        String message = req.getParameter("message");
        
        if (!"".equals(message)) {
            pubsub.getTopic().publishMessage(client, ResourceTypeEnum.PARSER, message);
        }
        
        resp.setStatus(HttpServletResponse.SC_NO_CONTENT);
        resp.getWriter().close();
    }
}

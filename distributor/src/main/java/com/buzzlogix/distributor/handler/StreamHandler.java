package com.buzzlogix.distributor.handler;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.google.appengine.api.ThreadManager;
import com.buzzlogix.distributor.fetcher.GenericFetcher;
import com.buzzlogix.distributor.fetcher.TwitterFetcher;
import com.buzzlogix.distributor.fetcher.WebhoseFetcher;
import com.buzzlogix.entity.TopicStream;

public class StreamHandler {

    private TopicStream stream;

    private final ExecutorService pool;

    List<GenericFetcher> fetchers = new ArrayList<>();

    public StreamHandler(TopicStream stream) {
        this.stream = stream;

        pool = Executors.newFixedThreadPool(countNeededWorkers(), ThreadManager.backgroundThreadFactory());
        createFetchers(pool);
    }

    public void createFetchers(ExecutorService pool) {
        if (stream.getTopicConfig().isWebHose()) {
            fetchers.add(new WebhoseFetcher(pool, stream));
        }
        if (stream.getTopicConfig().isTwitter()) {
            fetchers.add(new TwitterFetcher(pool, stream));
        }
    }

    public void start() {
        for (GenericFetcher fetcher : fetchers) {
            pool.execute(fetcher);
        }
    }

    public void stop() {
        pool.shutdown();
    }

    private int countNeededWorkers() {
        int count = 0;
        if (stream.getTopicConfig().isWebHose()) {
            count++;
        }
        if (stream.getTopicConfig().isTwitter()) {
            count++;
        }
        //etc etc for each provider.
        return count;
    }

    public TopicStream getTopicStream() {
        return stream;
    }
}

package com.buzzlogix.distributor.service;

import com.google.appengine.api.modules.ModulesServiceFactory;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.repackaged.com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.buzzlogix.distributor.Distributor;
import com.buzzlogix.entity.TopicStream;
import com.buzzlogix.enums.StreamStatusEnum;
import com.buzzlogix.utils.impl.HttpClientImpl;
import com.buzzlogix.utils.impl.LoggerImpl;
import java.net.URL;

@SuppressWarnings("serial")
public class TopicStreamServlet extends HttpServlet {

    private final static Logger logger = new LoggerImpl().getLoggerByHandler(TopicStreamServlet.class.getName(), new ConsoleHandler());

    private static final String HANDLE = "/handle";

    private static final String STOP = "/stop";

    private static final String STOP_ALL = "/stopAll";

    @Override
    @SuppressWarnings("unchecked")
    public final void doPost(final HttpServletRequest req, final HttpServletResponse resp) throws IOException {
        String result = null;
        resp.setContentType("application/json");

        switch (req.getPathInfo()) {
            case HANDLE: result = handle(req.getReader());
                break;
            case STOP: result = stop(req.getReader());
                break;
        }
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) {
        switch (req.getPathInfo()) {
            case STOP_ALL: {
                Distributor.getInstance().stopAll();
            }
        }
    }

    private String handle(BufferedReader reader) {
        String result = null;
        String data = "", line;

        try {
            if (reader.ready()) {
                while ((line = reader.readLine()) != null) {
                    data += line;
                }
                reader.close();
                if (!Strings.isNullOrEmpty(data)) {
                    logger.log(Level.INFO, "handle topic stream={0}", data);
                    
                    Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                    TopicStream stream = gson.fromJson(data, TopicStream.class);

                    if (stream != null) {
                        String target_host = ModulesServiceFactory.getModulesService().getVersionHostname("default", "v1");
                        String protocol = (Boolean.valueOf(System.getProperty("Local"))) ? "http://" : "https://";
                        String json = gson.toJson(new TopicStream.Builder()
                               .setId(stream.getId())
                               .setStastus(StreamStatusEnum.RUNNING)
                               .build());
                        new HttpClientImpl().post(new URL(protocol + target_host + "/topic/update"), json, null);

                        Distributor.getInstance().handleTopicStream(stream);

                        result = "1";
                    } else {
                        logger.log(Level.SEVERE, "Cannot read topic stream:{0}", data);

                        result = "1";
                    }
                } else {
                    logger.log(Level.SEVERE, "Must given a topic stream format json: {0}", data);

                    result = "1";
                }
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, null, e);
        }

        return result;
    }

    private String stop(BufferedReader reader) {
        String result = null;
        String data = "", line;
        
        try {
            if (reader.ready()) {
                while ((line = reader.readLine()) != null) {
                    data += line;
                }

                reader.close();
                if (!Strings.isNullOrEmpty(data)) {
                    logger.log(Level.INFO, "stop topic stream={0}", data);
                    
                    Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                    TopicStream stream = gson.fromJson(data, TopicStream.class);

                    if (stream != null) {
                        Distributor.getInstance().stopTopicStream(stream);

                        String target_host = ModulesServiceFactory.getModulesService().getVersionHostname("default", "v1");
                        String protocol = (Boolean.valueOf(System.getProperty("Local"))) ? "http://" : "https://";
                        String json = gson.toJson(new TopicStream.Builder()
                               .setId(stream.getId())
                               .setStastus(StreamStatusEnum.STOPPED)
                               .setTopicConfig(null)
                               .build());
                        new HttpClientImpl().post(new URL(protocol + target_host + "/topic/update"), json, null);

                        result = "1";
                    } else {
                        logger.log(Level.SEVERE, " Failed to stop topic stream");

                        result = "0";
                    }
                } else {
                    logger.log(Level.SEVERE, "Must given a topic stream format json: " + data);

                    result = "0";
                }
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, null, e);
        }

        return result;
    }
}

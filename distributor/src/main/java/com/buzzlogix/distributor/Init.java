package com.buzzlogix.distributor;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import com.google.appengine.api.urlfetch.HTTPResponse;
import com.google.appengine.api.urlfetch.URLFetchService;
import com.google.appengine.api.urlfetch.URLFetchServiceFactory;

public class Init implements ServletContextListener {

    private static final Logger logger = Logger.getLogger(Init.class.getName());

    public Init() {
    }

    @Override
    public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub

    }

    @Override
    public void contextInitialized(ServletContextEvent arg0) {
        logger.log(Level.ALL, "contextInitialized");
	//	try {
        //		testBasicOps();
        //	} catch (Exception e) {
        //		// TODO Auto-generated catch block
        //		e.printStackTrace();
        //	}
        //	Distributor.getInstance().launch();

    }

    public void testBasicOps() throws Exception {
        URLFetchService service = URLFetchServiceFactory.getURLFetchService();
        URL adminConsole = findAvailableUrl("http://localhost:8080/_ah/admin", "http://capedwarf-test.appspot.com/index.html");
        HTTPResponse response = service.fetch(adminConsole);
        printResponse(response);
        URL jbossOrg = new URL("http://www.jboss.org");
        if (available(jbossOrg)) {
            response = service.fetch(jbossOrg);
            printResponse(response);
        }
    }

    private void printResponse(HTTPResponse response) throws Exception {
        System.out.println("response = " + new String(response.getContent()));
    }

    private static URL findAvailableUrl(String... urls) throws Exception {
        for (String s : urls) {
            URL url = new URL(s);
            if (available(url)) {
                return url;
            }
        }
        throw new IllegalArgumentException("No available url: " + Arrays.toString(urls));
    }

    /**
     * Dummy check if we're available.
     *
     * @param url the url to check against
     * @return true if available, false otherwise
     */
    private static boolean available(URL url) {
        InputStream stream = null;
        try {
            stream = url.openStream();
            int x = stream.read();
            return (x != -1);
        } catch (Exception e) {
            return false;
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException ignored) {
                }
            }
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.utils.impl;

import com.buzzlogix.utils.IXMLConfiguration;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

/**
 *
 * @author administrator
 */
public class XMLConfigurationImpl implements IXMLConfiguration {

    private static final Logger logger = Logger.getLogger(XMLConfigurationImpl.class.getName());

    private Element element;

    public XMLConfigurationImpl(String configFile) {
        element = getConfigHandler(configFile);
    }

    @Override
    public String getString(String name) {
        return element.getChildText(name);
    }

    @Override
    public Integer getInteger(String name) {
        Integer value = null;
        
        try {
            value = Integer.parseInt(element.getChildText(name));
        } catch (NumberFormatException ex) {
            logger.log(Level.SEVERE, "{0}", ex.getMessage());
        } finally {
            return value;
        }
    }
    
    @Override
    public Long getLong(String name) {
        Long value = null;
        
        try {
            value = Long.parseLong(element.getChildText(name));
        } catch (NumberFormatException ex) {
            logger.log(Level.SEVERE, "{0}", ex.getMessage());
        } finally {
            return value;
        }
    }
    
    @Override
    public BigDecimal getDecimal(String name) {
        BigDecimal value = null;
        
        try {
            value = new BigDecimal(element.getChildText("deadtline"));
        } catch (NumberFormatException ex) {
            logger.log(Level.SEVERE, "{0}", ex.getMessage());
        } finally {
            return value;
        }
    }
    
    @Override
    public Boolean getBoolean(String name) {
        Boolean value = null;
        
        try {
            value = Boolean.parseBoolean(element.getChildText(name));
        } catch (NumberFormatException ex) {
            logger.log(Level.SEVERE, "{0}", ex.getMessage());
        } finally {
            return value;
        }
    }
    
    private Element getConfigHandler(String configFile) {
        Element element = null;

        try {
            SAXBuilder parser = new SAXBuilder();
            Document docConfig = null;

            docConfig = parser.build(configFile);
            element = docConfig.getRootElement();
        } catch (JDOMException | IOException ex) {
            logger.log(Level.SEVERE, "Error parsing xml file:" + ex.getMessage());
        }

        return element;
    }
}

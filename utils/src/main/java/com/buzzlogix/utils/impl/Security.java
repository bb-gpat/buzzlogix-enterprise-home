/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.utils.impl;

import com.google.common.base.Strings;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author administrator
 */
public class Security {

    public static final String MD5 = "MD5";

    public static final String SHA0 = "SHA-0";

    public static final String SHA1 = "SHA-1";

    public static final String SHA256 = "SHA-256";

    public static String encryption(String code, String message) {
        if (Strings.isNullOrEmpty(message)) {
            return null;
        }

        String chiper = "";
        MessageDigest digest = null;

        try {
            digest = MessageDigest.getInstance(code);
        } catch (NoSuchAlgorithmException e) {
            System.err.println(e.getMessage());
        }

        // Hashing entered key to construct a new key/salt
        byte[] keyAsSHA256 = digest.digest(message.getBytes());

        // Encoding the message with CBC
        char[] messageAsChars = message.toCharArray();
        // Avoiding buffer underflow
        messageAsChars[0] ^= keyAsSHA256[0];
        
        for (int i = 1; i < messageAsChars.length; i++) {
            messageAsChars[i] ^= messageAsChars[i - 1]; // XOR with previous character
            messageAsChars[i] ^= keyAsSHA256[i % keyAsSHA256.length]; // XOR with keys hash
        }
        
        // build cipher from the chars
        chiper = new String(messageAsChars);
        
        return chiper;
    }
}

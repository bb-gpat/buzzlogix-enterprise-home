/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.utils.impl;

import com.google.common.base.Strings;
import com.buzzlogix.utils.ILogger;
import java.util.logging.Handler;
import java.util.logging.Logger;

/**
 *
 * @author administrator
 */
public class LoggerImpl implements ILogger {

    @Override
    public Logger getLoggerByHandler(String className, Handler handler) {
        if (!Strings.isNullOrEmpty(className) && handler != null) {
            Logger logger = Logger.getLogger(className);
            
            if (logger != null) {
                logger.addHandler(handler);
                
                return logger;
            }
        }
        
        return null;
    }
}

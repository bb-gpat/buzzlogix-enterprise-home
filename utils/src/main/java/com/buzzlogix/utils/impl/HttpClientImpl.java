/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.utils.impl;

import static com.google.appengine.api.urlfetch.FetchOptions.Builder.withDefaults;
import com.google.appengine.api.urlfetch.HTTPHeader;
import com.google.appengine.api.urlfetch.HTTPMethod;
import com.google.appengine.api.urlfetch.HTTPRequest;
import com.google.appengine.api.urlfetch.HTTPResponse;
import com.google.appengine.api.urlfetch.URLFetchService;
import com.google.appengine.api.urlfetch.URLFetchServiceFactory;
import com.buzzlogix.utils.IHttpClient;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.HttpClientBuilder;

import org.apache.http.HttpResponse;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

/**
 *
 * @author administrator
 */
public class HttpClientImpl implements IHttpClient {

    private static final Logger logger = new LoggerImpl().getLoggerByHandler(HttpClientImpl.class.getName(), new ConsoleHandler());

    @Override
    public String post(URL url, String jsonInput, Map<String, String> header) {
        String content = null;

        try {
            URLFetchService urlFetchService = URLFetchServiceFactory.getURLFetchService();
            HTTPRequest request = new HTTPRequest(url, HTTPMethod.POST, withDefaults().setDeadline(30.0));

            request.setPayload(jsonInput.getBytes());
            request.setHeader(new HTTPHeader("Content-Type", "application/json"));
            if (header != null && !header.isEmpty()) {
                for (String name : header.keySet()) {
                    String value = header.get(name);
                    
                    request.setHeader(new HTTPHeader(name, value));
                }
            }

            HTTPResponse response = urlFetchService.fetch(request);
            if (response.getResponseCode() == HttpURLConnection.HTTP_OK) {
                content = new String(response.getContent());
            }
        } catch (IOException e) {
            logger.log(Level.SEVERE, null, e);
            e.printStackTrace();
        }

        return content;
    }

    @Override
    public String get(URL url) {
        String content = null;

        try {
            URLFetchService urlFetchService = URLFetchServiceFactory.getURLFetchService();
            HTTPRequest request = new HTTPRequest(url, HTTPMethod.GET, withDefaults().setDeadline(30.0));

            request.setHeader(new HTTPHeader("Content-Type", "application/json"));

            HTTPResponse response = urlFetchService.fetch(request);
            if (response.getResponseCode() == HttpURLConnection.HTTP_OK) {
                content = new String(response.getContent());
            }
        } catch (IOException e) {
            logger.log(Level.SEVERE, null, e);
        }

        return content;
    }

    @Override
    public String post(String uri, String jsonInput) {
        HttpClient httpClient = HttpClientBuilder.create().build();
        String result = null;

        try {
            HttpPost postRequest = new HttpPost(uri);
            postRequest.setEntity(new StringEntity(jsonInput, ContentType.create("application/json")));

            HttpResponse resp = httpClient.execute(postRequest);

            BufferedReader rd = new BufferedReader(new InputStreamReader(resp.getEntity().getContent()));
            String sum = "", line;

            while ((line = rd.readLine()) != null) {
                sum += line;
            }

            if (!sum.isEmpty()) {
                result = sum;
            }
        } catch (IOException e) {
            logger.log(Level.SEVERE, null, e);
        } catch (RuntimeException e) {
            logger.log(Level.SEVERE, null, e);
        } finally {
            httpClient.getConnectionManager().shutdown();

            return result;
        }
    }

    @Override
    public String get(String uri) {
        HttpClient httpClient = HttpClientBuilder.create().build();
        String result = null;

        try {
            HttpGet getRequest = new HttpGet(uri);
            getRequest.addHeader("accept", "application/json");
            HttpResponse response = httpClient.execute(getRequest);
            BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));
            String sum = "", line;

            while ((line = br.readLine()) != null) {
                sum += line;
            }
            if (!sum.isEmpty()) {
                result = sum;
            }
        } catch (IOException e) {
            logger.log(Level.SEVERE, null, e);
        } catch (RuntimeException e) {
            logger.log(Level.SEVERE, null, e);
        } finally {
            httpClient.getConnectionManager().shutdown();

            return result;
        }
    }

    @Override
    public String getResponseApache(String urlString) throws MalformedURLException, SocketTimeoutException {
        StringBuffer result = new StringBuffer();
        
        try {

            SSLConnectionSocketFactory sf = new SSLConnectionSocketFactory(
                                       SSLContexts.custom().useTLS().build(),
                                       new String[]{"TLSv1", "TLSv1.1", "TLSv1.2"},
                                       null,
                                       SSLConnectionSocketFactory.BROWSER_COMPATIBLE_HOSTNAME_VERIFIER);
            CloseableHttpClient client = HttpClients.custom()
                                .setSSLSocketFactory(sf)
                                .build();
            HttpGet request = new org.apache.http.client.methods.HttpGet(urlString);
            HttpResponse response = client.execute(request);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String line = "";
            
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
        } catch (IOException e) {
            Logger.getLogger(HttpPost.class.getSimpleName()).log(Level.SEVERE, e.getMessage(), e);
            e.printStackTrace();
        } catch (KeyManagementException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {

        }


        /* return response */
        return result.toString();
    }
}

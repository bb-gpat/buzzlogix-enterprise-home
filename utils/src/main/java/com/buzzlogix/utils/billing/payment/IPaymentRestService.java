package com.buzzlogix.utils.billing.payment;

import java.io.IOException;
import org.apache.http.HttpException;

public interface IPaymentRestService {

    public Payment adjust(Payment payment) throws HttpException, IOException;

    public Payment charge(Payment payment) throws HttpException, IOException;

    public Payment refunds(Payment payment) throws HttpException, IOException;

}

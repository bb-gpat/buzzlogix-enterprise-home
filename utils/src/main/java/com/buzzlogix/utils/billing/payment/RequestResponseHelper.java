package com.buzzlogix.utils.billing.payment;

import com.google.gson.Gson;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import com.buzzlogix.utils.billing.BillingHelper;
import com.buzzlogix.utils.billing.customer.CustomerRestService;

import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.http.HttpException;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;

public class RequestResponseHelper {
    
    private static final Logger logger = Logger.getLogger(CustomerRestService.class.getName());

    public HttpResponse post(String uri, Map<String, Map> params) throws HttpException, IOException {
        HttpPost postRequest = new HttpPost(uri);
        String jsonInput =  new Gson().toJson(params);
        
         postRequest.setEntity(new StringEntity(jsonInput, ContentType.create("application/json")));

        return BillingHelper.getClient().execute(postRequest);
    }

    public Payment jsonToPayment(String json, String key) {
        Payment payment = null;
        Gson gson = new Gson();
        
        try {
            Map<String, Object> responseMap = gson.fromJson(json, Map.class);
            Map map = (Map) responseMap.get(key);
            String paymentString = gson.toJson(map);
            
            payment = gson.fromJson(paymentString, Payment.class);
        } catch (Exception ex) {
            Logger.getLogger(RequestResponseHelper.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return payment;
        }
    }

    public Payment jsonToErrors(String json, Payment payment) {
        try {
            Map<String, Object> responseMap = new Gson().fromJson(json, Map.class);
            List<String> errors = (List<String>) responseMap.get("errors");
            
            payment.setErrors(errors);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "{0}", ex.getMessage());
        } finally {
            return payment;
        }
    }
}

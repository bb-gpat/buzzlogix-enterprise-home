package com.buzzlogix.utils.billing;

import java.io.IOException;
import java.util.List;
import com.buzzlogix.utils.billing.customer.Customer;
import com.buzzlogix.utils.billing.customer.CustomerRestService;
import com.buzzlogix.utils.billing.customer.ICustomerRestService;
import com.buzzlogix.utils.billing.payment.IPaymentRestService;
import com.buzzlogix.utils.billing.payment.Payment;
import com.buzzlogix.utils.billing.payment.PaymentRestService;
import com.buzzlogix.utils.impl.HttpClientImpl;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.HttpException;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;

public class BillingHelper {

    private static final Logger logger = Logger.getLogger(HttpClientImpl.class.getName());

    private String hostName = "chargify.com";

    private int port = 443;

    private String subDomain;

    private String key;

    private String password = "xxxxxxxx";

    private static HttpClient client;

    public BillingHelper(String subDomain, String key) {
        UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(key, password);
        AuthScope authScope = new AuthScope(subDomain + "." + hostName, port, "chargify API");
        CredentialsProvider provider = new BasicCredentialsProvider();

        provider.setCredentials(authScope, credentials);
        this.subDomain = subDomain;
        this.key = key;

        if (client == null) {
            try {
                client = HttpClientBuilder.create().setDefaultCredentialsProvider(provider).build();

                java.net.URI uri = new URIBuilder()
                             .setScheme("https")
                             .setHost(hostName + ":" + port)
                             .setPath(subDomain)
                             .build();

                HttpPost postRequest = new HttpPost(uri);
                
                client.execute(postRequest);
            } catch (URISyntaxException | IOException ex) {
                logger.log(Level.SEVERE, "{0}", ex.getMessage());
            }
        };
    }

    public void setDomain(String domain) {
        this.subDomain = domain;
    }

    public String getDomain() {
        return subDomain;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public void setClient(HttpClient client) {
        BillingHelper.client = client;
    }

    public static HttpClient getClient() {
        return BillingHelper.client;
    }

    public List<Customer> getCustomers() {
        return getCustomers(1);
    }

    public List<Customer> getCustomers(int page) {
        ICustomerRestService customerService = new CustomerRestService();
        
        try {
            return customerService.getCustomers(page);
        } catch (IOException | HttpException ex) {
            logger.log(Level.SEVERE, "{0}", ex.getMessage());
        }

        return null;
    }

    public Payment charge(Payment payment) {

        IPaymentRestService paymentService = new PaymentRestService();

        try {
            payment = paymentService.charge(payment);
        } catch (IOException | HttpException ex) {
            // TODO Auto-generated catch block
            logger.log(Level.SEVERE, "{0}", ex.getMessage());
        }

        return payment;
    }

    public Payment adjust(Payment payment) {

        IPaymentRestService paymentService = new PaymentRestService();

        try {
            payment = paymentService.adjust(payment);    
        } catch (IOException | HttpException ex) {
            // TODO Auto-generated catch block
            logger.log(Level.SEVERE, "{0}", ex.getMessage());
        }

        return payment;
    }

    public Payment refunds(Payment payment) {

        IPaymentRestService paymentService = new PaymentRestService();

        try {
            payment = paymentService.refunds(payment);
        } catch (IOException | HttpException ex) {
            // TODO Auto-generated catch block
            logger.log(Level.SEVERE, "{0}", ex.getMessage());
        }

        return payment;
    }
}

package com.buzzlogix.utils.billing.payment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.HttpException;
import org.apache.http.HttpResponse;

public class PaymentRestService extends RequestResponseHelper implements IPaymentRestService {

    @Override
    public Payment adjust(Payment payment) throws HttpException, IOException {
        String uri = "/subscriptions/" + payment.getSubscriptionId().intValue() + "/adjustments.json";

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("amount", payment.getAmount());
        paramMap.put("memo", payment.getMemo());

        Map<String, Map> params = new HashMap<>();
        params.put("adjustment", paramMap);

        HttpResponse response = post(uri, params);
        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        String result = "", line;

        while ((line = rd.readLine()) != null) {
            result += line;
        }
        
        Payment paymentResponse = jsonToPayment(result, "adjustment");

        if (paymentResponse == null) {
            paymentResponse = jsonToErrors(result, payment);
            paymentResponse.setSuccess(false);
        } else {
            paymentResponse.setSuccess(true);
        }

        paymentResponse.setResponseCode(response.getStatusLine().getStatusCode());
        paymentResponse.setResponseJson(result);

        return paymentResponse;
    }

    @Override
    public Payment charge(Payment payment) throws HttpException, IOException {
        String uri = "/subscriptions/" + payment.getSubscriptionId().intValue() + "/charges.json";

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("amount", payment.getAmount());
        paramMap.put("memo", payment.getMemo());

        Map<String, Map> params = new HashMap<>();
        params.put("charge", paramMap);

        HttpResponse response = post(uri, params);
        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        String result = "", line;

        while ((line = rd.readLine()) != null) {
            result += line;
        }
        
        Payment paymentResponse = jsonToPayment(result, "charge");

        if (paymentResponse == null) {
            paymentResponse = jsonToErrors(result, payment);
            paymentResponse.setSuccess(false);
        } else {
            paymentResponse.setSuccess(true);
        }

        paymentResponse.setResponseCode(response.getStatusLine().getStatusCode());
        paymentResponse.setResponseJson(result);

        return paymentResponse;
    }

    @Override
    public Payment refunds(Payment payment) throws HttpException, IOException {
        String uri = "/subscriptions/" + payment.getSubscriptionId().intValue() + "/refunds.json";

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("payment_id", payment.getPaymentId().intValue());
        paramMap.put("amount", payment.getAmount());
        paramMap.put("memo", payment.getMemo());

        Map<String, Map> params = new HashMap<>();
        params.put("refund", paramMap);

        HttpResponse response = post(uri, params);
        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        String result = "", line;

        while ((line = rd.readLine()) != null) {
            result += line;
        }
        
        Payment paymentResponse = jsonToPayment(result, "refund");

        if (paymentResponse == null) {
            paymentResponse = jsonToErrors(result, payment);
            paymentResponse.setSuccess(false);
        } else {
            paymentResponse.setSuccess(true);
        }

        paymentResponse.setResponseCode(response.getStatusLine().getStatusCode());
        paymentResponse.setResponseJson(result);

        return paymentResponse;
    }

}

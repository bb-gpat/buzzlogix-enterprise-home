package com.buzzlogix.utils.billing.customer;

import java.io.IOException;
import java.util.List;
import org.apache.http.HttpException;

public interface ICustomerRestService {

    public List<Customer> getCustomers(int page) throws HttpException, IOException;
}

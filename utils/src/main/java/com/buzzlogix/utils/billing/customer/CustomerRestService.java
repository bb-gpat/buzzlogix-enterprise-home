package com.buzzlogix.utils.billing.customer;

import com.google.gson.Gson;
import com.buzzlogix.utils.billing.BillingHelper;
import com.buzzlogix.utils.billing.payment.Payment;
import com.buzzlogix.utils.billing.payment.RequestResponseHelper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.http.HttpException;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;

public class CustomerRestService extends RequestResponseHelper implements ICustomerRestService {
    
    private static final Logger logger = Logger.getLogger(CustomerRestService.class.getName());

    public Payment adjust(Payment payment) throws HttpException, IOException {
        String uri = "/subscriptions/" + payment.getSubscriptionId().intValue() + "/adjustments.json";
        Map<String, Object> paramMap = new HashMap<>();
        Map<String, Map> params = new HashMap<>();

        paramMap.put("amount", payment.getAmount());
        paramMap.put("memo", payment.getMemo());
        params.put("adjustment", paramMap);

        HttpResponse response = post(uri, params);
        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        String result = "", line;

        while ((line = rd.readLine()) != null) {
            result += line;
        }

        Payment paymentResponse = jsonToPayment(result, "adjustment");

        paymentResponse.setResponseCode(response.getStatusLine().getStatusCode());
        paymentResponse.setResponseJson(result);

        return paymentResponse;
    }

    @Override
    public List<Customer> getCustomers(int page) throws HttpException, IOException {
        HttpGet getRequest = new HttpGet("/customers.json");

        getRequest.addHeader("accept", "application/json");
        
        HttpResponse response = BillingHelper.getClient().execute(getRequest);
        BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));
        String result = "", line;

        while ((line = br.readLine()) != null) {
            result += line;
        }
        
        Gson gson = new Gson();
        List<Map<String, Object>> customersJsonMap = gson.fromJson(result, List.class);
        String customerJson;
        Customer customer;
        HashMap<String, String> customerMap;
        List<Customer> customers = new ArrayList<>();

        int ss = response.getStatusLine().getStatusCode();
        logger.log(Level.INFO, "Status:{0}", ss);

        for (Map<String, Object> customerJsonMap : customersJsonMap) {
            customerMap = (HashMap<String, String>) customerJsonMap.get("customer");
            customerJson = gson.toJson(customerMap);
            customer = gson.fromJson(customerJson, Customer.class);
            customers.add(customer);
        }

        return customers;
    }
}

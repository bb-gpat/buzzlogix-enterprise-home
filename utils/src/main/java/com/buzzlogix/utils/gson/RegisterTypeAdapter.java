/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.utils.gson;

import com.google.appengine.api.datastore.Text;
import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import java.lang.reflect.Type;
import java.util.Date;

/**
 *
 * @author antonia
 */
public class RegisterTypeAdapter {

    public JsonSerializer<Text> ser = new JsonSerializer<Text>() {
        @Override
        public JsonElement serialize(Text src, Type typeOfSrc, JsonSerializationContext context) {
            String value = new Gson().toJson(src, Text.class);

            return src == null ? null : new JsonPrimitive(value);
        }
    };

    public static JsonDeserializer<Text> deser = new JsonDeserializer<Text>() {
        @Override
        public Text deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) {
            return json == null ? null : new Gson().fromJson(json.getAsString(), Text.class);
        }
    };

    public static JsonDeserializer<Date> deserDate = new JsonDeserializer<Date>() {
        @Override
        public Date deserialize(JsonElement json, Type type, JsonDeserializationContext jdc) throws
                JsonParseException {
            return new Date(json.getAsJsonPrimitive().getAsLong());
        }
    };
}

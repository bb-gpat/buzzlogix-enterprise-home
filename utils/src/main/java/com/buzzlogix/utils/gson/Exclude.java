/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.utils.gson;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import java.util.logging.Logger;

/**
 *
 * @author administrator
 */
public class Exclude implements ExclusionStrategy {

    private static final Logger logger = Logger.getLogger(Exclude.class.getName());

    @Override
    public boolean shouldSkipClass(Class<?> arg0) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean shouldSkipField(FieldAttributes field) {
        return false;
    }
}

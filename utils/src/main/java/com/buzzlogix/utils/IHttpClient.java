/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.utils;

import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.Map;

/**
 *
 * @author administrator
 */
public interface IHttpClient {

    /**
     *
     * @param url
     * @param jsonInput
     * @param header
     * @return
     */
    public String post(URL url, String jsonInput, Map<String, String> header);
    
    /**
     *
     * @param url
     * @return
     */
    public String get(URL url);

    /**
     *
     * @param uri
     * @param jsonInput
     * @return
     */
    public String post(String uri, String jsonInput);
    
    /**
     *
     * @param uri
     * @return
     */
    public String get(String uri);
    
    /**
     *
     * @param urlString
     * @return
     * @throws MalformedURLException
     * @throws SocketTimeoutException
     */
    public String getResponseApache(String urlString) throws MalformedURLException, SocketTimeoutException;
}

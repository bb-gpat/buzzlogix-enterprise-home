package com.buzzlogix.utils.debug;

import java.util.Enumeration;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ServletHelper {
	
	public static String getFormatedHeaders(HttpServletRequest req) {
		Enumeration headerNames = req.getHeaderNames();
		String s_headers = "";

		while (headerNames.hasMoreElements()) {
			String headerName = (String) headerNames.nextElement();
			s_headers = s_headers + " \r\n"
					+ (headerName + " : " + req.getHeader(headerName));
		}

		return s_headers;
	}

	public static String getFormatedParamNames(HttpServletRequest req) {
		Enumeration params = req.getParameterNames();
		String s_params = "";
		while (params.hasMoreElements()) {
			String paramName = (String) params.nextElement();
			s_params = s_params + "\r\n " + paramName;
		}

		return s_params;
	}

}

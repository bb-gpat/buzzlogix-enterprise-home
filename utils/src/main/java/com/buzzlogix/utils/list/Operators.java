/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.utils.list;

/**
 *
 * @author administrator
 */
public class Operators {
    
    private static void intAdded(Integer[] adders, final Integer[] adder) {
        Integer index = 0;

        for (Integer value : adder) {
            if (value != null) {
                adders[index] += value;
            } else {
                adder[index] = value;
            }
            index++;
        }
    }

    private static void longAdded(Long[] adders, final Long[] adder) {
        Integer index = 0;

        for (Long value : adder) {
            if (value != null) {
                adders[index] += value;
            } else {
                adders[index] = value;
            }
            index++;
        }
    }

    public static Object[] added(Object[] adders, final Object[] adder) {
        if (adder != null && adder.length > 0) {
            if (adders == null) {
                adders = adder;
            } else {
                if (adders instanceof Integer[] && adder instanceof Integer[]) {
                    intAdded((Integer[]) adders, (Integer[]) adder);
                } else if (adders instanceof Long[] && adder instanceof Long[]) {
                    longAdded((Long[]) adders, (Long[]) adder);
                }
            }
        }
        
        return adders;
    }

    private static void intSubtraction(Integer[] subtrahend, final Integer[] subtracter) {
        Integer index = 0;

        for (Integer value : subtracter) {
            if (value != null && subtrahend[index] != null && subtrahend[index].compareTo(value) > 0) {
                subtrahend[index++] -= value;
            }
        }
    }

    private static void longSubtraction(Long[] subtrahend, final Long[] subtracter) {
        Integer index = 0;

        for (Long value : subtracter) {
            if (value != null && subtrahend[index] != null && subtrahend[index].compareTo(value) > 0) {
                subtrahend[index++] -= value;
            }
        }
    }

    public static Object[] subtraction(Object[] subtrahend, final Object[] subtracter) {
        if (subtrahend != null && subtracter != null && subtracter.length > 0) {
            if (subtrahend instanceof Integer[] && subtracter instanceof Integer[]) {
                intSubtraction((Integer[]) subtrahend, (Integer[]) subtracter);
            } else if (subtrahend instanceof Long[] && subtracter instanceof Long[]) {
                longSubtraction((Long[]) subtrahend, (Long[]) subtracter);
            }
        }
        
        return subtrahend;
    }
}

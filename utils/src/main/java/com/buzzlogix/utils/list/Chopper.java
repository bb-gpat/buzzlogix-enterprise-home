package com.buzzlogix.utils.list;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Chopper {

    // chops a list into non-view sublists of length L
    public static List<Object>[] chopped(final List<?> original, final Integer amountOfSets) {
        if (original != null && !original.isEmpty() && amountOfSets != null && amountOfSets.compareTo(0) > 0) {
            List<Object>[] subsets = new List[amountOfSets];
            Object[] largesetarray = original.toArray(new Object[original.size()]);

            for (int i = 1; i <= amountOfSets; i++) {
                int fromIndex = (i - 1) * original.size() / amountOfSets;
                int toIndex = i * original.size() / amountOfSets - 1;
                List<Object> subHashSet = new ArrayList<>();

                subHashSet.addAll(Arrays.asList(Arrays.copyOfRange(largesetarray, fromIndex, toIndex)));
                subsets[i - 1] = subHashSet;
            }

            return subsets;
        } else {
            return null;
        }
    }
}

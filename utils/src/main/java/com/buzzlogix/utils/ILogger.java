/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.utils;

import java.util.logging.Handler;
import java.util.logging.Logger;

/**
 *
 * @author administrator
 */
public interface ILogger {
    public Logger getLoggerByHandler(String className, Handler handler); 
}

package com.buzzlogix.utils.locale;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;

public class LocaleHelper {

    public static List<String> getCountriesForDefaultLocale() {
        ArrayList<String> locationsSorted = new ArrayList<String>();
        for (Locale locale : Locale.getAvailableLocales()) {
            if (locale.getLanguage().equalsIgnoreCase("mk") || locale.getDisplayCountry().isEmpty()) {
                continue;
            }
            locationsSorted.add(locale.getDisplayCountry());
        }

        Collections.sort(locationsSorted);
        List<String> depdupeLocations
                = new ArrayList<>(new LinkedHashSet<>(locationsSorted));
        return depdupeLocations;
    }

    public static List<String> getLanguagesForDefaultLocale() {
        ArrayList<String> laguagesSorted = new ArrayList<>();
        for (Locale locale : Locale.getAvailableLocales()) {
            if (locale.getLanguage().equalsIgnoreCase("mk")) {
                continue;
            }
            laguagesSorted.add(locale.getDisplayLanguage());
        }
        Collections.sort(laguagesSorted);
        List<String> depdupeLanguages
                = new ArrayList<>(new LinkedHashSet<>(laguagesSorted));
        return depdupeLanguages;
    }

    public static String getCodeForDisplayCountry(String displayCountry) {
        String result = "";
        for (Locale locale : Locale.getAvailableLocales()) {
            if (locale.getDisplayCountry().equalsIgnoreCase(displayCountry)) {
                result = locale.getCountry().toUpperCase();
                break;
            }
        }
        return result;
    }

    public static String getCodeForDisplayLanguage(String displayLanguage) {
        String result = "";
        for (Locale locale : Locale.getAvailableLocales()) {
            if (locale.getDisplayLanguage().equalsIgnoreCase(displayLanguage)) {
                result = locale.getLanguage();
                break;
            }
        }
        return result;
    }

    public static String getDisplayCountryForCode(String countryCode) {
        String result = "";
        for (Locale locale : Locale.getAvailableLocales()) {
            if (locale.getCountry().equalsIgnoreCase(countryCode)) {
                result = locale.getDisplayCountry();
                break;
            }
        }
        return result;
    }

    public static String getDisplayLanguageForCode(String countryCode) {
        System.out.println("getDisplayLanguageForCode for code " + countryCode);
        String result = "";
        for (Locale locale : Locale.getAvailableLocales()) {
            if (locale.getLanguage().equalsIgnoreCase(countryCode)) {
                result = locale.getDisplayLanguage();
                break;
            }
        }

        System.out.println("result is " + result);
        return result;
    }

    public static String getEnglishDisplayLanguage(String langCode) {
        String result = "";
        
        for (Locale locale : Locale.getAvailableLocales()) {
            if (locale.getLanguage().equalsIgnoreCase(langCode)) {
                result = locale.getDisplayLanguage(Locale.US).toLowerCase();
                break;
            }
        }
        return result;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.utils.strings;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author administrator
 */
public class BuzzlogixStrings {

    public static String listToString(Set<?> list) {
        StringBuilder result = new StringBuilder();

        if (list != null && !list.isEmpty()) {
            Iterator it = list.iterator();

            while (it.hasNext()) {
                result.append(it.next()).append(",");
            }
        }

        return result.toString();
    }

    public static String listToString(Object[] list) {
        StringBuilder result = new StringBuilder();

        if (list != null && list.length > 0) {
            for (Object value : list) {
                result.append(value).append(",");
            }
        }

        return result.toString();
    }

    public static String listToString(List<?> list) {
        StringBuilder result = new StringBuilder();

        if (list != null && !list.isEmpty()) {
            Iterator it = list.iterator();

            while (it.hasNext()) {
                result.append(it.next()).append(",");
            }
        }

        return result.toString();
    }

    public static String mapToString(Map<?, ?> map) {
        StringBuilder result = new StringBuilder();

        if (map != null && !map.isEmpty()) {
            Iterator<?> it = map.keySet().iterator();

            while (it.hasNext()) {
                result.append(map.get(it.next())).append(",");
            }
        }

        return result.toString();
    }

    public static String toCapitalize(String givenString) {
        String[] arr = givenString.split(" ");
        StringBuilder sb = new StringBuilder();

        for (String arr1 : arr) {
            sb.append(Character.toUpperCase(arr1.charAt(0))).append(arr1.substring(1)).append(" ");
        }
        return sb.toString().trim();
    }

    public static String[] listToArray(final List<String> list) {
        if (list != null && !list.isEmpty()) {
            Object[] objectList = list.toArray();
            String[] stringArray = Arrays.copyOf(objectList, objectList.length, String[].class);
            
            return stringArray;
        } else {
            return null;
        }
    }
}

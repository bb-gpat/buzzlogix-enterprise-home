package com.buzzlogix.utils.seo;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class AlexaHelper {
	
	/*
	 * Returns alexarank for given URL. Should be used for blogs, news & discussions.
	 */
	public String getAlexaRank(String url) throws ClientProtocolException, IOException, ParserConfigurationException, SAXException
	{
		String result = null;		
		String target = "http://data.alexa.com/data?cli=10&dat=snbamz&url="+url;
		
		HttpClient httpclient = new DefaultHttpClient();
    	HttpGet httpget = new HttpGet(target);
    	org.apache.http.HttpResponse response = httpclient.execute(httpget);
    	HttpEntity entity = response.getEntity();
    	String responseString = EntityUtils.toString(entity, "UTF-8");
    	
    	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(new InputSource(new ByteArrayInputStream(responseString.getBytes("utf-8"))));
        doc.getDocumentElement().normalize();
     
        NodeList nList = doc.getElementsByTagName("SD");
        Node nNode = nList.item(1);
     	Element eElement = (Element) nNode;
    	NodeList nodeList = eElement.getElementsByTagName("POPULARITY"); 
    	Element popElement = (Element)nodeList.item(0);
    	result = popElement.getAttribute("TEXT");

		return result;
	}
}

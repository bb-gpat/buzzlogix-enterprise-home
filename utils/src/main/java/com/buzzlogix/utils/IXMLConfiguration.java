/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.utils;

import java.math.BigDecimal;
import org.jdom.Element;

/**
 *
 * @author administrator
 */
public interface IXMLConfiguration {

    public String getString(String name);

    public Integer getInteger(String name);

    public Long getLong(String name);

    public BigDecimal getDecimal(String name);

    public Boolean getBoolean(String name);
}

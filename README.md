# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

### Project Configuration ###
* For the configuration parameters reading  of projects you can use the interface XMLConfigurationImpl from the utils library
* The configuration file of the projects: datastore, distributor, parser and of the libraries: module, pubsub when used from the above projects must be into the modules-ear project and specific under the META-INF directory
* The configuration file of the project frontend and of the libraries module, pubsub when used from the above project must be into the specific project and specific under the WEB-INF/config directory

### Encrypting Code ###

### Data Entries URLs ###
The data of entities are in the file datastore-data.xml to json format is under the module modules-ear
For the insertion of all entities (Account, Billing, Channel, Dashboard, Group, Licence, Limit, Publisher, Report, Task, TopicStream, TopicStreamConfig, User, Widgets) the URL is:
http://localhost:8081/add/admin?file=datastore-data
For the insertion of the administrator user the URL is:
http://localhost:8081/add/noadmin?username=admin&password=admin&file=datastore-data


# **Problem Serialize/Deserialize Datastore Key structure** #
Unrecognized field "namespace" (Class com.google.appengine.api.datastore.Key), not marked as ignorable
## **Answer** ##
This means that there are get and set methods of an object in your class and Jackson is unable to figureout the serialization and deserialization process.
Use @JsonIgnore if you don't want the property to be serialized.
If you would like to have this data serialized and deserialized, it could be a problem with polymorphic references. Check Jackson's Annotation guide for appropriate usage. You may be interested in @JsonTypeInfo in such cases.
Our problem is that the Key structure is not editable from us.
## **My Solution** ##
So, we need to made a new structure base Key and we need to transfer the needed fields in ours, but this solution is very complex. However, we need the Key structure because we need the parent info from a detail entity.
For example, I suggest to get the group a specific user:
1. Fetch the specific user from entity User with criteria
2. From parent key fetch the specific group of specific user
So, I made a new unique id (UUID) for any entity, and the datastore sends to other application this unique key for any entity. The datastore key will used for the internal matching.
The previous example from the front end will become:
1. Fetch the specific user from entity User with unique id
2. From parent key fetch the specific group of specific user
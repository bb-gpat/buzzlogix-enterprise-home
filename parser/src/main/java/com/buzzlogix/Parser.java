package com.buzzlogix;

import com.buzzlogix.utils.impl.XMLConfigurationImpl;

public class Parser 
{
	private XMLConfigurationImpl api_key_config = new XMLConfigurationImpl("META-INF/provider_creds.xml");
	
	private Parser(){}
	private static class SingletonHolder {
		private static final Parser INSTANCE = new Parser();
	}

	public static Parser getInstance() {
		return SingletonHolder.INSTANCE;
	}
	
	public XMLConfigurationImpl getAPIKeyConfig()
	{
		return api_key_config;
	}
}

package com.buzzlogix.parser;

import com.buzzlogix.Parser;
import com.google.api.client.util.Strings;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.modules.ModulesServiceFactory;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.gson.Gson;
import java.io.IOException;
import java.util.logging.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.google.gson.GsonBuilder;
import com.buzzlogix.entity.bigquery.GenericPost;
import com.buzzlogix.enums.SourceTypeEnum;
import com.buzzlogix.nlp.FullResponse;
import com.buzzlogix.nlp.NlpApiClient;
import com.buzzlogix.utils.gson.InterfaceAdapter;
import com.buzzlogix.utils.gson.RegisterTypeAdapter;
import com.buzzlogix.utils.impl.HttpClientImpl;
import com.buzzlogix.utils.impl.LoggerImpl;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import twitter4j.ExtendedMediaEntity;
import twitter4j.HashtagEntity;
import twitter4j.MediaEntity;
import twitter4j.Place;
import twitter4j.Scopes;
import twitter4j.Status;
import twitter4j.SymbolEntity;
import twitter4j.URLEntity;
import twitter4j.User;
import twitter4j.UserMentionEntity;

public class TwitterParser extends HttpServlet {

    private Logger logger = new LoggerImpl().getLoggerByHandler(this.getClass().getName(), new ConsoleHandler());

    private Queue queue = QueueFactory.getQueue("twitter-to-datastore");

    @Override
    @SuppressWarnings({"unchecked", "rawtypes"})
    public final void doPost(final HttpServletRequest req, final HttpServletResponse resp) throws IOException {
        final String stream_id = req.getParameter("stream_id");
        Status[] tweets = gsonForStatus().fromJson(req.getParameter("twitter_status"), Status[].class);
        Set<GenericPost> genericPosts = new HashSet<>();

        if (tweets == null || tweets.length == 0) {
            return;
        }

        for (Status tweet : tweets) {
            if (tweet != null) {
                GenericPost genericPost = new GenericPost();
                NlpApiClient nlpClient = new NlpApiClient(Parser.getInstance().getAPIKeyConfig().getString("NLP_accessToken"));
                SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy h:mm:ss a");
                
                genericPost.setPublished("published:" + tweet.getCreatedAt().getTime());
                genericPost.setId("id:" + UUID.randomUUID().toString());
                genericPost.setStreamId("streamId:" + stream_id);
                genericPost.setSource("source:" + SourceTypeEnum.TWITTER.name());
                genericPost.setUrl("url:" + "http://www.twitter.com/" + tweet.getUser().getScreenName() + "/status/" + tweet.getId());
                genericPost.setLocation((tweet.getUser() != null) ? "location:" + tweet.getUser().getLocation() : "");
                genericPost.setAuthor((tweet.getUser() != null) ? "author:" + tweet.getUser().getScreenName() : "");
                genericPost.setImgMain((tweet.getUser() != null) ? "imgMain:" + tweet.getUser().getProfileImageURL() : "");
                genericPost.setTitle("title:" + tweet.getUser().getName());
                genericPost.setText("text:" + tweet.getText());
                genericPost.setLanguage("language:" + tweet.getLang());
                if (tweet.getMediaEntities() != null && tweet.getMediaEntities().length != 0) {
                    List<String> links = new ArrayList<>();

                    for (MediaEntity mediaEntity : tweet.getMediaEntities()) {
                        if (mediaEntity.getType().equalsIgnoreCase("photo") || (mediaEntity.getType().equalsIgnoreCase("animated_gif"))) {
                            links.add(mediaEntity.getMediaURL());
                        }
                    }

                    genericPost.setExternalLinks("externalLinks:" + new Gson().toJson(links));
                }
                if (!Strings.isNullOrEmpty(genericPost.getLanguage())
                        && (genericPost.getLanguage().equalsIgnoreCase("en") || genericPost.getLanguage().equalsIgnoreCase("de"))) {
                    try {
                        FullResponse nlp_response = nlpClient.getCustomAnalysis(tweet.getText(), tweet.getLang(), NlpApiClient.SENTIMENT_TYPE_TWITTER, false, false);

                        if (nlp_response != null) {
                            genericPost.setSentiment("sentiment:" + nlp_response.sentiment.Predicted_Class);
                        }
                        //TODO Keywords should be broken down and added to categories if option is set to auto categorize.
                        //genPost.setKeywords(nlp_response.keywords_response.keywords); 
                        // for when we get Gender in NLP genPost.setGender(GenderEnum.valueOf(GenderEnum.class, gender_as_string));
                    } catch (IOException ex) {
                        logger.log(Level.INFO, null, ex.getStackTrace());
                    }
                }
                genericPosts.add(genericPost);
            }
        }
        if (genericPosts != null && !genericPosts.isEmpty()) {
            String posts = new GsonBuilder()
                    .registerTypeAdapter(Text.class, new RegisterTypeAdapter().ser)
                    .create()
                    .toJson(genericPosts.toArray(), GenericPost[].class);

            String target_host = ModulesServiceFactory.getModulesService().getVersionHostname("default", "v1");

            if (Boolean.valueOf(System.getProperty("Local"))) {
                Map<String, String> header = new HashMap<String, String>() {
                    {
                        put("streamId", stream_id);
                    }
                };
                new HttpClientImpl().post(new URL("http://" + target_host + "/generic_post/upsert-local"), posts, header);
            } else {
                queue.add(TaskOptions.Builder
                        .withUrl("/generic_post/upsert")
                        .header("Host", target_host)
                        .param("streamId", stream_id)
                        .param("genericPost", posts));
            }
        }
    }

    private Gson gsonForStatus() {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Status.class, new InterfaceAdapter<Status>())
                .registerTypeAdapter(User.class, new InterfaceAdapter<User>())
                .registerTypeAdapter(UserMentionEntity.class, new InterfaceAdapter<UserMentionEntity>())
                .registerTypeAdapter(MediaEntity.class, new InterfaceAdapter<MediaEntity>())
                .registerTypeAdapter(MediaEntity.Size.class, new InterfaceAdapter<MediaEntity.Size>())
                .registerTypeAdapter(HashtagEntity.class, new InterfaceAdapter<HashtagEntity>())
                .registerTypeAdapter(URLEntity.class, new InterfaceAdapter<URLEntity>())
                .registerTypeAdapter(Place.class, new InterfaceAdapter<Place>())
                .registerTypeAdapter(ExtendedMediaEntity.class, new InterfaceAdapter<ExtendedMediaEntity>())
                .registerTypeAdapter(SymbolEntity.class, new InterfaceAdapter<SymbolEntity>())
                .registerTypeAdapter(Scopes.class, new InterfaceAdapter<Scopes>())
                .create();

        return gson;
    }
}

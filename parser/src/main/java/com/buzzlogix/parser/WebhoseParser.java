package com.buzzlogix.parser;

import com.buzzlogix.Parser;
import com.google.api.client.util.Strings;
import com.google.appengine.api.datastore.Text;

import java.io.IOException;
import java.text.SimpleDateFormat;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.google.appengine.api.modules.ModulesServiceFactory;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.webhose.domain.WebhosePost;
import com.buzzlogix.entity.bigquery.GenericPost;
import com.buzzlogix.nlp.FullResponse;
import com.buzzlogix.nlp.NlpApiClient;
import com.buzzlogix.utils.gson.RegisterTypeAdapter;
import com.buzzlogix.utils.impl.HttpClientImpl;
import com.buzzlogix.utils.impl.LoggerImpl;
import java.math.BigDecimal;
import java.net.URL;
import java.text.ParseException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class WebhoseParser extends HttpServlet {

    private static final Logger logger = new LoggerImpl().getLoggerByHandler(WebhoseParser.class.getName(), new ConsoleHandler());

    private final Queue queue = QueueFactory.getQueue("webhose-to-datastore");

    private static Long Counter = 0l;

    @Override
    @SuppressWarnings(
            {
                "unchecked", "rawtypes"
            })
    public final void doPost(final HttpServletRequest req, final HttpServletResponse resp) throws IOException {
        Gson gson = new Gson();
        final String stream_id = req.getParameter("stream_id");
        WebhosePost[] posts = gson.fromJson(req.getParameter("webhose_posts"), WebhosePost[].class);
        SimpleDateFormat webhose_date_format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        Set<GenericPost> genericPosts = new HashSet<>();

        logger.log(Level.INFO, "Parser Counter :{0},{1}", new Object[]{++Counter, posts.length});

        if (posts != null) {
            NlpApiClient nlpClient = new NlpApiClient(Parser.getInstance().getAPIKeyConfig().getString("NLP_accessToken"));
            for (WebhosePost post : posts) {
                GenericPost genPost = makeGenericPost(stream_id, post, webhose_date_format, true);

                if (!Strings.isNullOrEmpty(genPost.getLanguage())
                        && (genPost.getLanguage().equalsIgnoreCase("en") || genPost.getLanguage().equalsIgnoreCase("de"))) {
                    try {
                        int sentimentType = (post.text.length() > 140) ? NlpApiClient.SENTIMENT_TYPE_GENERAL : NlpApiClient.SENTIMENT_TYPE_TWITTER;
                        FullResponse nlp_response = nlpClient.getCustomAnalysis(post.text, genPost.getLanguage(), sentimentType, false, false);

                        if (nlp_response != null) {
                            genPost.setSentiment("sentiment:" + nlp_response.sentiment.Predicted_Class);
                        }
                        //TODO Keywords should be broken down and added to categories if option is set to auto categorize.
                        //genPost.setKeywords(nlp_response.keywords_response.keywords); 
                        // for when we get Gender in NLP genPost.setGender(GenderEnum.valueOf(GenderEnum.class, gender_as_string));
                    } catch (IOException ex) {
                        logger.log(Level.SEVERE, null, ex);
                    }
                }
                genericPosts.add(genPost);
            }
        } else {
            //POSTS ARE NULL. THIS SHOULDNT HAPPEN BUT PUT SOME CODE TO HANDLE IT JUST IN CASE
        }

        if (!genericPosts.isEmpty()) {
            String jsonPost = new GsonBuilder()
                    .registerTypeAdapter(Text.class, new RegisterTypeAdapter().ser)
                    .create()
                    .toJson(genericPosts.toArray(), GenericPost[].class);

            if (Boolean.valueOf(System.getProperty("Local"))) {
                Map<String, String> header = new HashMap<String, String>() {
                    {
                        put("streamId", stream_id);
                    }
                };
                String target_host = ModulesServiceFactory.getModulesService().getVersionHostname("default", "v1");
                new HttpClientImpl().post(new URL("http://" + target_host + "/generic_post/upsert-local"), jsonPost, header);
            } else {
                String queue_target_host = ModulesServiceFactory.getModulesService().getVersionHostname("default", "v1");

                queue.add(TaskOptions.Builder
                        .withUrl("/generic_post/upsert")
                        .header("Host", queue_target_host)
                        .param("streamId", stream_id)
                        .param("genericPost", jsonPost));
            }
        }
    }

    public GenericPost makeGenericPost(String streamId, WebhosePost post, SimpleDateFormat dateformat, boolean autoTag) {
        GenericPost genericPost = new GenericPost();
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy h:mm:ss a");

        genericPost.setId("id:" + UUID.randomUUID().toString());
        genericPost.setStreamId("streamId:" + streamId);
        genericPost.setSource("source:" + post.thread.site_type.toUpperCase());
        genericPost.setUrl("url:" + post.url);
        try {
//            genericPost.setCrawled(dateformat.parse(post.crawled));
            genericPost.setPublished("published:" + dateformat.parse(post.published).getTime());
        } catch (ParseException ex) {
            logger.log(Level.SEVERE, null, ex);
        }
        genericPost.setLocation("location:" + post.thread.country);
        genericPost.setTitle("title:" + post.title);
        genericPost.setText("text:" + post.text);
        genericPost.setAuthor("author:" + post.author);
        if (post.language != null && (!post.language.isEmpty())) {
            for (Locale locale : Locale.getAvailableLocales()) {
                if (locale.getDisplayLanguage(Locale.US).equalsIgnoreCase(post.language)) {
                    genericPost.setLanguage("language:" + locale.getLanguage());
                    break;
                }
            }
        } else {
            genericPost.setLanguage("");
        }

        genericPost.setSpamScore("spamScore:" + new BigDecimal(post.thread.spam_score).toString());
        genericPost.setPerformanceScore("performanceScore:" + Integer.toString(post.thread.performance_score));
        genericPost.setImgMain("imgMain:" + post.thread.main_image);

        if (autoTag) {
            String dbtags = (genericPost.getTags() != null) ? genericPost.getTags() : "";
            Set<String> tags = new HashSet<>();

            if (post.persons != null && !post.persons.isEmpty()) {
                tags.addAll(post.persons);
            }
            if (post.locations != null && !post.locations.isEmpty()) {
                tags.addAll(post.locations);
            }
            if (post.organizations != null && !post.organizations.isEmpty()) {
                tags.addAll(post.organizations);
            }
            dbtags += new Gson().toJson(tags);
            genericPost.setTags("tags:" + dbtags);
        }

        return genericPost;
    }
}

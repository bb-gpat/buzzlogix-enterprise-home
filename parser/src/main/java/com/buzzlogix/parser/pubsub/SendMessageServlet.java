/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.parser.pubsub;

import java.io.IOException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author administrator
 * 
 * Publishes messages to the application topic.
 */
@SuppressWarnings("serial")
public class SendMessageServlet extends HttpServlet {

    @Override
    public final void doPost(final HttpServletRequest req, final HttpServletResponse resp) throws IOException {
    }
}

package com.webhose.domain;

import com.google.api.client.util.Key;

import java.io.Serializable;

public class WebhoseThread implements Serializable {

	@Key
	public String url;

	@Key()
	public String site_type;

	@Key
	public String site_full;

	@Key
	public String site;

	@Key
	public String site_section;

	@Key
	public String section_title;

	@Key
	public String title;

	@Key
	public String title_full;

	@Key
	public String published;

	@Key
	public int replies_count;

	@Key
	public int participants_count;

	@Key
	public String country;

	@Key
	public double spam_score;

	@Key
	public int performance_score;

	@Key
	public String main_image;
}

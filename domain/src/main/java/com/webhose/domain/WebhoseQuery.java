package com.webhose.domain;

import com.google.appengine.repackaged.com.google.api.client.util.Strings;
import com.google.common.base.Joiner;

import java.util.ArrayList;
import java.util.List;

public class WebhoseQuery {

    public final List<String> allTerms = new ArrayList<>();
    public final List<String> someTerms = new ArrayList<>();
    public final List<SiteType> siteTypes = new ArrayList<>();
    public final List<String> language = new ArrayList<>();
    public final List<String> sites = new ArrayList<>();
    public final List<String> persons = new ArrayList<>();
    public final List<String> locations = new ArrayList<>();
    public final List<String> organizations = new ArrayList<>();
    public String phrase;
    public String exclude;
    public String title;
    public String bodyText;

    @Override
    public String toString() {
        List<String> terms = new ArrayList<>();

        if (allTerms != null && !allTerms.isEmpty()) {
            addTerm(terms, allTerms, "AND", null);
        }
        if (someTerms != null && !someTerms.isEmpty()) {
            addTerm(terms, someTerms, "OR", null);
        }
        if (!Strings.isNullOrEmpty(phrase)) {
            terms.add("\"" + phrase + "\"");
        }
        if (!Strings.isNullOrEmpty(exclude)) {
            terms.add("-(" + exclude + ")");
        }
        if (siteTypes != null && !siteTypes.isEmpty()) {
            addTerm(terms, siteTypes, "OR", "site_type");
        }
        if (language != null && !language.isEmpty()) {
            addTerm(terms, language, "OR", "language");
        }
        if (sites != null && !sites.isEmpty()) {
            addTerm(terms, sites, "OR", "site");
        }
        if (persons != null && !persons.isEmpty()) {
            addTerm(terms, persons, "OR", "person");
        }
        if (locations != null && !locations.isEmpty()) {
            addTerm(terms, locations, "OR", "thread.country");
        }
        if (organizations != null && !organizations.isEmpty()) {
            addTerm(terms, organizations, "OR", "organization");
        }
        if (!Strings.isNullOrEmpty(title)) {
            terms.add("title:(" + title + ")");
        }
        if (!Strings.isNullOrEmpty(bodyText)) {
            terms.add("text:(" + bodyText + ")");
        }

        return Joiner.on(" AND ").join(terms);
    }

    private void addTerm(List<String> terms, List<?> parts, String boolOp, String fieldName) {
        StringBuilder sb = new StringBuilder();
        boolean first = true;

        sb.append("(");
        for (Object part : parts) {
            if (first) {
                first = false;
            } else {
                sb.append(" ").append(boolOp).append(" ");
            }
            if (fieldName != null) {
                sb.append(fieldName).append(":");
            }
            if (part.toString().contains(" ")) {
                sb.append('"').append(part).append('"');
            } else {
                sb.append(part);
            }
        }
        sb.append(")");
        terms.add(sb.toString());
    }

    public static enum SiteType {

        news, blogs, discussions
    }
}

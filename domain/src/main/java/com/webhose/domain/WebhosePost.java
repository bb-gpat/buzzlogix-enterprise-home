package com.webhose.domain;

import com.google.api.client.util.Key;

import java.io.Serializable;
import java.util.List;

public class WebhosePost implements Serializable {

	@Key
	public String url;

	@Key
	public String title;

	@Key
	public String author;

	@Key
	public String text;

	@Key
	public String published;

	@Key
	public String crawled;

	@Key
	public int ord_in_thread;

	@Key
	public String language;

	@Key
	public List<String> external_links;

	@Key
	public List<String> persons;

	@Key
	public List<String> locations;

	@Key
	public List<String> organizations;

	@Key
	public WebhoseThread thread;

}

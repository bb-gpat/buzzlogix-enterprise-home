package com.webhose.domain;

import com.google.api.client.util.Key;
import java.util.List;

public class WebhoseResponse {

	@Key
	public int totalResults;

	@Key
	public String next;

	@Key
	public int requestsLeft;

	@Key
	public int moreResultsAvailable;

	@Key
	public List<WebhosePost> posts;
}

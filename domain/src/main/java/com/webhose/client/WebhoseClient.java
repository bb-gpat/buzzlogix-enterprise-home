package com.webhose.client;

import com.buzzlogix.utils.impl.HttpClientImpl;
import com.buzzlogix.utils.impl.LoggerImpl;
import com.google.api.client.http.*;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.Key;
import com.google.appengine.repackaged.com.google.api.client.util.Strings;
import com.google.gson.Gson;
import com.webhose.domain.WebhoseQuery;
import com.webhose.domain.WebhoseResponse;

import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class WebhoseClient {

    static final JsonFactory JSON_FACTORY = new JacksonFactory();
    private static final Logger logger = new LoggerImpl().getLoggerByHandler(WebhoseClient.class.getName(), new ConsoleHandler());
    static HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
    private final HttpRequestFactory requestFactory;

    private final String apiKey;

    public WebhoseClient(String apiKey) {
        this.requestFactory
                = HTTP_TRANSPORT.createRequestFactory(new HttpRequestInitializer() {
                    public void initialize(HttpRequest request) {
                        request.setParser(new JsonObjectParser(JSON_FACTORY));
                    }
                });

        this.apiKey = apiKey;
    }

    public WebhoseResponse search(WebhoseQuery query) throws IOException {
        return search(query.toString());
    }

    public WebhoseResponse search(String query) throws IOException {
        WebhoseResponse response = null;
        WebhoseUrl url = new WebhoseUrl("http://webhose.io/search");

        url.token = this.apiKey;
        url.query = query;

        String result = new HttpClientImpl().getResponseApache(url.toURL().toString());

        try {
            response = new Gson().fromJson(result, WebhoseResponse.class);
        } catch (Exception ex) {
            response = null;
            new Throwable(result);
        } finally {
            return response;
        }
    }

    /*
     * Note : Timestamps should always include trailing zero's!!!
     */
    public WebhoseResponse searchSince(String query, Long timestamp) throws IOException {
        WebhoseResponse response = null;
        WebhoseUrl url = new WebhoseUrl("https://webhose.io/search");

        url.token = this.apiKey;
        url.query = query;
        url.fromTimestamp = timestamp;

        String result = new HttpClientImpl().getResponseApache(url.toURL().toString());

        logger.log(Level.INFO, "since url={0}", url.toURL().toString());

        try {
            response = new Gson().fromJson(result, WebhoseResponse.class);
        } catch (Exception ex) {
            response = null;
            new Throwable(result);
        } finally {
            return response;
        }
    }

    public WebhoseResponse searchSince(WebhoseQuery query, Long timestamp) throws IOException {
        return searchSince(query.toString(), timestamp);
    }

    /*
     * Untested
     */
    public WebhoseResponse searchSince(WebhoseQuery query, Date date) throws IOException {
        return searchSince(query.toString(), date.getTime());
    }

    /*
     * Untested
     */
    public WebhoseResponse searchSince(String query, Date date) throws IOException {
        return searchSince(query, date.getTime());
    }

    public WebhoseResponse getMore(String next) throws IOException {
        WebhoseUrl url = new WebhoseUrl("https://webhose.io" + next);
        String result = new HttpClientImpl().getResponseApache(url.toURL().toString());

        logger.log(Level.INFO, "more url={0}", url.toURL().toString());

        return (!Strings.isNullOrEmpty(result)) ? new Gson().fromJson(result, WebhoseResponse.class) : null;
    }

    public WebhoseResponse getMore(WebhoseResponse response) throws IOException {
        return getMore(response.next);
    }

    /*
     * Should become scrapper for Url's mapped to omgili.
     */
    public void testRedirect(String url) throws IOException {
        GenericUrl gurl = new GenericUrl(url);
        HttpRequest request = requestFactory.buildGetRequest(gurl);
        HttpResponse resp = request.execute();
        //System.out.println(resp.parseAsString());

        /*	HttpClient httpclient = new DefaultHttpClient();
         HttpGet httpget = new HttpGet(url);
         org.apache.http.HttpResponse response = httpclient.execute(httpget);
         HttpEntity entity = response.getEntity();
         String responseString = EntityUtils.toString(entity, "UTF-8");
         System.out.println(responseString); */
    }

    public static class WebhoseUrl extends GenericUrl {

        @Key
        public String token;
        @Key("q")
        public String query;
        @Key
        public String format = "json";
        @Key("ts")
        public Long fromTimestamp;

        public WebhoseUrl(String encodedUrl) {
            super(encodedUrl);
        }

        public WebhoseUrl(URL url) {
            super(url);
        }
    }
}

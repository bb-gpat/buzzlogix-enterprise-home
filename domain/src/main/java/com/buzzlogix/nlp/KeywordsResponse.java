package com.buzzlogix.nlp;

import com.google.api.client.util.Key;

import java.util.HashMap;

public class KeywordsResponse {
	@Key
	public HashMap<String, Integer> keywords;

}

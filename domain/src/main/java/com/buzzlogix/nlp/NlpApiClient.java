package com.buzzlogix.nlp;

import com.buzzlogix.utils.impl.LoggerImpl;
import com.google.api.client.http.*;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.Strings;

import java.io.IOException;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NlpApiClient {

	public static final int SENTIMENT_TYPE_GENERAL = 0;
	public static final int SENTIMENT_TYPE_TWITTER = 1;
	static final JsonFactory JSON_FACTORY = new JacksonFactory();
	static HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
	private final HttpRequestFactory requestFactory;
	private final String apiKey;
	private Logger logger = new LoggerImpl().getLoggerByHandler(NlpApiClient.class.getName(), new ConsoleHandler());

	public NlpApiClient(String apiKey) {
		this.requestFactory =
				HTTP_TRANSPORT.createRequestFactory(new HttpRequestInitializer() {
					public void initialize(HttpRequest request) {
						request.setParser(new JsonObjectParser(JSON_FACTORY));
					}
				});

		this.apiKey = apiKey;
	}

	public SentimentResponse getSentiment(String query) throws IOException {
		GenericUrl url = new GenericUrl("https://buzzlogix-text-analysis.p.mashape.com/sentiment");
		final HttpContent content = new ByteArrayContent("text/plain", query.getBytes());
		HttpRequest request = requestFactory.buildPostRequest(url, content);
		HttpHeaders headers = new HttpHeaders();
		headers.set("X-Mashape-Key", this.apiKey);
		request.setHeaders(headers);
		return request.execute().parseAs(SentimentResponse.class);
	}

	public SentimentResponse getTwitterSentiment(String query) throws IOException {

		GenericUrl url = new GenericUrl("https://buzzlogix-text-analysis.p.mashape.com/twittersentiment");
		final HttpContent content = new ByteArrayContent("text/plain", query.getBytes());
		HttpRequest request = requestFactory.buildPostRequest(url, content);
		HttpHeaders headers = new HttpHeaders();
		headers.set("X-Mashape-Key", this.apiKey);
		request.setHeaders(headers);
		return request.execute().parseAs(SentimentResponse.class);
	}

	public KeywordsResponse getKeywords(String query) throws IOException {
		GenericUrl url = new GenericUrl("https://buzzlogix-text-analysis.p.mashape.com/keywords");
		final HttpContent content = new ByteArrayContent("text/plain", query.getBytes());
		HttpRequest request = requestFactory.buildPostRequest(url, content);
		HttpHeaders headers = new HttpHeaders();
		headers.set("X-Mashape-Key", this.apiKey);
		request.setHeaders(headers);
		return request.execute().parseAs(KeywordsResponse.class);
	}

	public ObjectivityResponse getObjectivity(String query) throws IOException {
		GenericUrl url = new GenericUrl("https://buzzlogix-text-analysis.p.mashape.com/objectivity");
		final HttpContent content = new ByteArrayContent("text/plain", query.getBytes());
		HttpRequest request = requestFactory.buildPostRequest(url, content);
		HttpHeaders headers = new HttpHeaders();
		headers.set("X-Mashape-Key", this.apiKey);
		request.setHeaders(headers);
		return request.execute().parseAs(ObjectivityResponse.class);
	}

	public FullResponse getCompleteAnalysis(String query, String language, int sentimentType) throws IOException {
		GenericUrl url = new GenericUrl("https://buzzlogix-text-analysis.p.mashape.com/full");
		final HttpContent content = new ByteArrayContent("text/plain", query.getBytes());
		HttpRequest request = requestFactory.buildPostRequest(url, content);
		HttpHeaders headers = new HttpHeaders();
		headers.set("X-Mashape-Key", this.apiKey);
		headers.set("X-Buzzlogix-Keywords", "1");
		headers.set("X-Buzzlogix-Objectivity", "1");

		if (Strings.isNullOrEmpty(language)) {
			headers.set("X-Buzzlogix-Language", "en");
		} else {
			headers.set("X-Buzzlogix-Language", language);
		}

		if (sentimentType == SENTIMENT_TYPE_TWITTER) {
			headers.set("X-Buzzlogix-Sentiment", "twitter");
		} else {
			headers.set("X-Buzzlogix-Sentiment", "general");
		}
		request.setHeaders(headers);
		return request.execute().parseAs(FullResponse.class);
	}

	public String getCompleteAnalysisString(String query, String language, int sentimentType) throws IOException {
		GenericUrl url = new GenericUrl("https://buzzlogix-text-analysis.p.mashape.com/full");
		final HttpContent content = new ByteArrayContent("text/plain", query.getBytes());
		HttpRequest request = requestFactory.buildPostRequest(url, content);
		HttpHeaders headers = new HttpHeaders();
		headers.set("X-Mashape-Key", this.apiKey);
		headers.set("X-Buzzlogix-Keywords", "1");
		headers.set("X-Buzzlogix-Objectivity", "1");

		if (Strings.isNullOrEmpty(language)) {
			headers.set("X-Buzzlogix-Language", "en");
		} else {
			headers.set("X-Buzzlogix-Language", language);
		}

		if (sentimentType == SENTIMENT_TYPE_TWITTER) {
			headers.set("X-Buzzlogix-Sentiment", "twitter");
		} else {
			headers.set("X-Buzzlogix-Sentiment", "general");
		}
		request.setHeaders(headers);
		return request.execute().parseAsString();
	}

	public FullResponse getCustomAnalysis(String query, String language, int sentimentType, boolean keywords, boolean objectivity) throws IOException {
		GenericUrl url = new GenericUrl("https://buzzlogix-text-analysis.p.mashape.com/full");
		final HttpContent content = new ByteArrayContent("text/plain", query.getBytes());
		HttpRequest request = requestFactory.buildPostRequest(url, content);
		HttpHeaders headers = new HttpHeaders();

		headers.set("X-Mashape-Key", this.apiKey);

		if (Strings.isNullOrEmpty(language)) {
			headers.set("X-Buzzlogix-Language", "en");
		} else {
			headers.set("X-Buzzlogix-Language", language);
		}

		if (keywords) {
			headers.set("X-Buzzlogix-Keywords", "1");
		} else {
			headers.set("X-Buzzlogix-Keywords", "0");
		}

		if (objectivity) {
			headers.set("X-Buzzlogix-Objectivity", "1");
		} else {
			headers.set("X-Buzzlogix-Objectivity", "0");
		}

		if (sentimentType == SENTIMENT_TYPE_TWITTER) {
			headers.set("X-Buzzlogix-Sentiment", "twitter");
		} else {
			headers.set("X-Buzzlogix-Sentiment", "general");
		}
		request.setHeaders(headers);

		HttpResponse response = request.execute();

		if (response == null) {
			logger.log(Level.INFO, "no response nlp with return error:{0}:{1}", new Object[]{response.getStatusCode(), response.getStatusMessage()});
		}

		return (response != null) ? request.execute().parseAs(FullResponse.class) : null;
	}
}

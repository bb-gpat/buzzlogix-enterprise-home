package com.buzzlogix.nlp;

import com.google.api.client.util.Key;

public class FullResponse {

	@Key
	public SentimentResponse sentiment;

	@Key
	public KeywordsResponse keywords;

	@Key
	public ObjectivityResponse objectivity;
}

package com.buzzlogix.nlp;

import com.google.api.client.util.Key;

public class ObjectivityResponse {
	@Key
	public String Classified_Sentence;

	@Key
	public String Predicted_Class;

	@Key
	public float Probability;

	@Override
	public String toString() {
		return "\r\nClassified_Sentence : " + Classified_Sentence + "\r\nPredicted_Class : " + Predicted_Class + "\r\nProbability : " + Probability;
	}
}

package com.buzzlogix.enums;

public enum SentimentEnum {

	ALL("All"), POSITIVE("Positive"), NEUTRAL("Neutral"), NEGATIVE("Negative");

	private String name;

	private SentimentEnum(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}
}

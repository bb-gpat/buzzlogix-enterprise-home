package com.buzzlogix.enums;

public enum ChannelTypeEnum {

	TWITTER,
	FACEBOOK,
	INSTAGRAM;
}

package com.buzzlogix.enums;

public enum UserRoleEnum {

	ADMIN,
	GROUP_ADMIN,
	EDITOR,
	VIEWER;
}

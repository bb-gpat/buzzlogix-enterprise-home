package com.buzzlogix.enums;

public enum StreamStatusEnum {

	STARTING("Starting"), RUNNING("Running"), STOPPED("Stopped");

	private String name;

	private StreamStatusEnum(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}
}

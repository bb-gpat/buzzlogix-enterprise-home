/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.info;

import java.util.HashMap;
import java.util.Map;

/**
 * @author administrator
 */
public class PubSubMessage {

	private String topic;

	private String subscription;

	private String channel;

	private String action;

	private Map<String, String> parameter = new HashMap<>();

	private String body;

	private PubSubMessage(Builder builder) {
		topic = builder.topic;
		subscription = builder.subscription;
		channel = builder.channel;
		action = builder.action;
		parameter = builder.parameter;
		body = builder.body;
	}

	public String getTopic() {
		return topic;
	}

	public String getSubscription() {
		return subscription;
	}

	public String getChannel() {
		return channel;
	}

	public String getAction() {
		return action;
	}

	public Map<String, String> getParameter() {
		return parameter;
	}

	public String getInfo() {
		return body;
	}

	public static class Builder {

		private String topic;

		private String subscription;

		private String channel;

		private String action;

		private Map<String, String> parameter = new HashMap<>();
		;

		private String body;

		public Builder setTopic(String topic) {
			this.topic = topic;

			return this;
		}

		public Builder setSubscription(String subscription) {
			this.subscription = subscription;

			return this;
		}

		public Builder setChannel(String channel) {
			this.channel = channel;

			return this;
		}

		public Builder setAction(String action) {
			this.action = action;

			return this;
		}

		public Builder setParameter(Map<String, String> parameter) {
			this.parameter = parameter;

			return this;
		}

		public Builder setBody(String body) {
			this.body = body;

			return this;
		}

		public PubSubMessage build() {
			return new PubSubMessage(this);
		}
	}
}

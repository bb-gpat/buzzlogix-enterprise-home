/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.dao.licence;

import com.buzzlogix.dao.GenericDaoImpl;
import com.buzzlogix.entity.Licence;
import com.google.api.client.util.Strings;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * @author administrator
 */
public class LicenceDaoImpl implements LicenceDao {

	private static final Logger logger = Logger.getLogger(LicenceDaoImpl.class.getName());

	private final GenericDaoImpl<Licence> generic = new GenericDaoImpl<>();

	/**
	 * @param licence
	 * @return
	 */
	@Override
	public Licence upsert(final Licence licence) {
		Licence result = null;

		if (licence.getId() == null) {
			result = generic.insert(licence);
		} else {
			final Map<String, Object> parameters = new HashMap<String, Object>() {
				{
					put("id", licence.getId());
				}
			};
			result = new LicenceDaoImpl().getSingleByQuery("com.buzzlogix.entity.Licence.findById", parameters);

			if (result != null) {
				generic.change(result, licence);
			}
		}

		return result;
	}

	/**
	 * @param namedQuery
	 * @param parameters
	 * @param page
	 * @param size
	 * @return
	 */
	@Override
	public List getListByQuery(String namedQuery, Map<String, Object> parameters, Integer page, Integer size) {
		List<Licence> result = null;

		if (!Strings.isNullOrEmpty(namedQuery)) {
			result = generic.getListByQuery(Licence.class, namedQuery, parameters, page, size);
		}

		return result;
	}

	/**
	 * @param namedQuery
	 * @param parameters
	 * @return
	 */
	@Override
	public Licence getSingleByQuery(String namedQuery, Map<String, Object> parameters) {
		Licence result = null;

		if (!Strings.isNullOrEmpty(namedQuery)) {
			result = generic.getSingleByQuery(Licence.class, namedQuery, parameters);
		}

		return result;
	}


	/**
	 * @param namedQuery
	 * @param parameters
	 * @return
	 */
	@Override
	public Integer getCountByQuery(String namedQuery, Map<String, Object> parameters) {
		Integer result = null;

		if (!Strings.isNullOrEmpty(namedQuery)) {
			result = generic.getCountByQuery(Licence.class, namedQuery, parameters);
		}

		return result;
	}

	/**
	 * @param namedQuery
	 * @param parameters
	 * @return
	 */
	@Override
	public Integer delete(String namedQuery, Map<String, Object> parameters) {
		Integer result = null;

		if (!Strings.isNullOrEmpty(namedQuery)) {
			result = generic.delete(Licence.class, namedQuery, parameters);
		}

		return result;
	}
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.dao;

import java.util.List;
import java.util.Map;

/**
 * @param <MODEL>
 * @author administrator
 */
public interface GenericDao<MODEL> {

	/**
	 * @param glazz
	 * @param namedQuery
	 * @param parameters
	 * @param page
	 * @param size
	 * @return
	 */
	public List getListByQuery(Class glazz, String namedQuery, Map<String, Object> parameters, Integer page, Integer size);

	/**
	 * @param glazz
	 * @param namedQuery
	 * @param parameters
	 * @return
	 */
	public MODEL getSingleByQuery(Class glazz, String namedQuery, Map<String, Object> parameters);

	/**
	 * @param glazz
	 * @param namedQuery
	 * @param parameters
	 * @return
	 */
	public Integer getCountByQuery(Class glazz, String namedQuery, Map<String, Object> parameters);

	/**
	 * @param glazz
	 * @param query
	 * @param page
	 * @param size
	 * @return
	 */
	public List getListByFilterOrder(Class glazz, String query, Integer page, Integer size);

	/**
	 * @param glazz
	 * @param query
	 * @return
	 */
	public MODEL getSingleByFilter(Class glazz, String query);

	/**
	 * @param models
	 * @return
	 */
	public MODEL[] insert(MODEL[] models);

	/**
	 * @param models
	 * @return
	 */
	public MODEL insert(MODEL models);

	/**
	 * @param model
	 * @return
	 */
	public MODEL[] update(MODEL[] model);

	/**
	 * @param model
	 * @return
	 */
	public MODEL update(MODEL model);

	/**
	 * @param glazz
	 * @param namedQuery
	 * @param parameters
	 * @return
	 */
	public Integer delete(Class glazz, String namedQuery, Map<String, Object> parameters);

	/**
	 * @param result
	 * @param values
	 * @return
	 */
	public MODEL change(MODEL result, MODEL values);

	/**
	 * @param result
	 * @param values
	 * @return
	 */
	public MODEL changeValues(MODEL result, Object values);

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.dao.location;

import com.buzzlogix.dao.GenericDaoImpl;
import com.buzzlogix.entity.Account;
import com.buzzlogix.entity.Location;
import com.buzzlogix.utils.impl.LoggerImpl;
import com.google.api.client.util.Strings;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.ConsoleHandler;
import java.util.logging.Logger;

/**
 *
 * @author administrator
 */
public class LocationDaoImpl implements LocationDao {

    private static final Logger logger = new LoggerImpl().getLoggerByHandler(LocationDaoImpl.class.getName(), new ConsoleHandler());

    private final GenericDaoImpl<Location> generic = new GenericDaoImpl<>();

    /**
     * @param location
     * @return
     */
    @Override
    public Location upsert(final Location location) {
        Location result = null;

        if (location.getId() == null) {
            result = generic.insert(location);
        } else {
            final Map<String, Object> parameters = new HashMap<String, Object>() {
                {
                    put("id", location.getId());
                }
            };
            result = new LocationDaoImpl().getSingleByQuery("com.buzzlogix.entity.Location.findById", parameters);

            if (result != null) {
                generic.change(result, location);
            }
        }

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @param page
     * @param size
     * @return
     */
    @Override
    public List getListByQuery(String namedQuery, Map<String, Object> parameters, Integer page, Integer size) {
        List<Location> result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.getListByQuery(Account.class, namedQuery, parameters, page, size);
        }

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @return
     */
    @Override
    public Location getSingleByQuery(String namedQuery, Map<String, Object> parameters) {
        Location result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.getSingleByQuery(Location.class, namedQuery, parameters);
        }

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @return
     */
    @Override
    public Integer getCountByQuery(String namedQuery, Map<String, Object> parameters) {
        Integer result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.getCountByQuery(Location.class, namedQuery, parameters);
        }

        return result;
    }

    @Override
    public Integer delete(String namedQuery, Map<String, Object> parameters) {
        Integer result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.delete(Location.class, namedQuery, parameters);
        }

        return result;
    }
}

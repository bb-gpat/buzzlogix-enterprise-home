/*
 Copyright 2015, Google, Inc.
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package com.buzzlogix.dao;

import com.google.api.services.bigquery.Bigquery;
import com.google.api.services.bigquery.Bigquery.Datasets;
import com.google.api.services.bigquery.model.*;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

/**
 * Helper functions for the other classes.
 */
public class BigqueryUtils {

    /**
     * Private contructor to prevent creation of this class, which is just all static helper methods.
     */
    protected BigqueryUtils() {
    }

    /**
     * Polls the job for completion.
     *
     * @param request The bigquery request to poll for completion
     * @param interval Number of milliseconds between each poll
     * @return The finished job
     * @throws IOException IOException
     * @throws InterruptedException InterruptedException
     */
    // [START poll_job]
    public static Job pollJob(final Bigquery.Jobs.Get request, final long interval) throws IOException, InterruptedException {
        Job job = request.execute();

        while (!job.getStatus().getState().equals("DONE")) {
            Thread.sleep(interval);

            job = request.execute();
        }

        return job;
    }
    // [END poll_job]

    /**
     * Loads a Bigquery schema.
     *
     * @param schemaSource The source of the schema
     * @return The TableSchema
     */
    // [START load_schema]
    public static TableSchema loadSchema(final Reader schemaSource) {
        TableSchema sourceSchema = new TableSchema();

        List<TableFieldSchema> fields = (new Gson())
                .<List<TableFieldSchema>>fromJson(schemaSource,
                        (new ArrayList<TableFieldSchema>()).getClass());

        sourceSchema.setFields(fields);

        return sourceSchema;
    }
	// [END load_schema]

	// [START list_datasets]
    /**
     * Display all BigQuery datasets associated with a project.
     *
     * @param bigquery an authorized BigQuery client
     * @param projectId a string containing the current project ID
     * @return
     * @throws IOException Thrown if there is a network error connecting to Bigquery.
     */
    public static List<DatasetList.Datasets> listDatasets(final Bigquery bigquery, final String projectId) throws IOException {
        List<DatasetList.Datasets> datasets = null;
        Datasets.List datasetRequest = bigquery.datasets().list(projectId);
        DatasetList datasetList = datasetRequest.execute();

        if (datasetList.getDatasets() != null) {
            datasets = datasetList.getDatasets();
        }

        return datasets;
    }

    /**
     * Inserts a Query Job for a particular query
     *
     * @param bigquery
     * @param projectId
     * @param querySql
     * @return
     * @throws java.io.IOException
     */
    public static JobReference startQuery(Bigquery bigquery, String projectId, String querySql) throws IOException {
        Job job = new Job();
        JobConfiguration config = new JobConfiguration();
        JobConfigurationQuery queryConfig = new JobConfigurationQuery();

        config.setQuery(queryConfig);
        job.setConfiguration(config);
        queryConfig.setQuery(querySql);

        Bigquery.Jobs.Insert insert = bigquery.jobs().insert(projectId, job);

        insert.setProjectId(projectId);
        JobReference jobId = insert.execute().getJobReference();

        return jobId;
    }

    /**
     * Polls the status of a BigQuery job, returns TableReference to results if "DONE"
     *
     * @param bigquery
     * @param projectId
     * @param jobId
     * @return
     * @throws java.io.IOException
     * @throws java.lang.InterruptedException
     */
    public static TableReference checkQueryResults(Bigquery bigquery, String projectId, JobReference jobId) throws IOException, InterruptedException {
        // Variables to keep track of total query time
        long startTime = System.currentTimeMillis();
        long elapsedTime;

        while (true) {
            Job pollJob = bigquery.jobs().get(projectId, jobId.getJobId()).execute();
            elapsedTime = System.currentTimeMillis() - startTime;
            System.out.format("Job status (%dms) %s: %s\n", elapsedTime,
                    jobId.getJobId(), pollJob.getStatus().getState());
            if (pollJob.getStatus().getState().equals("DONE")) {
                return pollJob.getConfiguration().getQuery().getDestinationTable();
            }
			// Pause execution for one second before polling job status again, to
            // reduce unnecessary calls to the BigQUery API and lower overall
            // application bandwidth.
            Thread.sleep(1000);
        }
    }

    /**
     * Page through the result set
     *
     * @param bigquery
     * @param page
     * @param size
     * @param completedJob
     * @return
     * @throws java.io.IOException
     */
    public static List<TableRow> displayQueryResults(Bigquery bigquery, Integer page, Long size, TableReference completedJob) throws IOException {
        List<TableRow> rows = null;
        String pageToken = null;
        Integer pg = 0;
        // Default to not looping
        boolean moreResults = false;

        do {
            TableDataList queryResult = bigquery.tabledata().list(
                    completedJob.getProjectId(),
                    completedJob.getDatasetId(),
                    completedJob.getTableId())
                    .setMaxResults(size)
                    .setPageToken(pageToken)
                    .execute();

            rows = queryResult.getRows();

            if (queryResult.getPageToken() != null) {
                pageToken = queryResult.getPageToken();
                moreResults = true;
                pg++;
            }
        } while (moreResults && pg.compareTo(page) < 0);

        return rows;
    }
    
    /**
     * Page through the result set
     *
     * @param bigquery
     * @param completedJob
     * @return
     * @throws java.io.IOException
     */
    public static List<TableRow> displayQueryResults(Bigquery bigquery, TableReference completedJob) throws IOException {
        List<TableRow> rows = null;
        
        TableDataList queryResult = bigquery.tabledata().list(
                    completedJob.getProjectId(),
                    completedJob.getDatasetId(),
                    completedJob.getTableId())
                    .execute();

        rows = queryResult.getRows();

        return rows;
    }
	// [END list_datasets]
}

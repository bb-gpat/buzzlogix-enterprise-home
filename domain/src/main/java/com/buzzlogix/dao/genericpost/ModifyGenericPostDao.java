/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.dao.genericpost;

import com.buzzlogix.entity.ModifyGenericPost;

import java.util.List;
import java.util.Map;

/**
 * @author administrator
 */
public interface ModifyGenericPostDao {

	public ModifyGenericPost upsert(ModifyGenericPost modify);

	public List getListByQuery(String namedQuery, Map<String, Object> parameters, Integer page, Integer size);

	public ModifyGenericPost getSingleByQuery(String namedQuery, Map<String, Object> parameters);

	public Integer delete(String namedQuery, Map<String, Object> parameters);
}

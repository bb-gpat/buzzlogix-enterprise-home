package com.buzzlogix.dao.genericpost;

import com.buzzlogix.dao.BigQueryDaoImpl;
import com.buzzlogix.entity.bigquery.GenericPost;
import com.buzzlogix.utils.impl.LoggerImpl;
import com.google.api.services.bigquery.model.TableCell;
import com.google.api.services.bigquery.model.TableDataInsertAllResponse;
import com.google.api.services.bigquery.model.TableRow;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author administrator
 */
public class GenericPostDaoImpl implements GenericPostDao {

    private final static Logger logger = new LoggerImpl().getLoggerByHandler(GenericPostDaoImpl.class.getName(), new ConsoleHandler());

    private final static BigQueryDaoImpl service = new BigQueryDaoImpl();

    /**
     * @param tableId
     * @return
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    public String createTable(final String tableId) throws IOException, InterruptedException {
        Map<String, String> fields = new HashMap<String, String>() {
            {
                put("id", "STRING");
                put("streamId", "STRING");
                put("source", "STRING");
                put("url", "STRING");
                put("title", "STRING");
                put("author", "STRING");
                put("text", "STRING");
                put("language", "STRING");
                put("externalLinks", "STRING");
                put("location", "STRING");
                put("published", "STRING");
                put("added", "STRING");
                put("updated", "STRING");
                put("sentiment", "STRING");
                put("keywords", "STRING");
                put("gender", "STRING");
                put("tags", "STRING");
                put("spamScore", "STRING");
                put("performanceScore", "STRING");
                put("imgMain", "STRING");
                put("imgProfile", "STRING");
                put("extras", "STRING");
            }
        };

        return service.createTable(tableId, fields);
    }
    
    /**
     * @param tableId
     * @return
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    public String deleteTable(final String tableId) throws IOException, InterruptedException {
        return service.deleteTable(tableId);
    }

    /**
     * @param tableId
     * @param values
     * @return
     * @throws IOException
     */
    @Override
    public Iterator<TableDataInsertAllResponse> insert(
            final String tableId,
            final String values) throws IOException {
        JsonReader rows = new JsonReader(new StringReader(values));

        Iterator<TableDataInsertAllResponse> responses = service.insert(tableId, rows);

        if (responses != null) {
            while (responses.hasNext()) {
                logger.log(Level.INFO, "push data ={0}", responses.next());
            }
        }

        rows.close();

        return responses;
    }

    /**
     * @param query
     * @param page
     * @param size
     * @return
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    public String runQuery(
            String query,
            Integer page,
            Long size) throws IOException, InterruptedException {
        String result = null;
        List<TableRow> rows = service.runQuery(query, page, size);

        if (rows != null && !rows.isEmpty()) {
            GenericPost[] posts = new GenericPost[rows.size()];
            Integer i = 0;

            for (TableRow row : rows) {
                posts[i++] = new GenericPost(row);
            }

            result = new Gson().toJson(posts, GenericPost[].class);
        }

        return result;
    }

    /**
     * @param query
     * @return
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    public Long countQuery(String query) throws IOException, InterruptedException {
        Long result = 0l;
        List<TableRow> rows = service.countQuery(query);

        if (rows != null && !rows.isEmpty()) {
            List<TableCell> cells = rows.get(0).getF();

            if (cells != null && !cells.isEmpty()) {
                result = (cells.get(0).getV() instanceof String) ? Long.parseLong((String) cells.get(0).getV()) : 0l;
            }
        }

        return result;
    }
}

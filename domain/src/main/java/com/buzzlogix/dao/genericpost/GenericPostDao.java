package com.buzzlogix.dao.genericpost;

import com.google.api.services.bigquery.model.TableDataInsertAllResponse;

import java.io.IOException;
import java.util.Iterator;

/**
 * @author administrator
 */
public interface GenericPostDao {

    /**
     * @param tableId
     * @return
     * @throws IOException
     * @throws InterruptedException
     */
    public String createTable(final String tableId) throws IOException, InterruptedException;

    /**
     * @param tableId
     * @return
     * @throws IOException
     * @throws InterruptedException
     */
    public String deleteTable(final String tableId) throws IOException, InterruptedException;
    
    /**
     * @param tableId
     * @param values
     * @return
     * @throws IOException
     */
    Iterator<TableDataInsertAllResponse> insert(final String tableId, final String values
    ) throws IOException ;

    /**
     * @param query
     * @param page
     * @param size
     * @return
     * @throws IOException
     * @throws InterruptedException
     */
    public String runQuery(String query, Integer page, Long size) throws IOException, InterruptedException;

    /**
     * @param query
     * @return
     * @throws IOException
     * @throws InterruptedException
     */
    public Long countQuery(String query) throws IOException, InterruptedException;
}

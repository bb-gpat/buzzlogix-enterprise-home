/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.dao.genericpost;

import com.buzzlogix.dao.GenericDaoImpl;
import com.buzzlogix.dao.publisher.PublisherDaoImpl;
import com.buzzlogix.entity.Account;
import com.buzzlogix.entity.ModifyGenericPost;
import com.buzzlogix.utils.impl.LoggerImpl;
import com.google.api.client.util.Strings;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.ConsoleHandler;
import java.util.logging.Logger;

/**
 * @author administrator
 */
public class ModifyGenericPostDaoImpl implements ModifyGenericPostDao {
	private static final Logger logger = new LoggerImpl().getLoggerByHandler(PublisherDaoImpl.class.getName(), new ConsoleHandler());

	private final GenericDaoImpl<ModifyGenericPost> generic = new GenericDaoImpl<>();

	@Override
	public ModifyGenericPost upsert(final ModifyGenericPost modify) {
		ModifyGenericPost result = null;

		if (modify.getId() == null) {
			result = generic.insert(modify);
		} else {
			final Map<String, Object> parameters = new HashMap<String, Object>() {
				{
					put("id", modify.getId());
				}
			};
			result = new ModifyGenericPostDaoImpl().getSingleByQuery("com.buzzlogix.entity.ModifyGenericPost.findById", parameters);

			if (result != null) {
				generic.change(result, modify);
			}
		}

		return result;
	}

	@Override
	public List getListByQuery(String namedQuery, Map<String, Object> parameters, Integer page, Integer size) {
		List<ModifyGenericPost> result = null;

		if (!Strings.isNullOrEmpty(namedQuery)) {
			result = generic.getListByQuery(Account.class, namedQuery, parameters, page, size);
		}

		return result;
	}

	@Override
	public ModifyGenericPost getSingleByQuery(String namedQuery, Map<String, Object> parameters) {
		ModifyGenericPost result = null;

		if (!Strings.isNullOrEmpty(namedQuery)) {
			result = generic.getSingleByQuery(ModifyGenericPost.class, namedQuery, parameters);
		}

		return result;
	}

	@Override
	public Integer delete(String namedQuery, Map<String, Object> parameters) {
		Integer result = null;

		if (!Strings.isNullOrEmpty(namedQuery)) {
			result = generic.delete(ModifyGenericPost.class, namedQuery, parameters);
		}

		return result;
	}
}

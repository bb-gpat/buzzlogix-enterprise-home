/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.dao.limit;

import com.buzzlogix.dao.GenericDaoImpl;
import com.buzzlogix.entity.Limit;
import com.google.api.client.util.Strings;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * @author administrator
 */
public class LimitDaoImpl implements LimitDao {

	private static final Logger logger = Logger.getLogger(LimitDaoImpl.class.getName());

	private final GenericDaoImpl<Limit> generic = new GenericDaoImpl<>();

	/**
	 * @param limit
	 * @return
	 */
	@Override
	public Limit upsert(final Limit limit) {
		Limit result = null;

		if (limit.getId() == null) {
			result = generic.insert(limit);
		} else {
			final Map<String, Object> parameters = new HashMap<String, Object>() {
				{
					put("id", limit.getId());
				}
			};
			result = new LimitDaoImpl().getSingleByQuery("com.buzzlogix.entity.Limit.findById", parameters);

			if (result != null) {
				generic.change(result, limit);
			}
		}

		return result;
	}

	/**
	 * @param namedQuery
	 * @param parameters
	 * @param page
	 * @param size
	 * @return
	 */
	@Override
	public List getListByQuery(String namedQuery, Map<String, Object> parameters, Integer page, Integer size) {
		List<Limit> result = null;

		if (!Strings.isNullOrEmpty(namedQuery)) {
			result = generic.getListByQuery(Limit.class, namedQuery, parameters, page, size);
		}

		return result;
	}

	/**
	 * @param namedQuery
	 * @param parameters
	 * @return
	 */
	@Override
	public Limit getSingleByQuery(String namedQuery, Map<String, Object> parameters) {
		Limit result = null;

		if (!Strings.isNullOrEmpty(namedQuery)) {
			result = generic.getSingleByQuery(Limit.class, namedQuery, parameters);
		}

		return result;
	}


	/**
	 * @param namedQuery
	 * @param parameters
	 * @return
	 */
	@Override
	public Integer getCountByQuery(String namedQuery, Map<String, Object> parameters) {
		Integer result = null;

		if (!Strings.isNullOrEmpty(namedQuery)) {
			result = generic.getCountByQuery(Limit.class, namedQuery, parameters);
		}

		return result;
	}

	/**
	 * @param namedQuery
	 * @param parameters
	 * @return
	 */
	@Override
	public Integer delete(String namedQuery, Map<String, Object> parameters) {
		Integer result = null;

		if (!Strings.isNullOrEmpty(namedQuery)) {
			result = generic.delete(Limit.class, namedQuery, parameters);
		}

		return result;
	}
}

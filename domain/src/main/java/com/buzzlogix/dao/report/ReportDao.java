/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.dao.report;

import com.buzzlogix.entity.Report;

import java.util.List;
import java.util.Map;

/**
 * @author administrator
 */
public interface ReportDao {

	/**
	 * @param report
	 * @return
	 */
	public Report upsert(Report report);

	/**
	 * @param namedQuery
	 * @param parameters
	 * @param page
	 * @param size
	 * @return
	 */
	public List getListByQuery(String namedQuery, Map<String, Object> parameters, Integer page, Integer size);

	/**
	 * @param namedQuery
	 * @param parameters
	 * @return
	 */
	public Report getSingleByQuery(String namedQuery, Map<String, Object> parameters);


	/**
	 * @param namedQuery
	 * @param parameters
	 * @return
	 */
	public Integer getCountByQuery(String namedQuery, Map<String, Object> parameters);

	/**
	 * @param namedQuery
	 * @param parameters
	 * @return
	 */
	public Integer delete(String namedQuery, Map<String, Object> parameters);
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.dao.report;

import com.buzzlogix.dao.GenericDaoImpl;
import com.buzzlogix.entity.Report;
import com.google.api.client.util.Strings;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * @author administrator
 */
public class ReportDaoImpl implements ReportDao {

    private static final Logger logger = Logger.getLogger(ReportDaoImpl.class.getName());

    private final GenericDaoImpl<Report> generic = new GenericDaoImpl<>();

    /**
     * @param report
     * @return
     */
    @Override
    public Report upsert(final Report report) {
        Report result = null;

        if (report.getId() == null) {
            result = generic.insert(report);
        } else {
            final Map<String, Object> parameters = new HashMap<String, Object>() {
                {
                    put("id", report.getId());
                }
            };
            result = new ReportDaoImpl().getSingleByQuery("com.buzzlogix.entity.Report.findById", parameters);

            if (result != null) {
                generic.change(result, report);
            }
        }

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @param page
     * @param size
     * @return
     */
    @Override
    public List getListByQuery(String namedQuery, Map<String, Object> parameters, Integer page, Integer size) {
        List<Report> result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.getListByQuery(Report.class, namedQuery, parameters, page, size);
        }

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @return
     */
    @Override
    public Report getSingleByQuery(String namedQuery, Map<String, Object> parameters) {
        Report result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.getSingleByQuery(Report.class, namedQuery, parameters);
        }

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @return
     */
    @Override
    public Integer getCountByQuery(String namedQuery, Map<String, Object> parameters) {
        Integer result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.getCountByQuery(Report.class, namedQuery, parameters);
        }

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @return
     */
    @Override
    public Integer delete(String namedQuery, Map<String, Object> parameters) {
        Integer result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.delete(Report.class, namedQuery, parameters);
        }

        return result;
    }
}

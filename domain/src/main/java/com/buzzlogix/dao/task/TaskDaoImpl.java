/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.dao.task;

import com.buzzlogix.dao.GenericDaoImpl;
import com.buzzlogix.entity.Task;
import com.google.api.client.util.Strings;

import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * @author administrator
 */
public class TaskDaoImpl implements TaskDao {

    private static final Logger logger = Logger.getLogger(TaskDaoImpl.class.getName());

    private final GenericDaoImpl<Task> generic = new GenericDaoImpl<>();

    /**
     * @param task
     * @return
     */
    @Override
    public Task insert(final Task task) {
        Task result = null;

        result = generic.insert(task);

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @param page
     * @param size
     * @return
     */
    @Override
    public List getListByQuery(String namedQuery, Map<String, Object> parameters, Integer page, Integer size) {
        List<Task> result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.getListByQuery(Task.class, namedQuery, parameters, page, size);
        }

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @return
     */
    @Override
    public Task getSingleByQuery(String namedQuery, Map<String, Object> parameters) {
        Task result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.getSingleByQuery(Task.class, namedQuery, parameters);
        }

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @return
     */
    @Override
    public Integer getCountByQuery(String namedQuery, Map<String, Object> parameters) {
        Integer result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.getCountByQuery(Task.class, namedQuery, parameters);
        }

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @return
     */
    @Override
    public Integer delete(String namedQuery, Map<String, Object> parameters) {
        Integer result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.delete(Task.class, namedQuery, parameters);
        }

        return result;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.dao;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

/**
 * @author administrator
 */
public class DatastoreModelImpl implements DatastoreModel {

	String id;

	/**
	 * @return
	 */
	@Override
	public Key getKey() {
		return new KeyFactory.Builder("", "").getKey();
	}

	/**
	 * @return
	 */
	@Override
	public String getId() {
		return this.id;
	}

	/**
	 * @param id
	 */
	@Override
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @param values
	 * @return
	 */
	@Override
	public DatastoreModel changeValues(final Object values) {
		return this;
	}
}

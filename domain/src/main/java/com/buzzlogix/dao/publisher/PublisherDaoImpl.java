/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.dao.publisher;

import com.buzzlogix.dao.GenericDaoImpl;
import com.buzzlogix.entity.Publisher;
import com.buzzlogix.utils.impl.LoggerImpl;
import com.google.api.client.util.Strings;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.ConsoleHandler;
import java.util.logging.Logger;

/**
 * @author administrator
 */
public class PublisherDaoImpl implements PublisherDao {

    private static final Logger logger = new LoggerImpl().getLoggerByHandler(PublisherDaoImpl.class.getName(), new ConsoleHandler());

    private final GenericDaoImpl<Publisher> generic = new GenericDaoImpl<>();

    /**
     * @param publisher
     * @return
     */
    @Override
    public Publisher upsert(final Publisher publisher) {
        Publisher result = null;

        if (publisher.getId() == null) {
            result = generic.insert(publisher);
        } else {
            final Map<String, Object> parameters = new HashMap<String, Object>() {
                {
                    put("id", publisher.getId());
                }
            };
            result = new PublisherDaoImpl().getSingleByQuery("com.buzzlogix.entity.Publisher.findById", parameters);

            if (result != null) {
                generic.change(result, publisher);
            }
        }

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @param page
     * @param size
     * @return
     */
    @Override
    public List getListByQuery(String namedQuery, Map<String, Object> parameters, Integer page, Integer size) {
        List<Publisher> result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.getListByQuery(Publisher.class, namedQuery, parameters, page, size);
        }

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @return
     */
    @Override
    public Publisher getSingleByQuery(String namedQuery, Map<String, Object> parameters) {
        Publisher result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.getSingleByQuery(Publisher.class, namedQuery, parameters);
        }

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @return
     */
    @Override
    public Integer getCountByQuery(String namedQuery, Map<String, Object> parameters) {
        Integer result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.getCountByQuery(Publisher.class, namedQuery, parameters);
        }

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @return
     */
    @Override
    public Integer delete(String namedQuery, Map<String, Object> parameters) {
        Integer result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.delete(Publisher.class, namedQuery, parameters);
        }

        return result;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.dao.publisher;

import com.buzzlogix.entity.Publisher;

import java.util.List;
import java.util.Map;

/**
 * @author administrator
 */
public interface PublisherDao {

	/**
	 * @param publisher
	 * @return
	 */
	public Publisher upsert(Publisher publisher);

	/**
	 * @param namedQuery
	 * @param parameters
	 * @param page
	 * @param size
	 * @return
	 */
	public List getListByQuery(String namedQuery, Map<String, Object> parameters, Integer page, Integer size);

	/**
	 * @param namedQuery
	 * @param parameters
	 * @return
	 */
	public Publisher getSingleByQuery(String namedQuery, Map<String, Object> parameters);


	/**
	 * @param namedQuery
	 * @param parameters
	 * @return
	 */
	public Integer getCountByQuery(String namedQuery, Map<String, Object> parameters);

	/**
	 * @param namedQuery
	 * @param parameters
	 * @return
	 */
	public Integer delete(String namedQuery, Map<String, Object> parameters);
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.dao.channel;

import com.buzzlogix.dao.GenericDaoImpl;
import com.buzzlogix.entity.Channel;
import com.google.api.client.util.Strings;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * @author administrator
 */
public class ChannelDaoImpl implements ChannelDao {

    private static final Logger logger = Logger.getLogger(ChannelDaoImpl.class.getName());

    private final GenericDaoImpl<Channel> generic = new GenericDaoImpl<>();

    /**
     * @param channel
     * @return
     */
    @Override
    public Channel upsert(final Channel channel) {
        Channel result = null;

        if (channel.getId() == null) {
            result = generic.insert(channel);
        } else {
            final Map<String, Object> parameters = new HashMap<String, Object>() {
                {
                    put("id", channel.getId());
                }
            };
            result = new ChannelDaoImpl().getSingleByQuery("com.buzzlogix.entity.Channel.findById", parameters);

            if (result != null) {
                generic.change(result, channel);
            }
        }

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @param page
     * @param size
     * @return
     */
    @Override
    public List getListByQuery(String namedQuery, Map<String, Object> parameters, Integer page, Integer size) {
        List<Channel> result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.getListByQuery(Channel.class, namedQuery, parameters, page, size);
        }

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @return
     */
    @Override
    public Channel getSingleByQuery(String namedQuery, Map<String, Object> parameters) {
        Channel result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.getSingleByQuery(Channel.class, namedQuery, parameters);
        }

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @return
     */
    @Override
    public Integer getCountByQuery(String namedQuery, Map<String, Object> parameters) {
        Integer result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.getCountByQuery(Channel.class, namedQuery, parameters);
        }

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @return
     */
    @Override
    public Integer delete(String namedQuery, Map<String, Object> parameters) {
        Integer result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.delete(Channel.class, namedQuery, parameters);
        }

        return result;
    }
}

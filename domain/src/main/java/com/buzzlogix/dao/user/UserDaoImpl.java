/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.dao.user;

import com.buzzlogix.dao.GenericDaoImpl;
import com.buzzlogix.entity.User;
import com.buzzlogix.utils.impl.LoggerImpl;
import com.google.api.client.util.Strings;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.ConsoleHandler;
import java.util.logging.Logger;

/**
 * @author administrator
 */
public class UserDaoImpl implements UserDao {

    private static final Logger logger = new LoggerImpl().getLoggerByHandler(UserDaoImpl.class.getName(), new ConsoleHandler());

    private final GenericDaoImpl<User> generic = new GenericDaoImpl<>();

    /**
     * @param user
     * @return
     */
    @Override
    public User upsert(final User user) {
        User result = null;

        if (user != null) {
            if (user.getId() == null) {
                result = generic.insert(user);
            } else {
                final Map<String, Object> parameters = new HashMap<String, Object>() {
                    {
                        put("id", user.getId());
                    }
                };

                result = new UserDaoImpl().getSingleByQuery("com.buzzlogix.entity.User.findById", parameters);

                if (result != null) {
                    result = generic.change(result, user);
                }
            }
        }

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @param page
     * @param size
     * @return
     */
    @Override
    public List getListByQuery(String namedQuery, Map<String, Object> parameters, Integer page, Integer size) {
        List<User> result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.getListByQuery(User.class, namedQuery, parameters, page, size);
        }

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @return
     */
    @Override
    public User getSingleByQuery(String namedQuery, Map<String, Object> parameters) {
        User result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.getSingleByQuery(User.class, namedQuery, parameters);
        }

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @return
     */
    @Override
    public Integer getCountByQuery(String namedQuery, Map<String, Object> parameters) {
        Integer result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.getCountByQuery(User.class, namedQuery, parameters);
        }

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @return
     */
    @Override
    public Integer delete(String namedQuery, Map<String, Object> parameters) {
        Integer result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.delete(User.class, namedQuery, parameters);
        }

        return result;
    }
}

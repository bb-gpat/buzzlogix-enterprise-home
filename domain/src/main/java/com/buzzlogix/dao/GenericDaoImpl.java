/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.dao;

import com.buzzlogix.utils.impl.LoggerImpl;
import com.google.common.base.Strings;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @param <MODEL>
 * @author administrator
 */
public class GenericDaoImpl<MODEL extends DatastoreModel> implements GenericDao<MODEL> {

    private static final Logger logger = new LoggerImpl().getLoggerByHandler(GenericDaoImpl.class.getName(), new ConsoleHandler());

    private final EntityManager em = new EntityManagerDatastore().getEm();

    private List<MODEL> getResults(Class glazz, String namedQuery, Map<String, Object> parameters, Integer page, Integer size) {
        List<MODEL> models = null;

        try {
            TypedQuery<MODEL> query = em.createNamedQuery(namedQuery, glazz);

            if (parameters != null) {
                for (String key : parameters.keySet()) {
                    query.setParameter(key, parameters.get(key));
                }
            }

            if (page != null && size != null) {
                query.setFirstResult(page * size).setMaxResults(size);
            }

            models = query.getResultList();
        } catch (NoResultException ex) {
        } catch (Exception ex) {
            TypedQuery<MODEL> query = em.createNamedQuery(namedQuery, glazz);

            if (parameters != null) {
                for (String key : parameters.keySet()) {
                    query.setParameter(key, parameters.get(key));
                }
            }

            if (page != null && size != null) {
                query.setFirstResult(page * size).setMaxResults(size);
            }

            models = query.getResultList();
        }

        return models;
    }

    private MODEL getResult(Class glazz, String namedQuery, Map<String, Object> parameters) {
        MODEL model = null;

        try {
            TypedQuery<MODEL> query = em.createNamedQuery(namedQuery, glazz);

            if (parameters != null) {
                for (String key : parameters.keySet()) {
                    query.setParameter(key, parameters.get(key));
                }
            }

            model = query.getSingleResult();
        } catch (NoResultException ex) {
        } catch (Exception ex) {
            TypedQuery<MODEL> query = em.createNamedQuery(namedQuery, glazz);

            if (parameters != null) {
                for (String key : parameters.keySet()) {
                    query.setParameter(key, parameters.get(key));
                }
            }

            model = query.getSingleResult();
        }

        return model;
    }

    /**
     * @param models
     * @return
     */
    @Override
    public MODEL[] insert(MODEL[] models) {
        try {
            if (!em.getTransaction().isActive()) {
                em.getTransaction().begin();
            }
            for (MODEL model : models) {
                model.setId(UUID.randomUUID().toString());
                em.persist(model);
            }
            em.getTransaction().commit();
        } finally {
            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
            em.close();
        }

        return models;
    }

    /**
     * @param model
     * @return
     */
    @Override
    public MODEL insert(MODEL model) {
        try {
            if (!em.getTransaction().isActive()) {
                em.getTransaction().begin();
            }
            model.setId(UUID.randomUUID().toString());
            em.persist(model);

            em.getTransaction().commit();
        } finally {
            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
            em.close();
        }

        return model;
    }

    /**
     * @param models
     * @return
     */
    @Override
    public MODEL[] update(MODEL[] models) {
        try {
            if (!em.getTransaction().isActive()) {
                em.getTransaction().begin();
            }

            for (MODEL model : models) {
                model = em.merge(model);
            }

            em.getTransaction().commit();
        } finally {
            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
            em.close();
        }

        return (MODEL[]) models;
    }

    /**
     * @param model
     * @return
     */
    @Override
    public MODEL update(MODEL model) {
        try {
            if (!em.getTransaction().isActive()) {
                em.getTransaction().begin();
            }

            model = em.merge(model);

            em.getTransaction().commit();
        } finally {
            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
            em.close();
        }

        return model;
    }

    /**
     * @param result
     * @param values
     * @return
     */
    @Override
    public MODEL change(MODEL result, MODEL values) {
        MODEL model = (MODEL) result.changeValues(values);

        result = update(model);

        return result;
    }

    /**
     * @param result
     * @param values
     * @return
     */
    @Override
    public MODEL changeValues(MODEL result, Object values) {
        result = update((MODEL) result.changeValues(values));

        return result;
    }

    /**
     * @param glazz
     * @param namedQuery
     * @param parameters
     * @return
     */
    @Override
    public Integer delete(Class glazz, String namedQuery, Map<String, Object> parameters) {
        Integer result = 0;

        if (glazz != null && Strings.isNullOrEmpty(namedQuery)) {
            return null;
        }

        try {
            List<MODEL> models = getResults(glazz, namedQuery, parameters, null, null);

            if (models != null && !models.isEmpty()) {
                if (!em.getTransaction().isActive()) {
                    em.getTransaction().begin();
                }
                for (MODEL model : models) {
                    em.remove(model);
                    result++;
                }
                em.getTransaction().commit();
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, null, e);
        } finally {
            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
            em.close();
        }

        return result;
    }

    /**
     * @param glazz
     * @param namedQuery
     * @param parameters
     * @param page
     * @param size
     * @return
     */
    @Override
    public List getListByQuery(Class glazz, String namedQuery, Map<String, Object> parameters, Integer page, Integer size) {
        if (glazz != null && Strings.isNullOrEmpty(namedQuery)) {
            return null;
        }

        List<MODEL> models = getResults(glazz, namedQuery, parameters, page, size);

        em.close();

        return models;
    }

    /**
     * @param glazz
     * @param query
     * @param page
     * @param size
     * @return
     */
    @Override
    public List getListByFilterOrder(Class glazz, String query, Integer page, Integer size) {
        List<MODEL> models = null;

        if (glazz != null && Strings.isNullOrEmpty(query)) {
            return null;
        }

        try {
            TypedQuery<MODEL> _query = em.createQuery(query, glazz);

            _query.setFirstResult(page * size).setMaxResults(size);

            models = _query.getResultList();
        } catch (Exception e) {
            TypedQuery<MODEL> _query = em.createQuery(query, glazz);

            _query.setFirstResult(page * size).setMaxResults(size);

            models = _query.getResultList();
        } finally {
            em.close();
        }

        return models;
    }

    /**
     * @param glazz
     * @param query
     * @return
     */
    @Override
    public MODEL getSingleByFilter(Class glazz, String query) {
        MODEL models = null;

        if (glazz != null && Strings.isNullOrEmpty(query)) {
            return null;
        }

        try {
            TypedQuery<MODEL> _query = em.createQuery(query, glazz);

            models = _query.getSingleResult();
        } catch (Exception e) {
            TypedQuery<MODEL> _query = em.createQuery(query, glazz);

            models = _query.getSingleResult();
        } finally {
            em.close();
        }

        return models;
    }

    /**
     * @param glazz
     * @param namedQuery
     * @param parameters
     * @return
     */
    @Override
    public MODEL getSingleByQuery(Class glazz, String namedQuery, Map<String, Object> parameters) {
        if (glazz == null || Strings.isNullOrEmpty(namedQuery)) {
            return null;
        }

        MODEL model = getResult(glazz, namedQuery, parameters);

        em.close();

        return model;
    }

    /**
     * @param glazz
     * @param namedQuery
     * @param parameters
     * @return
     */
    @Override
    public Integer getCountByQuery(Class glazz, String namedQuery, Map<String, Object> parameters) {
        if (glazz == null || Strings.isNullOrEmpty(namedQuery)) {
            return null;
        }

        Integer count = getResults(glazz, namedQuery, parameters, null, null).size();

        em.close();

        return count;
    }

}

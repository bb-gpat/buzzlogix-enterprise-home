package com.buzzlogix.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * JPA implementation of the Clinic interface using EntityManagerDatastore.
 * <p>
 * <p>
 * The mappings are defined in "orm.xml" located in the META-INF directory.
 *
 * @author Mike Keith
 * @author Rod Johnson
 * @author Sam Brannen
 * @since 22.4.2006
 */
public class EntityManagerDatastore {

    private final EntityManagerFactory emf = Persistence.createEntityManagerFactory("buzzlogix");
    private final EntityManager em = emf.createEntityManager();

    /**
     * @return
     */
    public EntityManager getEm() {
        return em;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.dao.widget;

import com.buzzlogix.dao.GenericDaoImpl;
import com.buzzlogix.entity.Widget;
import com.google.api.client.util.Strings;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * @author administrator
 */
public class WidgetDaoImpl implements WidgetDao {

    private static final Logger logger = Logger.getLogger(WidgetDaoImpl.class.getName());

    private final GenericDaoImpl<Widget> generic = new GenericDaoImpl<>();

    /**
     * @param widget
     * @return
     */
    @Override
    public Widget upsert(final Widget widget) {
        Widget result = null;

        if (widget.getId() == null) {
            result = generic.insert(widget);
        } else {
            final Map<String, Object> parameters = new HashMap<String, Object>() {
                {
                    put("id", widget.getId());
                }
            };
            result = new WidgetDaoImpl().getSingleByQuery("com.buzzlogix.entity.Widget.findById", parameters);

            if (result != null) {
                result.changeValues(widget);

                generic.update(result);
            }
        }

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @param page
     * @param size
     * @return
     */
    @Override
    public List getListByQuery(String namedQuery, Map<String, Object> parameters, Integer page, Integer size) {
        List<Widget> result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.getListByQuery(Widget.class, namedQuery, parameters, page, size);
        }

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @return
     */
    @Override
    public Widget getSingleByQuery(String namedQuery, Map<String, Object> parameters) {
        Widget result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.getSingleByQuery(Widget.class, namedQuery, parameters);
        }

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @return
     */
    @Override
    public Integer getCountByQuery(String namedQuery, Map<String, Object> parameters) {
        Integer result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.getCountByQuery(Widget.class, namedQuery, parameters);
        }

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @return
     */
    @Override
    public Integer delete(String namedQuery, Map<String, Object> parameters) {
        Integer result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.delete(Widget.class, namedQuery, parameters);
        }

        return result;
    }
}

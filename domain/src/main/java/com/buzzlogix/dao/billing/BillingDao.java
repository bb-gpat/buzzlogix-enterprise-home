/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.dao.billing;

import com.buzzlogix.entity.Billing;

import java.util.List;
import java.util.Map;

/**
 * @author administrator
 */
public interface BillingDao {

	/**
	 * @param billing
	 * @return
	 */
	public Billing upsert(Billing billing);

	/**
	 * @param namedQuery
	 * @param parameters
	 * @param page
	 * @param size
	 * @return
	 */
	public List getListByQuery(String namedQuery, Map<String, Object> parameters, Integer page, Integer size);

	/**
	 * @param namedQuery
	 * @param parameters
	 * @return
	 */
	public Billing getSingleByQuery(String namedQuery, Map<String, Object> parameters);


	/**
	 * @param namedQuery
	 * @param parameters
	 * @return
	 */
	public Integer getCountByQuery(String namedQuery, Map<String, Object> parameters);

	/**
	 * @param namedQuery
	 * @param parameters
	 * @return
	 */
	public Integer delete(String namedQuery, Map<String, Object> parameters);
}

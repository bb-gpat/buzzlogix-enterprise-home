/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.dao.billing;

import com.buzzlogix.dao.GenericDaoImpl;
import com.buzzlogix.entity.Billing;
import com.google.api.client.util.Strings;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * @author administrator
 */
public class BillingDaoImpl implements BillingDao {

    private static final Logger logger = Logger.getLogger(BillingDaoImpl.class.getName());

    private final GenericDaoImpl<Billing> generic = new GenericDaoImpl<>();

    /**
     * @param billing
     * @return
     */
    @Override
    public Billing upsert(final Billing billing) {
        Billing result = null;

        if (billing.getId() == null) {
            result = generic.insert(billing);
        } else {
            final Map<String, Object> parameters = new HashMap<String, Object>() {
                {
                    put("id", billing.getId());
                }
            };
            result = new BillingDaoImpl().getSingleByQuery("com.buzzlogix.entity.Billing.findById", parameters);

            if (result != null) {
                generic.change(result, billing);
            }
        }

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @param page
     * @param size
     * @return
     */
    @Override
    public List getListByQuery(String namedQuery, Map<String, Object> parameters, Integer page, Integer size) {
        List<Billing> result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.getListByQuery(Billing.class, namedQuery, parameters, page, size);
        }

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @return
     */
    @Override
    public Billing getSingleByQuery(String namedQuery, Map<String, Object> parameters) {
        Billing result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.getSingleByQuery(Billing.class, namedQuery, parameters);
        }

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @return
     */
    @Override
    public Integer getCountByQuery(String namedQuery, Map<String, Object> parameters) {
        Integer result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.getCountByQuery(Billing.class, namedQuery, parameters);
        }

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @return
     */
    @Override
    public Integer delete(String namedQuery, Map<String, Object> parameters) {
        Integer result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.delete(Billing.class, namedQuery, parameters);
        }

        return result;
    }
}

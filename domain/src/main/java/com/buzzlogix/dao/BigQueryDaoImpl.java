/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.dao;

import com.buzzlogix.dao.genericpost.GenericPostDaoImpl;
import com.buzzlogix.utils.impl.LoggerImpl;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.services.bigquery.Bigquery;
import com.google.api.services.bigquery.model.*;
import com.google.appengine.repackaged.com.google.api.client.util.Strings;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;

import java.io.IOException;
import java.util.*;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author administrator
 */
public class BigQueryDaoImpl implements BigQueryDao {

    private final static Logger logger = new LoggerImpl().getLoggerByHandler(GenericPostDaoImpl.class.getName(), new ConsoleHandler());

    private final static String PROJECT_ID = "gp-test-1016";

    private final static String DATASET_ID = "gap";

    /**
     * Stream the given row into the given bigquery table.
     *
     * @param bigquery The bigquery service
     * @param projectId project id from Google Developers console
     * @param datasetId id of the dataset
     * @param tableId id of the table we're streaming
     * @param row the row we're inserting
     * @return Response from the insert
     * @throws IOException ioexception
     */
    // [START streamRow]
    public static TableDataInsertAllResponse streamRow(
            final Bigquery bigquery,
            final String projectId,
            final String datasetId,
            final String tableId,
            final TableDataInsertAllRequest.Rows row) throws IOException {
        return bigquery.tabledata().insertAll(
                projectId,
                datasetId,
                tableId,
                new TableDataInsertAllRequest().setRows(
                        Collections.singletonList(row))).execute();
    }

    /**
     * Create table.
     *
     * @param tableId tableid
     * @param fields
     * @return
     * @throws IOException Thrown if there is an error connecting to Bigquery.
     * @throws java.lang.InterruptedException
     */
    @Override
    public String createTable(final String tableId, final Map<String, String> fields) throws IOException, InterruptedException {
        String result = "OK";

        if (!Strings.isNullOrEmpty(tableId) && fields != null && !fields.isEmpty()) {
            Bigquery bigquery = BigqueryServiceFactory.getService();
            TableSchema schema = new TableSchema();
            List<TableFieldSchema> tableFieldSchema = new ArrayList<>();
            Table table = new Table();
            TableReference tableRef = new TableReference();

            for (String name : fields.keySet()) {
                String type = fields.get(name);

                if (type != null) {
                    TableFieldSchema schemaEntry = new TableFieldSchema()
                                     .setName(name)
                                     .setType(type);
                    tableFieldSchema.add(schemaEntry);
                }
            }
            schema.setFields(tableFieldSchema);
            table.setSchema(schema);
            tableRef.setDatasetId(DATASET_ID);
            tableRef.setProjectId(PROJECT_ID);
            tableRef.setTableId(tableId);
            table.setTableReference(tableRef);
            try {
                bigquery.tables().insert(PROJECT_ID, DATASET_ID, table).execute();
            } catch (GoogleJsonResponseException ex) {
                result = ex.getMessage();
            }
        }

        return result;
    }

    /**
     * Delete table.
     *
     * @param tableId tableid
     * @return
     * @throws IOException Thrown if there is an error connecting to Bigquery.
     * @throws java.lang.InterruptedException
     */
    @Override
    public String deleteTable(final String tableId) throws IOException, InterruptedException {
        String result = "OK";

        try {
            Bigquery bigquery = BigqueryServiceFactory.getService();
            
            bigquery.tables().delete(PROJECT_ID, DATASET_ID, tableId).execute();
        } catch (GoogleJsonResponseException ex) {
            result = ex.getMessage();
        }
        
        return result;
    }

    /**
     * insert rows in exist table.
     *
     * @param tableId tableid
     * @param rows The source of the JSON rows we are streaming in.
     * @return Returns Iterates through the stream responses
     * @throws IOException Thrown if there is an error connecting to Bigquery.
     */
    // [START run]
    @Override
    public Iterator<TableDataInsertAllResponse> insert(final String tableId, final JsonReader rows) throws IOException {
        final Bigquery bigquery = BigqueryServiceFactory.getService();
        final Gson gson = new Gson();
        rows.beginArray();

        return new Iterator<TableDataInsertAllResponse>() {
            /**
             * Check whether there is another row to stream.
             *
             * @return True if there is another row in the stream
             */
            @Override
            public boolean hasNext() {
                try {
                    return rows.hasNext();
                } catch (IOException ex) {
                    logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
                }

                return false;
            }

            /**
             * Insert the next row, and return the response.
             *
             * @return Next page of data
             */
            @Override
            public TableDataInsertAllResponse next() {
                try {
                    Map<String, Object> rowData = gson.<Map<String, Object>>fromJson(
                                        rows,
                                        (new HashMap<String, Object>()).getClass());
                    return streamRow(bigquery,
                                     PROJECT_ID,
                                     DATASET_ID,
                                     tableId,
                                     new TableDataInsertAllRequest.Rows().setJson(rowData));
                } catch (JsonSyntaxException | IOException ex) {
                    logger.log(Level.SEVERE, "{0}", ex.getStackTrace());
                }
                return null;
            }

            @Override
            public void remove() {
                this.next();
            }
        };
    }

    /**
     * @param query
     * @param page
     * @param size
     * @return
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    public List<TableRow> runQuery(
            String query,
            Integer page,
            Long size) throws IOException, InterruptedException {
        List<TableRow> rows = null;
        Bigquery bigquery = BigqueryServiceFactory.getService();
        JobReference jobId = BigqueryUtils.startQuery(bigquery, PROJECT_ID, query);

        // Poll for Query Results, return result output
        TableReference completedJob = BigqueryUtils.checkQueryResults(bigquery, PROJECT_ID, jobId);

        // Return and display the results of the Query Job
        if (completedJob != null && !completedJob.isEmpty()) {
            rows = BigqueryUtils.displayQueryResults(bigquery, page, size, completedJob);
        }

        return rows;
    }

    /**
     * @param query
     * @return
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    public List<TableRow> countQuery(String query) throws IOException, InterruptedException {
        List<TableRow> rows = null;
        Bigquery bigquery = BigqueryServiceFactory.getService();
        JobReference jobId = BigqueryUtils.startQuery(bigquery, PROJECT_ID, query);

        // Poll for Query Results, return result output
        TableReference completedJob = BigqueryUtils.checkQueryResults(bigquery, PROJECT_ID, jobId);

        // Return and display the results of the Query Job
        if (completedJob != null && !completedJob.isEmpty()) {
            rows = BigqueryUtils.displayQueryResults(bigquery, completedJob);
        }

        return rows;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.dao.account;

import com.buzzlogix.dao.GenericDaoImpl;
import com.buzzlogix.entity.Account;
import com.buzzlogix.utils.impl.LoggerImpl;
import com.google.api.client.util.Strings;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.ConsoleHandler;
import java.util.logging.Logger;

/**
 * @author administrator
 */
public class AccountDaoImpl implements AccountDao {

    private static final Logger logger = new LoggerImpl().getLoggerByHandler(AccountDaoImpl.class.getName(), new ConsoleHandler());

    private final GenericDaoImpl<Account> generic = new GenericDaoImpl<>();

    /**
     * @param account
     * @return
     */
    @Override
    public Account upsert(final Account account) {
        Account result = null;

        if (account.getId() == null) {
            result = generic.insert(account);
        } else {
            final Map<String, Object> parameters = new HashMap<String, Object>() {
                {
                    put("id", account.getId());
                }
            };
            result = new AccountDaoImpl().getSingleByQuery("com.buzzlogix.entity.Account.findById", parameters);

            if (result != null) {
                generic.change(result, account);
            }
        }

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @param page
     * @param size
     * @return
     */
    @Override
    public List getListByQuery(String namedQuery, Map<String, Object> parameters, Integer page, Integer size) {
        List<Account> result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.getListByQuery(Account.class, namedQuery, parameters, page, size);
        }

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @return
     */
    @Override
    public Account getSingleByQuery(String namedQuery, Map<String, Object> parameters) {
        Account result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.getSingleByQuery(Account.class, namedQuery, parameters);
        }

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @return
     */
    @Override
    public Integer getCountByQuery(String namedQuery, Map<String, Object> parameters) {
        Integer result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.getCountByQuery(Account.class, namedQuery, parameters);
        }

        return result;
    }

    @Override
    public Integer delete(String namedQuery, Map<String, Object> parameters) {
        Integer result = null;
        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.delete(Account.class, namedQuery, parameters);
        }
        return result;
    }
}

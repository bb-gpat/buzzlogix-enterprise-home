/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.dao;

import com.google.appengine.api.datastore.Key;

/**
 * @author administrator
 */
public interface DatastoreModel {

	/**
	 * @return
	 */
	public Key getKey();

	/**
	 * @return
	 */
	public String getId();

	/**
	 * @param id
	 */
	public void setId(String id);

	/**
	 * @param values
	 * @return
	 */
	public DatastoreModel changeValues(final Object values);
}

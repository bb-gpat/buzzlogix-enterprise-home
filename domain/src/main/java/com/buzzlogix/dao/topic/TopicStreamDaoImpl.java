/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.dao.topic;

import com.buzzlogix.dao.GenericDaoImpl;
import com.buzzlogix.entity.TopicStream;
import com.buzzlogix.enums.StreamStatusEnum;
import com.buzzlogix.utils.impl.LoggerImpl;
import com.google.api.client.util.Strings;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.ConsoleHandler;
import java.util.logging.Logger;

/**
 * @author administrator
 */
public class TopicStreamDaoImpl implements TopicStreamDao {

    private static final Logger logger = new LoggerImpl().getLoggerByHandler(TopicStreamDaoImpl.class.getName(), new ConsoleHandler());

    private final GenericDaoImpl<TopicStream> generic = new GenericDaoImpl<>();

    /**
     * @param topic
     * @return
     */
    @Override
    public TopicStream upsert(final TopicStream topic) {
        TopicStream result = null;

        if (topic.getId() == null) {
            topic.setStatus(StreamStatusEnum.STOPPED);
            result = generic.insert(topic);
        } else {
            final Map<String, Object> parameters = new HashMap<String, Object>() {
                {
                    put("id", topic.getId());
                }
            };
            result = new TopicStreamDaoImpl().getSingleByQuery("com.buzzlogix.entity.TopicStream.findById", parameters);

            if (result != null) {
                generic.change(result, topic);
            }
        }

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @param page
     * @param size
     * @return
     */
    @Override
    public List getListByQuery(String namedQuery, Map<String, Object> parameters, Integer page, Integer size) {
        List<TopicStream> result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.getListByQuery(TopicStream.class, namedQuery, parameters, page, size);
        }

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @return
     */
    @Override
    public TopicStream getSingleByQuery(String namedQuery, Map<String, Object> parameters) {
        TopicStream result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.getSingleByQuery(TopicStream.class, namedQuery, parameters);
        }

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @return
     */
    @Override
    public Integer getCountByQuery(String namedQuery, Map<String, Object> parameters) {
        Integer result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.getCountByQuery(TopicStream.class, namedQuery, parameters);
        }

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @return
     */
    @Override
    public Integer delete(String namedQuery, Map<String, Object> parameters) {
        Integer result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.delete(TopicStream.class, namedQuery, parameters);
        }

        return result;
    }
}

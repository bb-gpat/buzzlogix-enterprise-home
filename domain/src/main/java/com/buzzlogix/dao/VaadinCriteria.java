/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.dao;

import com.buzzlogix.enums.VaadinCompareEnum;
import com.buzzlogix.enums.VaadinFilterEnum;

/**
 * @author administrator
 */
public class VaadinCriteria {

	private VaadinFilterEnum filter;

	private VaadinCompareEnum compare;

	private String property;

	private String value;

	private Boolean sensitive;

	/**
	 * @param filter
	 * @param compare
	 * @param property
	 * @param value
	 * @param sensitive
	 */
	public VaadinCriteria(VaadinFilterEnum filter, VaadinCompareEnum compare, String property, String value, Boolean sensitive) {
		this.filter = filter;
		this.compare = compare;
		this.property = property;
		this.value = value;
		this.sensitive = sensitive;
	}

	/**
	 * @return
	 */
	public VaadinFilterEnum getFilter() {
		return filter;
	}

	/**
	 * @param filter
	 */
	public void setFilter(VaadinFilterEnum filter) {
		this.filter = filter;
	}

	/**
	 * @return
	 */
	public VaadinCompareEnum getCompare() {
		return compare;
	}

	/**
	 * @param compare
	 */
	public void setCompare(VaadinCompareEnum compare) {
		this.compare = compare;
	}

	/**
	 * @return
	 */
	public String getProperty() {
		return property;
	}

	/**
	 * @param property
	 */
	public void setProperty(String property) {
		this.property = property;
	}

	/**
	 * @return
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @return
	 */
	public Boolean getSensitive() {
		return sensitive;
	}

	/**
	 * @param sensitive
	 */
	public void setSensitive(Boolean sensitive) {
		this.sensitive = sensitive;
	}

	@Override
	public String toString() {
		return ((filter != null) ? filter.name() : "") + "," +
				((compare != null) ? compare.name() : "") + "," +
				((property != null) ? property : "") + "," +
				((value != null) ? value.toString() : "") + "," +
				((sensitive != null) ? sensitive.toString() : "");
	}
}

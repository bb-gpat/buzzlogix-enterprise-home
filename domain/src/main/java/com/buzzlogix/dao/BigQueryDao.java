/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.dao;

import com.google.api.services.bigquery.model.TableDataInsertAllResponse;
import com.google.api.services.bigquery.model.TableRow;
import com.google.gson.stream.JsonReader;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author administrator
 */
public interface BigQueryDao {

	/**
	 * @param tableId
	 * @param fields
	 * @return
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public String createTable(
			final String tableId,
			final Map<String, String> fields) throws IOException, InterruptedException;
        
        /**
	 * @param tableId
	 * @return
	 * @throws IOException
	 * @throws InterruptedException
	 */
        public String deleteTable(final String tableId) throws IOException, InterruptedException;

	/**
	 * @param tableId
	 * @param rows
	 * @return
	 * @throws IOException
	 */
	public Iterator<TableDataInsertAllResponse> insert(
			final String tableId,
			final JsonReader rows) throws IOException;

	/**
	 * @param query
	 * @param page
	 * @param size
	 * @return
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public List<TableRow> runQuery(
			String query,
			Integer page,
			Long size) throws IOException, InterruptedException;
	
        /**
	 * @param query
	 * @return
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public List<TableRow> countQuery(String query) throws IOException, InterruptedException;
}

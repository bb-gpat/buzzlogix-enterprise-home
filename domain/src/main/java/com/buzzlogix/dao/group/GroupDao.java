/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.dao.group;

import com.buzzlogix.entity.Group;

import java.util.List;
import java.util.Map;

/**
 * @author administrator
 */
public interface GroupDao {

	/**
	 * @param group
	 * @return
	 */
	public Group upsert(Group group);

	/**
	 * @param namedQuery
	 * @param parameters
	 * @param page
	 * @param size
	 * @return
	 */
	public List getListByQuery(String namedQuery, Map<String, Object> parameters, Integer page, Integer size);

	/**
	 * @param namedQuery
	 * @param parameters
	 * @return
	 */
	public Group getSingleByQuery(String namedQuery, Map<String, Object> parameters);

	/**
	 * @param namedQuery
	 * @param parameters
	 * @return
	 */
	public Group getDeleteSingleByQuery(String namedQuery, Map<String, Object> parameters);


	/**
	 * @param namedQuery
	 * @param parameters
	 * @return
	 */
	public Integer getCountByQuery(String namedQuery, Map<String, Object> parameters);

	/**
	 * @param namedQuery
	 * @param parameters
	 * @return
	 */
	public Integer delete(String namedQuery, Map<String, Object> parameters);
}

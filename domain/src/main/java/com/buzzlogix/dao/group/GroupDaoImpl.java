/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.dao.group;

import com.buzzlogix.dao.GenericDaoImpl;
import com.buzzlogix.entity.Group;
import com.buzzlogix.utils.impl.LoggerImpl;
import com.google.api.client.util.Strings;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.ConsoleHandler;
import java.util.logging.Logger;

/**
 * @author administrator
 */
public class GroupDaoImpl implements GroupDao {

    private final static Logger logger = new LoggerImpl().getLoggerByHandler(GroupDaoImpl.class.getName(), new ConsoleHandler());

    private final GenericDaoImpl<Group> generic = new GenericDaoImpl<>();

    /**
     * @param group
     * @return
     */
    @Override
    public Group upsert(final Group group) {
        Group result = null;

        if (group.getId() == null) {
            result = generic.insert(group);
        } else {
            final Map<String, Object> parameters = new HashMap<String, Object>() {
                {
                    put("id", group.getId());
                }
            };
            result = new GroupDaoImpl().getDeleteSingleByQuery("com.buzzlogix.entity.Group.findById", parameters);

            if (result != null) {
                generic.change(result, group);
            }
        }

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @param page
     * @param size
     * @return
     */
    @Override
    public List getListByQuery(String namedQuery, Map<String, Object> parameters, Integer page, Integer size) {
        List<Group> result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.getListByQuery(Group.class, namedQuery, parameters, page, size);
        }

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @return
     */
    @Override
    public Group getSingleByQuery(String namedQuery, Map<String, Object> parameters) {
        Group result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.getSingleByQuery(Group.class, namedQuery, parameters);
        }

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @return
     */
    @Override
    public Group getDeleteSingleByQuery(String namedQuery, Map<String, Object> parameters) {
        Group result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.getSingleByQuery(Group.class, namedQuery, parameters);
        }

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @return
     */
    @Override
    public Integer getCountByQuery(String namedQuery, Map<String, Object> parameters) {
        Integer result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.getCountByQuery(Group.class, namedQuery, parameters);
        }

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @return
     */
    @Override
    public Integer delete(String namedQuery, Map<String, Object> parameters) {
        Integer result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.delete(Group.class, namedQuery, parameters);
        }

        return result;
    }
}

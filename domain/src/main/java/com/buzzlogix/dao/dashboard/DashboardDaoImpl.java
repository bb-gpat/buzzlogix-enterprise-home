/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.dao.dashboard;

import com.buzzlogix.dao.GenericDaoImpl;
import com.buzzlogix.entity.Dashboard;
import com.google.api.client.util.Strings;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * @author administrator
 */
public class DashboardDaoImpl implements DashboardDao {

    private static final Logger logger = Logger.getLogger(DashboardDaoImpl.class.getName());

    private final GenericDaoImpl<Dashboard> generic = new GenericDaoImpl<>();

    /**
     * @param dashboard
     * @return
     */
    @Override
    public Dashboard upsert(final Dashboard dashboard) {
        Dashboard result = null;

        if (dashboard.getId() == null) {
            result = generic.insert(dashboard);
        } else {
            final Map<String, Object> parameters = new HashMap<String, Object>() {
                {
                    put("id", dashboard.getId());
                }
            };
            result = new DashboardDaoImpl().getSingleByQuery("com.buzzlogix.entity.Dashboard.findById", parameters);

            if (result != null) {
                generic.change(result, dashboard);
            }
        }

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @param page
     * @param size
     * @return
     */
    @Override
    public List getListByQuery(String namedQuery, Map<String, Object> parameters, Integer page, Integer size) {
        List<Dashboard> result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.getListByQuery(Dashboard.class, namedQuery, parameters, page, size);
        }

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @return
     */
    @Override
    public Dashboard getSingleByQuery(String namedQuery, Map<String, Object> parameters) {
        Dashboard result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.getSingleByQuery(Dashboard.class, namedQuery, parameters);
        }

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @return
     */
    @Override
    public Integer getCountByQuery(String namedQuery, Map<String, Object> parameters) {
        Integer result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.getCountByQuery(Dashboard.class, namedQuery, parameters);
        }

        return result;
    }

    /**
     * @param namedQuery
     * @param parameters
     * @return
     */
    @Override
    public Integer delete(String namedQuery, Map<String, Object> parameters) {
        Integer result = null;

        if (!Strings.isNullOrEmpty(namedQuery)) {
            result = generic.delete(Dashboard.class, namedQuery, parameters);
        }

        return result;
    }
}

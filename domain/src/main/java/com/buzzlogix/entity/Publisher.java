/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.entity;

import com.buzzlogix.dao.DatastoreModelImpl;
import com.buzzlogix.enums.PublisherActionEnum;
import com.buzzlogix.enums.PublisherTypeEnum;
import com.google.api.client.util.Strings;
import com.google.appengine.api.datastore.Key;
import com.google.gson.annotations.Expose;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author administrator
 */
@Entity
@Table(name = "Publisher")
@NamedQueries({
		@NamedQuery(name = "com.buzzlogix.entity.Publisher.findById", query = "SELECT p FROM Publisher p WHERE p.id=:id")
})
public class Publisher extends DatastoreModelImpl implements Serializable {

	@Id
	@Column(name = "Key")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Key key;

	@Column(name = "id")
	@Expose
	private String id;

	@Column(name = "action")
	@Enumerated(EnumType.STRING)
	@Expose
	private PublisherActionEnum action;

	@Column(name = "type")
	@Enumerated(EnumType.STRING)
	@Expose
	private PublisherTypeEnum type;

	@Column(name = "typeId")
	@Expose
	private String typeId;

	@Column(name = "senderId")
	@Expose
	private String senderId;

	@Column(name = "receiverId")
	@Expose
	private String receiverId;

	@Column(name = "from")
	@Expose
	private Date from;

	@Column(name = "to")
	@Expose
	private Date to;

	@Column(name = "step")
	@Expose
	private Integer step;

	@Column(name = "birthday")
	@Expose
	private String birthday;

	@Column(name = "message")
	@Expose
	private String message;

	@ManyToOne(optional = true, fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
	@JoinColumn(name = "keyPublishers", referencedColumnName = "Key")
	private Group group;

	private Publisher(Builder builder) {
		id = builder.id;
		action = builder.action;
		type = builder.type;
		typeId = builder.typeId;
		senderId = builder.senderId;
		receiverId = builder.receiverId;
		from = builder.from;
		to = builder.to;
		step = builder.step;
		birthday = builder.birthday;
		message = builder.message;
	}

	private Publisher(Publisher publisher) {
		id = publisher.id;
		action = publisher.action;
		type = publisher.type;
		typeId = publisher.typeId;
		senderId = publisher.senderId;
		receiverId = publisher.receiverId;
		from = publisher.from;
		to = publisher.to;
		step = publisher.step;
		birthday = publisher.birthday;
		message = publisher.message;
	}

	public Publisher() {
	}

	@Override
	public Key getKey() {
		return key;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public void setId(String id) {
		this.id = id;
	}

	public PublisherActionEnum getAction() {
		return action;
	}

	public void setAction(PublisherActionEnum action) {
		this.action = action;
	}

	public PublisherTypeEnum getType() {
		return type;
	}

	public void setType(PublisherTypeEnum type) {
		this.type = type;
	}

	public String getTypeId() {
		return typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	public String getSenderId() {
		return senderId;
	}

	public void setSenderId(String senderId) {
		this.senderId = senderId;
	}

	public String getReceiverId() {
		return receiverId;
	}

	public void setReceiverId(String receiverId) {
		this.receiverId = receiverId;
	}

	public Date getFrom() {
		return from;
	}

	public void setFrom(Date from) {
		this.from = from;
	}

	public Date getTo() {
		return to;
	}

	public void setTo(Date to) {
		this.to = to;
	}

	public Integer getStep() {
		return step;
	}

	public void setStep(Integer step) {
		this.step = step;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public Publisher changeValues(final Object values) {
		if (Publisher.class.isInstance(values)) {
			Publisher p = (Publisher) values;

			if ((p.getAction() != null) && (this.action == null || !this.action.equals(p.getAction()))) {
				this.action = p.getAction();
			}
			if (!Strings.isNullOrEmpty(p.getBirthday()) && (this.birthday == null || !this.birthday.equals(p.getBirthday()))) {
				this.birthday = p.getBirthday();
			}
			if ((p.getFrom() != null) && (this.from == null || !this.from.equals(p.getFrom()))) {
				this.from = p.getFrom();
			}
			if (!Strings.isNullOrEmpty(p.getMessage()) && (this.message == null || !this.message.equals(p.getMessage()))) {
				this.message = p.getMessage();
			}
			if (!Strings.isNullOrEmpty(p.getReceiverId()) && (this.receiverId == null || !this.receiverId.equals(p.getReceiverId()))) {
				this.receiverId = p.getReceiverId();
			}
			if (!Strings.isNullOrEmpty(p.getSenderId()) && (this.senderId == null || !this.senderId.equals(p.getSenderId()))) {
				this.senderId = p.getSenderId();
			}
			if (!Strings.isNullOrEmpty(p.getTypeId()) && (this.typeId == null || !this.typeId.equals(p.getTypeId()))) {
				this.typeId = p.getTypeId();
			}
			if ((p.getStep() != null) && (this.step == null || !this.step.equals(p.getStep()))) {
				this.step = p.getStep();
			}
			if ((p.getTo() != null) && (this.to == null || !this.to.equals(p.getTo()))) {
				this.to = p.getTo();
			}
			if ((p.getType() != null) && (this.type == null || !this.type.equals(p.getType()))) {
				this.type = p.getType();
			}
		}

		return this;
	}

	@Override
	public String toString() {
		return "key:" + key + ",id:" + id + ", action:" + action + ", type:" + type + ", typeId:" + typeId +
				", senderId:" + senderId +
				", from:" + from + ", to:" + to + ", step:" + step + ", birthday:" + birthday + ", message:" + message;
	}

	public static class Builder {

		private String id;

		private PublisherActionEnum action;

		private PublisherTypeEnum type;

		private String typeId;

		private String senderId;

		private String receiverId;

		private Date from;

		private Date to;

		private Integer step;

		private String birthday;

		private String message;

		public Builder setId(String id) {
			this.id = id;

			return this;
		}

		public Builder setAction(PublisherActionEnum action) {
			this.action = action;

			return this;
		}

		public Builder setType(PublisherTypeEnum type) {
			this.type = type;

			return this;
		}

		public Builder setTypeId(String typeId) {
			this.typeId = typeId;

			return this;
		}

		public Builder setSenderId(String senderId) {
			this.senderId = senderId;

			return this;
		}

		public Builder setReceiverId(String receiverId) {
			this.receiverId = receiverId;

			return this;
		}

		public Builder setFrom(Date from) {
			this.from = from;

			return this;
		}

		public Builder setTo(Date to) {
			this.to = to;

			return this;
		}

		public Builder setStep(Integer step) {
			this.step = step;

			return this;
		}

		public Builder setBirthday(String birthday) {
			this.birthday = birthday;

			return this;
		}

		public Builder setMessage(String message) {
			this.message = message;

			return this;
		}

		public Publisher build() {
			return new Publisher(this);
		}

		public Publisher build(Publisher publisher) {
			return new Publisher(publisher);
		}
	}
}

package com.buzzlogix.entity;

import com.buzzlogix.dao.DatastoreModelImpl;
import com.google.appengine.api.datastore.Key;
import com.google.common.base.Strings;
import com.google.gson.annotations.Expose;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Task")
@NamedQueries({
		@NamedQuery(name = "com.buzzlogix.entity.Task.findById", query = "SELECT t FROM Task t WHERE t.id=:id")
})
public class Task extends DatastoreModelImpl implements Serializable {

	@Id
	@Column(name = "Key")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Key key;

	@Column(name = "id")
	@Expose
	private String id;

	@Column(name = "from")
	@Expose
	private String from;

	@Column(name = "fromUserId")
	@Expose
	private String fromUserId;

	@Column(name = "to")
	@Expose
	private String to;

	@Column(name = "toUserId")
	@Expose
	private String toUserId;

	@Column(name = "message")
	@Expose
	private String message;

	@Column(name = "itemLocation")
	@Expose
	private String itemLocation;

	@Column(name = "topicId")
	@Expose
	private String topicId;

	@Column(name = "streamId")
	@Expose
	private String streamId;

	private Task(Builder builder) {
		id = builder.id;
		from = builder.from;
		fromUserId = builder.fromUserId;
		to = builder.to;
		toUserId = builder.toUserId;
		message = builder.message;
		itemLocation = builder.itemLocation;
		topicId = builder.topicId;
		streamId = builder.streamId;
	}

	private Task(Task task) {
		id = task.id;
		from = task.from;
		fromUserId = task.fromUserId;
		to = task.to;
		toUserId = task.toUserId;
		message = task.message;
		itemLocation = task.itemLocation;
		topicId = task.topicId;
		streamId = task.streamId;
	}

	public Task() {
	}

	@Override
	public Key getKey() {
		return this.key;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public void setId(String id) {
		this.id = id;
	}

	public String getFrom() {
		return this.from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getFromUserId() {
		return fromUserId;
	}

	public void setFromUserId(String fromUserId) {
		this.fromUserId = fromUserId;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getToUserId() {
		return toUserId;
	}

	public void setToUserId(String toUserId) {
		this.toUserId = toUserId;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getItemLocation() {
		return this.itemLocation;
	}

	public void setItemLocation(String itemLocation) {
		this.itemLocation = itemLocation;
	}

	@Override
	public String toString() {
		return "key:" + ((key != null) ? key : "") +
				",id:" + (!Strings.isNullOrEmpty(id) ? id : "") +
				", from:" + (!Strings.isNullOrEmpty(from) ? from : "") +
				", fromUserId:" + (!Strings.isNullOrEmpty(fromUserId) ? fromUserId : "") +
				", to:" + (!Strings.isNullOrEmpty(to) ? to : "") +
				", toUserId:" + (!Strings.isNullOrEmpty(toUserId) ? toUserId : "") +
				", message:" + (!Strings.isNullOrEmpty(message) ? message : "") +
				", itemLocation:" + (!Strings.isNullOrEmpty(itemLocation) ? itemLocation : "") +
				", topicId:" + (!Strings.isNullOrEmpty(topicId) ? topicId : "") +
				", streamd:" + (!Strings.isNullOrEmpty(streamId) ? streamId : "");
	}

	public static class Builder {

		private String id;

		private String from;

		private String fromUserId;

		private String to;

		private String toUserId;

		private String message;

		private String itemLocation;

		private String topicId;

		private String streamId;

		public Builder setId(String id) {
			this.id = id;

			return this;
		}

		public Builder setFrom(String from) {
			this.from = from;

			return this;
		}

		public Builder setFromUserId(String fromUserId) {
			this.fromUserId = fromUserId;

			return this;
		}

		public Builder setTo(String to) {
			this.to = to;

			return this;
		}

		public Builder setToUserId(String toUserId) {
			this.toUserId = toUserId;

			return this;
		}

		public Builder setMessage(String message) {
			this.message = message;

			return this;
		}

		public Builder setItemLocation(String itemLocation) {
			this.itemLocation = itemLocation;

			return this;
		}

		public Builder setTopicId(String topicId) {
			this.topicId = topicId;

			return this;
		}

		public Builder setStreamId(String streamId) {
			this.streamId = streamId;

			return this;
		}

		public Task build() {
			return new Task(this);
		}

		public Task build(Task task) {
			return new Task(task);
		}
	}
}

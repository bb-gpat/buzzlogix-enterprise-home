package com.buzzlogix.entity;

import com.buzzlogix.dao.DatastoreModelImpl;
import com.buzzlogix.enums.PlanEnum;
import com.buzzlogix.utils.strings.BuzzlogixStrings;
import com.google.api.client.util.Strings;
import com.google.appengine.api.datastore.Key;
import com.google.gson.annotations.Expose;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "Account")
@NamedQueries({
		@NamedQuery(name = "com.buzzlogix.entity.Account.findById", query = "SELECT a FROM Account a WHERE a.id=:id"),
		@NamedQuery(name = "com.buzzlogix.entity.Account.findByKey", query = "SELECT a FROM Account a WHERE a.key=:key")
})
public class Account extends DatastoreModelImpl implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Key")
	Key key;

	@Column(name = "id")
	@Expose
	private String id;

	@Column(name = "name")
	@Expose
	private String name;

	@Column(name = "plan")
	@Enumerated(EnumType.STRING)
	@Expose
	private PlanEnum plan;

	@OneToOne(optional = true, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "keyBilling")
	@Expose
	private Billing billing;

	@OneToMany(targetEntity = Group.class, fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "account")
	@Expose
	private Set<Group> groups;

	private Account(Builder builder) {
		id = builder.id;
		name = builder.name;
		plan = builder.plan;
		billing = builder.billing;
		groups = builder.groups;
	}

	private Account(Account account) {
		id = account.id;
		name = account.name;
		plan = account.plan;
		billing = account.billing;
		groups = account.groups;
	}

	public Account() {
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public void setId(String id) {
		this.id = id;
	}

	@Override
	public Key getKey() {
		return key;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public PlanEnum getPlan() {
		return this.plan;
	}

	public void setPlan(PlanEnum plan) {
		this.plan = plan;
	}

	public Billing getBilling() {
		return this.billing;
	}

	public void setBilling(Billing billing) {
		this.billing = billing;
	}

	public Set<Group> getGroups() {
		return groups;
	}

	public void setGroups(Set<Group> groups) {
		this.groups = groups;
	}

	@Override
	public Account changeValues(final Object values) {
		if (Account.class.isInstance(values)) {
			Account account = (Account) values;

			if (!Strings.isNullOrEmpty(account.getName()) && (name == null || !name.equals(account.getName()))) {
				name = account.getName();
			}
			if ((account.getPlan() != null) && (plan == null || !plan.equals(account.getPlan()))) {
				plan = account.getPlan();
			}
			if (account.getGroups() != null && !account.getGroups().isEmpty()) {
				Set<Group> grps = account.getGroups();

				for (Group group : grps) {
					if (group.getId() == null) {
						group.setId(UUID.randomUUID().toString());
					}
				}
				if (groups != null) {
					groups.addAll(grps);
				} else {
					groups = grps;
				}
			}
		}

		return this;
	}

	@Override
	public String toString() {
		return "key:" + ((key != null) ? key : "") +
				", id:" + ((!Strings.isNullOrEmpty(id)) ? id : "") +
				", name:" + ((!Strings.isNullOrEmpty(name)) ? name : "") +
				", plan:" + ((plan != null) ? plan : "") +
				", billing:" + ((billing != null) ? billing.toString() : "") +
				", groups:" + ((groups != null) ? BuzzlogixStrings.listToString(groups) : "");
	}

	public static class Builder {

		private String id;

		private String name;

		private PlanEnum plan;

		private Billing billing;

		private Set<Group> groups;

		public Builder setId(String id) {
			this.id = id;

			return this;
		}

		public Builder setName(String name) {
			this.name = name;

			return this;
		}

		public Builder setPlan(PlanEnum plan) {
			this.plan = plan;

			return this;
		}

		public Builder setBilling(Billing billing) {
			this.billing = billing;

			return this;
		}

		public Builder setGroups(Set<Group> groups) {
			this.groups = groups;

			return this;
		}

		public Account build() {
			return new Account(this);
		}

		public Account build(Account account) {
			return new Account(account);
		}
	}
}

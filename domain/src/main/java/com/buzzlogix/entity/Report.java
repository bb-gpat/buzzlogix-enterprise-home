/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.entity;

import com.buzzlogix.dao.DatastoreModelImpl;
import com.google.api.client.util.Strings;
import com.google.appengine.api.datastore.Key;
import com.google.gson.annotations.Expose;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author administrator
 */

@Entity
@Table(name = "Report")
@NamedQueries({
		@NamedQuery(name = "com.buzzlogix.entity.Report.findById", query = "SELECT r FROM Report r WHERE r.id=:id")
})
public class Report extends DatastoreModelImpl implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Key")
	Key key;

	@Column(name = "id")
	@Expose
	private String id;

	@Column(name = "name")
	@Expose
	private String name;

	private Report(Builder builder) {
		name = builder.name;
	}

	private Report(Report report) {
		key = report.key;
		name = report.name;
	}

	public Report() {
	}

	@Override
	public Key getKey() {
		return key;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public Report changeValues(final Object values) {
		if (Report.class.isInstance(values)) {
			Report r = (Report) values;

			if (!Strings.isNullOrEmpty(r.getName()) && (this.name == null || !this.name.equals(r.getName()))) {
				this.name = r.getName();
			}
		}

		return this;
	}

	@Override
	public String toString() {
		return "key:" + key + ",id:" + id + ", name:" + name;
	}

	public static class Builder {

		private String id;

		private String name;

		public Builder setId(String id) {
			this.id = id;

			return this;
		}

		public Builder setName(String name) {
			this.name = name;

			return this;
		}

		public Report build() {
			return new Report(this);
		}

		public Report build(Report report) {
			return new Report(report);
		}
	}
}

package com.buzzlogix.entity;

import com.buzzlogix.dao.DatastoreModelImpl;
import com.google.appengine.api.datastore.Key;
import com.google.gson.annotations.Expose;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Limit")
@NamedQueries({
    @NamedQuery(name = "com.buzzlogix.entity.Limit.findById", query = "SELECT l FROM Limit l WHERE l.id=:id")
})
public class Limit extends DatastoreModelImpl implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Key")
    Key key;

    @Column(name = "id")
    @Expose
    private String id;

    @Column(name = "maxUsers")
    @Expose
    private Integer maxUsers;

    @Column(name = "maxGroups")
    @Expose
    private Integer maxGroups;

    @Column(name = "maxSocials")
    @Expose
    private Integer maxSocials;

    @Column(name = "maxStreams")
    @Expose
    private Integer maxStreams;

    private Limit(Builder builder) {
        id = builder.id;
        maxUsers = builder.maxUsers;
        maxGroups = builder.maxGroups;
        maxSocials = builder.maxSocials;
        maxStreams = builder.maxStreams;
    }

    private Limit(Limit limit) {
        id = limit.id;
        maxUsers = limit.maxUsers;
        maxGroups = limit.maxGroups;
        maxSocials = limit.maxSocials;
        maxStreams = limit.maxStreams;
    }

    public Limit() {
    }

    @Override
    public Key getKey() {
        return key;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    public Integer getMaxUsers() {
        return this.maxUsers;
    }

    public void setMaxUsers(Integer maxUsers) {
        this.maxUsers = maxUsers;
    }

    public Integer getMaxGroups() {
        return this.maxGroups;
    }

    public void setMaxGroups(Integer maxGroups) {
        this.maxGroups = maxGroups;
    }

    public Integer getMaxSocials() {
        return this.maxSocials;
    }

    public void setMaxSocials(Integer maxSocials) {
        this.maxSocials = maxSocials;
    }

    public Integer getMaxStreams() {
        return this.maxStreams;
    }

    public void setMaxStreams(Integer maxStreams) {
        this.maxStreams = maxStreams;
    }

    @Override
    public Limit changeValues(final Object values) {
        if (Limit.class.isInstance(values)) {
            Limit l = (Limit) values;

            if ((l.getMaxGroups() != null) && (this.maxGroups == null || !this.maxGroups.equals(l.getMaxGroups()))) {
                this.maxGroups = l.getMaxGroups();
            }
            if ((l.getMaxSocials() != null) && (this.maxSocials == null || !this.maxSocials.equals(l.getMaxSocials()))) {
                this.maxSocials = l.getMaxSocials();
            }
            if ((l.getMaxStreams() != null) && (this.maxStreams == null || !this.maxStreams.equals(l.getMaxStreams()))) {
                this.maxStreams = l.getMaxStreams();
            }
            if ((l.getMaxUsers() != null) && (this.maxUsers == null || !this.maxUsers.equals(l.getMaxUsers()))) {
                this.maxUsers = l.getMaxUsers();
            }
        }

        return this;
    }

    @Override
    public String toString() {
        return "key:" + key + ",id:" + id + ", maxUsers:" + maxUsers + ", maxGroups:" + maxGroups + ", maxSocials:" + maxSocials
                + ", maxStreams:" + maxStreams;
    }

    public static class Builder {

        private String id;

        private Integer maxUsers;

        private Integer maxGroups;

        private Integer maxSocials;

        private Integer maxStreams;

        public Builder setId(String id) {
            this.id = id;

            return this;
        }

        public Builder setMaxUsers(Integer maxUsers) {
            this.maxUsers = maxUsers;

            return this;
        }

        public Builder setMaxGroups(Integer maxGroups) {
            this.maxGroups = maxGroups;

            return this;
        }

        public Builder setMaxSocials(Integer maxSocials) {
            this.maxSocials = maxSocials;

            return this;
        }

        public Builder setMaxStreams(Integer maxStreams) {
            this.maxStreams = maxStreams;

            return this;
        }

        public Limit build() {
            return new Limit(this);
        }

        public Limit build(Limit limit) {
            return new Limit(limit);
        }
    }
}

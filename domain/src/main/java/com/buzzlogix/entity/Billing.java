package com.buzzlogix.entity;

import com.buzzlogix.dao.DatastoreModelImpl;
import com.google.appengine.api.datastore.Key;
import com.google.gson.annotations.Expose;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Billing")
@NamedQueries({
		@NamedQuery(name = "com.buzzlogix.entity.Billing.findById", query = "SELECT b FROM  Billing b WHERE b.id=:id")
})
public class Billing extends DatastoreModelImpl implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Key")
	Key key;

	@Column(name = "id")
	@Expose
	private String id;

	@OneToOne(optional = true, fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "keyLimit")
	@Expose
	private Limit limit;

	@OneToOne(optional = true, fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "keyLicence")
	@Expose
	private Licence licence;

	private Billing(Builder builder) {
		id = builder.id;
		limit = builder.limit;
		licence = builder.licence;
	}

	private Billing(Billing billing) {
		id = billing.id;
		limit = billing.limit;
		licence = billing.licence;
	}

	public Billing() {
	}

	@Override
	public Key getKey() {
		return key;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public void setId(String id) {
		this.id = id;
	}

	public Licence getLicence() {
		return this.licence;
	}

	public void setLicence(Licence licence) {
		this.licence = licence;
	}

	public Limit getLimit() {
		return this.limit;
	}

	public void setLimit(Limit limit) {
		this.limit = limit;
	}

	@Override
	public Billing changeValues(final Object values) {
		if (Billing.class.isInstance(values)) {
			Billing b = (Billing) values;

			if (b.getLicence() != null && b.getLicence().getKey() != null) {
				licence = b.getLicence();
			}
			if (b.getLimit() != null && b.getLimit().getKey() != null) {
				limit = b.getLimit();
			}
		}

		return this;
	}

	@Override
	public String toString() {
		return "key:" + key + ",id:" + id + ", limit:" + ((limit != null) ? limit.toString() : limit) + ", licence:" +
				((licence != null) ? licence.toString() : licence);
	}

	public static class Builder {

		private String id;

		private Limit limit;

		private Licence licence;

		public Builder setId(String id) {
			this.id = id;

			return this;
		}

		public Builder setLimit(Limit limit) {
			this.limit = limit;

			return this;
		}

		public Builder setLicence(Licence licence) {
			this.licence = licence;

			return this;
		}

		public Billing build() {
			return new Billing(this);
		}

		public Billing build(Billing billing) {
			return new Billing(billing);
		}
	}
}

/**
 * To change this license header, choose License Headers in Project Properties. To change this template file, choose
 * Tools | Templates and open the template in the editor.
 */
package com.buzzlogix.entity;

import com.buzzlogix.dao.DatastoreModelImpl;
import com.buzzlogix.enums.WidgetTypeEnum;
import com.google.api.client.util.Strings;
import com.google.appengine.api.datastore.Key;
import com.google.gson.annotations.Expose;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author administrator
 */
@Entity
@Table(name = "Widget")
@NamedQueries({
		@NamedQuery(name = "com.buzzlogix.entity.Widget.findById", query = "SELECT w FROM Widget w WHERE w.id=:id")
})
public class Widget extends DatastoreModelImpl implements Serializable {

	@Id
	@Column(name = "Key")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Key key;

	@Column(name = "id")
	@Expose
	private String id;

	@Column(name = "type")
	@Enumerated(EnumType.STRING)
	@Expose
	private WidgetTypeEnum type;

	@Column(name = "source")
	@Expose
	private String source;

	@Column(name = "startRange")
	@Expose
	private Date startRange;

	@Column(name = "endRange")
	@Expose
	private Date endRange;

	@Column(name = "width")
	@Expose
	private Integer width;

	@Column(name = "height")
	@Expose
	private Integer height;

	@Column(name = "top")
	@Expose
	private Integer top;

	@Column(name = "left")
	@Expose
	private Integer left;

	@ManyToOne(optional = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "keyDashboard")
	private Dashboard dashboard;

	private Widget(Builder builder) {
		id = builder.id;
		type = builder.type;
		source = builder.source;
		startRange = builder.startRange;
		endRange = builder.endRange;
		width = builder.width;
		height = builder.height;
		top = builder.top;
		left = builder.left;
	}

	private Widget(Widget widget) {
		id = widget.id;
		type = widget.type;
		source = widget.source;
		startRange = widget.startRange;
		endRange = widget.endRange;
		width = widget.width;
		height = widget.height;
		top = widget.top;
		left = widget.left;
	}

	public Widget() {
	}

	@Override
	public Key getKey() {
		return key;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public void setId(String id) {
		this.id = id;
	}

	public WidgetTypeEnum getType() {
		return type;
	}

	public void setType(WidgetTypeEnum type) {
		this.type = type;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Date getStartRange() {
		return startRange;
	}

	public void setStartRange(Date startRange) {
		this.startRange = startRange;
	}

	public Date getEndRange() {
		return endRange;
	}

	public void setEndRange(Date endRange) {
		this.endRange = endRange;
	}

	public Integer getWidth() {
		return width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	public Integer getTop() {
		return top;
	}

	public void setTop(Integer top) {
		this.top = top;
	}

	public Integer getLeft() {
		return left;
	}

	public void setLeft(Integer left) {
		this.left = left;
	}

	@Override
	public Widget changeValues(final Object values) {
		if (Widget.class.isInstance(values)) {
			Widget w = (Widget) values;

			if (w.getType() != null && (this.type == null || !this.type.equals(w.getType()))) {
				this.type = w.getType();
			}
			if (w.getStartRange() != null && (this.startRange == null || !this.startRange.equals(w.getStartRange()))) {
				this.startRange = w.getStartRange();
			}
			if (!Strings.isNullOrEmpty(w.getSource()) && (this.source == null || !this.source.equals(w.getSource()))) {
				this.source = w.getSource();
			}
			if (w.getWidth() != null && (this.width == null || !this.width.equals(w.getWidth()))) {
				this.width = w.getWidth();
			}
			if (w.getHeight() != null && (this.height == null || !this.height.equals(w.getHeight()))) {
				this.height = w.getHeight();
			}
			if (w.getTop() != null && (this.top == null || !this.top.equals(w.getTop()))) {
				this.top = w.getTop();
			}
			if (w.getLeft() != null && (this.left == null || !this.left.equals(w.getLeft()))) {
				this.left = w.getLeft();
			}
		}

		return this;
	}

	@Override
	public String toString() {
		return "key:" + key + ",id:" + id + ", type:" + type + ", source:" + source + ", startRange:" + startRange +
				", endRange:" +
				endRange + ", width:" + width + ", height:" + height + ", top:" + top + ", left:" + left;
	}

	public static class Builder {

		private String id;

		private WidgetTypeEnum type;

		private String source;

		private Date startRange;

		private Date endRange;

		private Integer width;

		private Integer height;

		private Integer top;

		private Integer left;

		public Builder setId(String id) {
			this.id = id;

			return this;
		}

		public Builder setType(WidgetTypeEnum type) {
			this.type = type;

			return this;
		}

		public Builder setSource(String source) {
			this.source = source;

			return this;
		}

		public Builder setStartRange(Date startRange) {
			this.startRange = startRange;

			return this;
		}

		public Builder setEndRange(Date endRange) {
			this.endRange = endRange;

			return this;
		}

		public Builder setWidth(Integer width) {
			this.width = width;

			return this;
		}

		public Builder setHeight(Integer height) {
			this.height = height;

			return this;
		}

		public Builder setTop(Integer top) {
			this.top = top;

			return this;
		}

		public Builder setLeft(Integer left) {
			this.left = left;

			return this;
		}

		public Widget build() {
			return new Widget(this);
		}

		public Widget build(Widget widget) {
			return new Widget(widget);
		}
	}
}

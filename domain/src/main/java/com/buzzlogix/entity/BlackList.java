/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.entity;

import com.buzzlogix.dao.DatastoreModelImpl;
import com.google.api.client.util.Strings;
import com.google.appengine.api.datastore.Key;
import com.google.gson.annotations.Expose;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author administrator
 */
@Entity
@Table(name = "BlackList")
public class BlackList extends DatastoreModelImpl implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Key")
	Key key;

	@Column(name = "name")
	@Expose
	private String name;

	@Column(name = "value")
	@Expose
	private String value;

	private BlackList(Builder builder) {
		name = builder.name;
		value = builder.value;
	}

	private BlackList(BlackList blackList) {
		name = blackList.name;
		value = blackList.value;
	}

	public BlackList() {
	}

	@Override
	public Key getKey() {
		return key;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "key:" + ((key != null) ? key : "") +
				", name:" + ((!Strings.isNullOrEmpty(name)) ? name : "") +
				", value:" + ((!Strings.isNullOrEmpty(value)) ? value : "");
	}

	public static class Builder {

		private String name;

		private String value;

		public Builder setName(String name) {
			this.name = name;

			return this;
		}

		public Builder setValue(String value) {
			this.value = value;

			return this;
		}

		public BlackList build() {
			return new BlackList(this);
		}

		public BlackList build(BlackList blackList) {
			return new BlackList(blackList);
		}
	}
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.entity;

import com.buzzlogix.dao.DatastoreModelImpl;
import com.google.api.client.util.Strings;
import com.google.appengine.api.datastore.Key;
import com.google.gson.annotations.Expose;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author administrator
 */
@Entity
@Table(name = "ModifyGenericPost")
@NamedQueries({
		@NamedQuery(name = "com.buzzlogix.entity.ModifyGenericPost.findById", query = "SELECT mgp FROM ModifyGenericPost mgp WHERE mgp.id=:id"),
		@NamedQuery(name = "com.buzzlogix.entity.ModifyGenericPost.findByKey", query = "SELECT mgp FROM ModifyGenericPost mgp WHERE mgp.key=:key")
})
public class ModifyGenericPost extends DatastoreModelImpl implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Key")
	private Key key;

	@Column(name = "bigQueryName")
	@Expose
	private String bigQueryName;

	@Column(name = "genericPostId")
	@Expose
	private String genericPostId;

	@Column(name = "gender")
	@Expose
	private String gender;

	@Column(name = "sentiment")
	@Expose
	private String sentiment;

	@Column(name = "keywords")
	@Expose
	private String keywords;

	@Column(name = "language")
	@Expose
	private String language;

	@Column(name = "tags")
	@Expose
	private String tags;

	@Column(name = "location")
	@Expose
	private String location;

	private ModifyGenericPost(Builder builder) {
		bigQueryName = builder.bigQueryName;
		genericPostId = builder.genericPostId;
		gender = builder.gender;
		sentiment = builder.sentiment;
		keywords = builder.keywords;
		language = builder.language;
		tags = builder.tags;
		location = builder.location;
	}

	private ModifyGenericPost(ModifyGenericPost modify) {
		bigQueryName = modify.bigQueryName;
		genericPostId = modify.genericPostId;
		gender = modify.gender;
		sentiment = modify.sentiment;
		keywords = modify.keywords;
		language = modify.language;
		tags = modify.tags;
		location = modify.location;
	}

	public ModifyGenericPost() {
	}

	public String getBigQueryName() {
		return bigQueryName;
	}

	public void setBigQueryName(String bigQueryName) {
		this.bigQueryName = bigQueryName;
	}

	public String getGenericPostId() {
		return genericPostId;
	}

	public void setGenericPostId(String genericPostId) {
		this.genericPostId = genericPostId;
	}

	public String getSentiment() {
		return sentiment;
	}

	public void setSentiment(String sentiment) {
		this.sentiment = sentiment;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	@Override
	public ModifyGenericPost changeValues(final Object values) {
		if (ModifyGenericPost.class.isInstance(values)) {
			ModifyGenericPost modify = (ModifyGenericPost) values;

			if (!Strings.isNullOrEmpty(modify.getKeywords()) && (keywords == null || !keywords.equals(modify.getKeywords()))) {
				keywords = modify.getKeywords();
			}
			if ((modify.getLanguage() != null) && (language == null || !language.equals(modify.getLanguage()))) {
				language = modify.getLanguage();
			}
			if ((modify.getLocation() != null) && (location == null || !location.equals(modify.getLocation()))) {
				location = modify.getLocation();
			}
			if ((modify.getSentiment() != null) && (sentiment == null || !sentiment.equals(modify.getSentiment()))) {
				sentiment = modify.getSentiment();
			}
			if ((modify.getTags() != null) && (tags == null || !tags.equals(modify.getTags()))) {
				tags = modify.getTags();
			}
		}

		return this;
	}

	@Override
	public String toString() {
		return "key:" + ((key != null) ? key : "") +
				", bigQueryName:" + ((!Strings.isNullOrEmpty(bigQueryName)) ? bigQueryName : "") +
				", genericPostId:" + ((!Strings.isNullOrEmpty(genericPostId)) ? genericPostId : "") +
				", gender:" + ((!Strings.isNullOrEmpty(gender)) ? gender : "") +
				", keywords:" + ((!Strings.isNullOrEmpty(keywords)) ? keywords : "") +
				", language:" + ((!Strings.isNullOrEmpty(language)) ? language : "") +
				", location:" + ((!Strings.isNullOrEmpty(location)) ? location : "") +
				", sentiment:" + ((!Strings.isNullOrEmpty(sentiment)) ? sentiment : "") +
				", tags:" + ((!Strings.isNullOrEmpty(tags)) ? tags : "");
	}

	public static class Builder {

		private String bigQueryName;

		private String genericPostId;

		private String gender;

		private String sentiment;

		private String keywords;

		private String language;

		private String tags;

		private String location;

		public Builder setBigQueryName(String bigQueryName) {
			this.bigQueryName = bigQueryName;

			return this;
		}

		public Builder setGenericPostId(String genericPostId) {
			this.genericPostId = genericPostId;

			return this;
		}

		public Builder setGender(String gender) {
			this.gender = gender;

			return this;
		}

		public Builder setSentiment(String sentiment) {
			this.sentiment = sentiment;

			return this;
		}

		public Builder setKeywords(String keywords) {
			this.keywords = keywords;

			return this;
		}

		public Builder setLanguage(String language) {
			this.language = language;

			return this;
		}

		public Builder setTags(String tags) {
			this.tags = tags;

			return this;
		}

		public Builder setLocation(String location) {
			this.location = location;

			return this;
		}

		public ModifyGenericPost build() {
			return new ModifyGenericPost(this);
		}

		public ModifyGenericPost build(ModifyGenericPost account) {
			return new ModifyGenericPost(account);
		}
	}
}

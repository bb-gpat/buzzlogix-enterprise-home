package com.buzzlogix.entity;

import com.buzzlogix.dao.DatastoreModelImpl;
import com.google.api.client.util.Strings;
import com.google.appengine.api.datastore.Key;
import com.google.gson.annotations.Expose;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by gp on 22/4/2016.
 */
@Entity
@Table(name = "Location")
@NamedQueries({
    @NamedQuery(name = "com.buzzlogix.entity.Location.findAll", query = "SELECT l FROM Location l ORDER BY l.name")
})
public class Location extends DatastoreModelImpl implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Key")
    Key key;

    @Column(name = "id")
    @Expose
    private String id;

    @Column(name = "name")
    @Expose
    private String name;
    
    @Column(name = "timezone")
    @Expose
    private String timezone;

    @Column(name = "codeCountry")
    @Expose
    private String codeCountry;

    @Column(name = "latitude")
    @Expose
    private String latitude;

    @Column(name = "longitude")
    @Expose
    private String longitude;

    @Column(name = "utcOffset")
    @Expose
    private String utcOffset;

    @Column(name = "utcDestOffset")
    @Expose
    private String utcDestOffset;

    @Column(name = "notes")
    @Expose
    private String notes;

    @Column(name = "comments")
    @Expose
    private String comments;

    private Location(Builder builder) {
        id = builder.id;
        name = builder.name;
        timezone = builder.timezone;
        codeCountry = builder.codeCountry;
        latitude = builder.latitude;
        longitude = builder.longitude;
        utcOffset = builder.utcOffset;
        utcDestOffset = builder.utcDestOffset;
        notes = builder.notes;
        comments = builder.comments;
    }

    private Location(Location location) {
        id = location.id;
        name = location.name;
        timezone = location.timezone;
        codeCountry = location.codeCountry;
        latitude = location.latitude;
        longitude = location.longitude;
        utcOffset = location.utcOffset;
        utcDestOffset = location.utcDestOffset;
        notes = location.notes;
        comments = location.comments;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getCodeCountry() {
        return codeCountry;
    }

    public void setCodeCountry(String codeCountry) {
        this.codeCountry = codeCountry;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getUtcOffset() {
        return utcOffset;
    }

    public void setUtcOffset(String utcOffset) {
        this.utcOffset = utcOffset;
    }

    public String getUtcDestOffset() {
        return utcDestOffset;
    }

    public void setUtcDestOffset(String utcDestOffset) {
        this.utcDestOffset = utcDestOffset;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    @Override
    public String toString() {
        return "key:" + ((key != null) ? key : "")
                + ", id:" + ((!Strings.isNullOrEmpty(id)) ? id : "")
                + ", name:" + ((!Strings.isNullOrEmpty(name)) ? name : "")
                + ", timezone:" + ((!Strings.isNullOrEmpty(timezone)) ? timezone : "")
                + ", codeCountry:" + ((!Strings.isNullOrEmpty(codeCountry)) ? codeCountry : "")
                + ", latitude:" + ((!Strings.isNullOrEmpty(latitude)) ? latitude : "")
                + ", longitude:" + ((!Strings.isNullOrEmpty(longitude)) ? longitude : "")
                + ", utcOffset:" + ((!Strings.isNullOrEmpty(utcOffset)) ? utcOffset : "")
                + ", utcDestOffset:" + ((!Strings.isNullOrEmpty(utcDestOffset)) ? utcDestOffset : "")
                + ", notes:" + ((!Strings.isNullOrEmpty(notes)) ? notes : "")
                + ", comments:" + ((!Strings.isNullOrEmpty(comments)) ? comments : "");

    }

    public static class Builder {

        private String id;

        private String name;
        
        private String timezone;

        private String codeCountry;

        private String latitude;

        private String longitude;

        private String utcOffset;

        private String utcDestOffset;

        private String notes;

        private String comments;

        public Builder setId(String id) {
            this.id = id;

            return this;
        }

        public Builder setName(String name) {
            this.name = name;

            return this;
        }
        
        public Builder setTimezone(String timezone) {
            this.timezone = timezone;

            return this;
        }

        public Builder setCodeCountry(String codeCountry) {
            this.codeCountry = codeCountry;

            return this;
        }

        public Builder setLatitude(String latitude) {
            this.latitude = latitude;

            return this;
        }

        public Builder setLongitude(String longitude) {
            this.longitude = longitude;

            return this;
        }

        public Builder setUtcOffset(String utcOffset) {
            this.utcOffset = utcOffset;

            return this;
        }

        public Builder setUtcDestOffset(String utcDestOffset) {
            this.utcDestOffset = utcDestOffset;

            return this;
        }

        public Builder setNotes(String notes) {
            this.notes = notes;

            return this;
        }

        public Builder setComments(String comments) {
            this.comments = comments;

            return this;
        }

        public Location build() {
            return new Location(this);
        }

        public Location build(Location location) {
            return new Location(location);
        }
    }
}

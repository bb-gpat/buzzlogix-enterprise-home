package com.buzzlogix.entity;

import com.buzzlogix.dao.DatastoreModelImpl;
import com.buzzlogix.enums.StreamStatusEnum;
import com.google.api.client.util.Strings;
import com.google.appengine.api.datastore.Key;
import com.google.gson.annotations.Expose;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "TopicStream")
@NamedQueries({
    @NamedQuery(name = "com.buzzlogix.entity.TopicStream.findById", query = "SELECT ts FROM TopicStream ts WHERE ts.id=:id"),
    @NamedQuery(name = "com.buzzlogix.entity.TopicStream.findAll", query = "SELECT ts FROM TopicStream ts ")
})
public class TopicStream extends DatastoreModelImpl implements Serializable {

    @Id
    @Column(name = "Key")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Key key;

    @Column(name = "id")
    @Expose
    private String id;

    @Column(name = "streamName")
    @Expose
    private String streamName;

    @Column(name = "createdBy")
    @Expose
    private String createdBy;

    @Column(name = "updatedBy")
    @Expose
    private String updatedBy;

    @Column(name = "dateCreated")
    @Expose
    private Date dateCreated;

    @Column(name = "dateUpdated")
    @Expose
    private Date dateUpdated;

    @Column(name = "status")
    @Expose
    private StreamStatusEnum status;

    @OneToOne(optional = true, fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "keyTopicConfig")
    @Expose
    private TopicStreamConfig topicConfig;

    @OneToOne(optional = true, fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "keyBlackList")
    @Expose
    private BlackList blackList;

    @OneToOne(optional = true, fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "keyTag")
    @Expose
    private Tag tag;

    @ManyToOne(optional = true, fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name = "keyTopics", referencedColumnName = "Key")
    private Group group;

    private TopicStream(Builder builder) {
        id = builder.id;
        streamName = builder.streamName;
        createdBy = builder.createdBy;
        updatedBy = builder.updatedBy;
        dateCreated = builder.dateCreated;
        dateUpdated = builder.dateUpdated;
        status = builder.status;
        topicConfig = builder.topicConfig;
        blackList = builder.blackList;
    }

    private TopicStream(TopicStream topic) {
        id = topic.id;
        streamName = topic.streamName;
        createdBy = topic.createdBy;
        updatedBy = topic.updatedBy;
        dateCreated = topic.dateCreated;
        dateUpdated = topic.dateUpdated;
        status = topic.status;
        topicConfig = topic.topicConfig;
        blackList = topic.blackList;
    }

    public TopicStream() {
    }

    @Override
    public Key getKey() {
        return key;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    public String getStreamName() {
        return streamName;
    }

    public void setStreamName(String streamName) {
        this.streamName = streamName;
    }

    public TopicStreamConfig getTopicConfig() {
        return topicConfig;
    }

    public void setTopicConfig(TopicStreamConfig topicConfig) {
        this.topicConfig = topicConfig;
    }

    public BlackList getBlackList() {
        return blackList;
    }

    public void setBlackList(BlackList blackList) {
        this.blackList = blackList;
    }

    public Tag getTag() {
        return tag;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public StreamStatusEnum getStatus() {
        return status;
    }

    public void setStatus(StreamStatusEnum status) {
        this.status = status;
    }

    @Override
    public TopicStream changeValues(final Object values) {
        if (TopicStream.class.isInstance(values)) {
            TopicStream t = (TopicStream) values;

            if (!Strings.isNullOrEmpty(t.getStreamName()) && (streamName == null || !streamName.equals(t.getStreamName()))) {
                streamName = t.getStreamName();
            }

            if (!Strings.isNullOrEmpty(t.getCreatedBy()) && (createdBy == null || !createdBy.equals(t.getCreatedBy()))) {
                createdBy = t.getCreatedBy();
            }

            if (t.getDateCreated() != null && (dateCreated == null || !dateCreated.equals(t.getDateCreated()))) {
                dateCreated = t.getDateCreated();
            }

            if (t.getDateUpdated() != null && (dateUpdated == null || !dateUpdated.equals(t.getDateUpdated()))) {
                dateUpdated = t.getDateUpdated();
            }
            if (!Strings.isNullOrEmpty(t.getUpdatedBy()) && (updatedBy == null || !updatedBy.equals(t.getUpdatedBy()))) {
                updatedBy = t.getUpdatedBy();
            }
            if (t.getStatus() != null && (status == null || !status.equals(t.getStatus()))) {
                status = t.getStatus();
            }
            if (t.getTopicConfig() != null && t.getTopicConfig().getKey() != null) {
                topicConfig = t.getTopicConfig();
            }
            if (t.getBlackList() != null && t.getBlackList().getKey() != null) {
                blackList = t.getBlackList();
            }
            if (t.getTag() != null && t.getTag().getKey() != null) {
                tag = t.getTag();
            }
        }

        return this;
    }

    @Override
    public String toString() {
        return "key:" + key
                + ",id:" + id
                + ", streamName:" + ((!Strings.isNullOrEmpty(streamName)) ? streamName : "")
                + ", createdBy:" + ((createdBy != null) ? createdBy.toString() : "")
                + ", updatedBy:" + ((updatedBy != null) ? updatedBy.toString() : "")
                + ", dateCreated:" + ((!Strings.isNullOrEmpty(streamName)) ? dateCreated : "")
                + ", dateUpdated:" + ((dateUpdated != null) ? dateUpdated.toString() : "")
                + ", updatedBy:" + ((!Strings.isNullOrEmpty(updatedBy)) ? updatedBy : "")
                + ", topicConfig:" + ((topicConfig != null) ? topicConfig.toString() : "")
                + ", blackList:" + ((blackList != null) ? blackList.toString() : "")
                + ", tag:" + ((tag != null) ? tag.toString() : "");
    }

    public static class Builder {

        private String id;

        private String streamName;

        private String createdBy;

        private String updatedBy;

        private Date dateCreated;

        private Date dateUpdated;

        private StreamStatusEnum status;

        private TopicStreamConfig topicConfig;

        private BlackList blackList;

        private Tag tag;

        public Builder setId(String id) {
            this.id = id;

            return this;
        }

        public Builder setStreamName(String streamName) {
            this.streamName = streamName;

            return this;
        }

        public Builder setTopicConfig(TopicStreamConfig topicConfig) {
            this.topicConfig = topicConfig;

            return this;
        }

        public Builder setBlackList(BlackList blackList) {
            this.blackList = blackList;

            return this;
        }

        public Builder seTag(Tag tag) {
            this.tag = tag;

            return this;
        }

        public Builder setDateCreated(Date dateCreated) {
            this.dateCreated = dateCreated;

            return this;
        }

        public Builder setDateUpdated(Date dateUpdated) {
            this.dateUpdated = dateUpdated;

            return this;
        }

        public Builder setUpdatedBy(String updatedBy) {
            this.updatedBy = updatedBy;

            return this;
        }

        public Builder setStastus(StreamStatusEnum status) {
            this.status = status;

            return this;
        }

        public TopicStream build() {
            return new TopicStream(this);
        }

        public TopicStream build(TopicStream topic) {
            return new TopicStream(topic);
        }
    }
}

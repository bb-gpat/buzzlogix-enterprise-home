/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.entity;

import com.buzzlogix.dao.DatastoreModelImpl;
import com.buzzlogix.utils.strings.BuzzlogixStrings;
import com.google.api.client.util.Strings;
import com.google.appengine.api.datastore.Key;
import com.google.gson.annotations.Expose;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author administrator
 */
@Entity
@Table(name = "Tag")
public class Tag extends DatastoreModelImpl implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Key")
	Key key;

	@Column(name = "name")
	@Expose
	private String name;

	@Column(name = "value")
	@Expose
	private String[] value;

	private Tag(Builder builder) {
		name = builder.name;
		value = builder.value;
	}

	private Tag(Tag tag) {
		name = tag.name;
		value = tag.value;
	}

	public Tag() {
	}

	@Override
	public Key getKey() {
		return key;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String[] getValue() {
		return this.value;
	}

	public void setValue(String[] value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "key:" + ((key != null) ? key : "") +
				", name:" + ((!Strings.isNullOrEmpty(name)) ? name : "") +
				", value:" + ((value != null) ? BuzzlogixStrings.listToString(value) : "");
	}

	public static class Builder {

		private String name;

		private String[] value;

		public Builder setName(String name) {
			this.name = name;

			return this;
		}

		public Builder setValue(String[] value) {
			this.value = value;

			return this;
		}

		public Tag build() {
			return new Tag(this);
		}

		public Tag build(Tag tag) {
			return new Tag(tag);
		}
	}
}

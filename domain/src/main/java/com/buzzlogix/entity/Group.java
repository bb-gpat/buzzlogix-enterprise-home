package com.buzzlogix.entity;

import com.buzzlogix.dao.DatastoreModelImpl;
import com.buzzlogix.utils.impl.LoggerImpl;
import com.buzzlogix.utils.strings.BuzzlogixStrings;
import com.google.api.client.util.Strings;
import com.google.appengine.api.datastore.Key;
import com.google.gson.annotations.Expose;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;
import java.util.logging.ConsoleHandler;
import java.util.logging.Logger;

@Entity
@Table(name = "Group")
@NamedQueries({
		@NamedQuery(name = "com.buzzlogix.entity.Group.findById", query = "SELECT g FROM Group g WHERE g.id=:id"),
		@NamedQuery(name = "com.buzzlogix.entity.Group.findByKeys", query = "SELECT g FROM Group g WHERE g.key IN(:keys)"),
		@NamedQuery(name = "com.buzzlogix.entity.Group.findByName", query = "SELECT g FROM Group g WHERE g.name = :name")
})
public class Group extends DatastoreModelImpl implements Serializable {

	private final static Logger logger = new LoggerImpl().getLoggerByHandler(Group.class.getName(), new ConsoleHandler());

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Key")
	Key key;

	@Column(name = "id")
	@Expose
	private String id;

	@Column(name = "name")
	@Expose
	private String name;

	@ManyToOne(optional = true, fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
	@JoinColumn(name = "keyGroups", referencedColumnName = "Key")
	private Account account;

	@OneToMany(targetEntity = User.class, fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST}, mappedBy = "group")
	@Expose
	private Set<User> users;

	@OneToMany(targetEntity = Channel.class, fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST}, mappedBy = "group")
	@Expose
	private Set<Channel> channels;

	@OneToMany(targetEntity = TopicStream.class, fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST}, mappedBy = "group")
	@Expose
	private Set<TopicStream> topics;

	@OneToMany(targetEntity = Publisher.class, fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST}, mappedBy = "group")
	@Expose
	private Set<Publisher> publishers;

	@OneToMany(targetEntity = Dashboard.class, fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST}, mappedBy = "group")
	@Expose
	private Set<Dashboard> dashboards;

	private Group(Builder builder) {
		id = builder.id;
		name = builder.name;
		users = builder.users;
		channels = builder.channels;
		topics = builder.topics;
		publishers = builder.publishers;
		dashboards = builder.dashboards;
	}

	private Group(Group group) {
		id = group.id;
		name = group.name;
		users = group.users;
		channels = group.channels;
		topics = group.topics;
		publishers = group.publishers;
		dashboards = group.dashboards;
	}

	public Group() {
	}

	@Override
	public Key getKey() {
		return key;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<User> getUsers() {
		return users;
	}

	public void setUser(Set<User> users) {
		this.users = users;
	}

	public Set<Channel> getChannels() {
		return this.channels;
	}

	public void setChannels(Set<Channel> channels) {
		this.channels = channels;
	}

	public Set<TopicStream> getTopics() {
		return this.topics;
	}

	public void setTopics(Set<TopicStream> topics) {
		this.topics = topics;
	}

	public Set<Publisher> getPublishers() {
		return publishers;
	}

	public void setPublishers(Set<Publisher> publishers) {
		this.publishers = publishers;
	}

	public Set<Dashboard> getDashboards() {
		return dashboards;
	}

	public void setDashboards(Set<Dashboard> dashboards) {
		this.dashboards = dashboards;
	}

	@Override
	public Group changeValues(final Object values) {
		if (Group.class.isInstance(values)) {
			Group g = (Group) values;

			if (!Strings.isNullOrEmpty(g.getName()) && (name == null || !name.equals(g.getName()))) {
				name = g.getName();
			}
			if (g.getChannels() != null && !g.getChannels().isEmpty()) {
				if (channels != null) {
					channels.addAll(g.getChannels());
				} else {
					channels = g.getChannels();
				}
			}
			if (g.getDashboards() != null && !g.getDashboards().isEmpty()) {
				if (dashboards != null) {
					dashboards.addAll(g.getDashboards());
				} else {
					dashboards = g.getDashboards();
				}
			}
			if (g.getPublishers() != null && !g.getPublishers().isEmpty()) {
				if (publishers != null) {
					publishers.addAll(g.getPublishers());
				} else {
					publishers = g.getPublishers();
				}
			}
			if (g.getTopics() != null && !g.getTopics().isEmpty()) {
				if (topics != null) {
					topics.addAll(g.getTopics());
				} else {
					topics = g.getTopics();
				}
			}
			if (g.getUsers() != null && !g.getUsers().isEmpty()) {
				if (users != null) {
					users.addAll(g.getUsers());
				} else {
					users = g.getUsers();
				}
			}
		}

		return this;
	}

	@Override
	public String toString() {
		return "key:" + key + ",id:" + id + ",name:" + name +
				", users:" + ((users != null) ? BuzzlogixStrings.listToString(users) : "") +
				", channels:" + ((channels != null) ? BuzzlogixStrings.listToString(channels) : "") +
				", topics:" + ((topics != null) ? BuzzlogixStrings.listToString(topics) : "") +
				", publishers:" + ((publishers != null) ? BuzzlogixStrings.listToString(publishers) : "") +
				", dashboards:" + ((dashboards != null) ? BuzzlogixStrings.listToString(dashboards) : "");
	}

	public static class Builder {

		private String id;

		private String name;

		private Set<User> users;

		private Set<Channel> channels;

		private Set<TopicStream> topics;

		private Set<Publisher> publishers;

		private Set<Dashboard> dashboards;

		public Builder setId(String id) {
			this.id = id;

			return this;
		}

		public Builder setName(String name) {
			this.name = name;

			return this;
		}

		public Builder setUsers(Set<User> users) {
			this.users = users;

			return this;
		}

		public Builder setChannels(Set<Channel> channels) {
			this.channels = channels;

			return this;
		}

		public Builder setTopics(Set<TopicStream> topics) {
			this.topics = topics;

			return this;
		}

		public Builder setPublishers(Set<Publisher> publishers) {
			this.publishers = publishers;

			return this;
		}

		public Builder setDashboards(Set<Dashboard> dashboards) {
			this.dashboards = dashboards;

			return this;
		}

		public Group build() {
			return new Group(this);
		}

		public Group build(Group group) {
			return new Group(group);
		}
	}
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buzzlogix.entity.bigquery;

import java.io.Serializable;
import java.util.Map;

/**
 *
 * @author administrator
 */
public class Counters implements Serializable {
    Long total;
    
    Map<String, Long> source;
    
    Map<String, Long> sentiment;
    
    Map<String, Long> language;
    
    Map<String, Long> tags;

    public Counters() {
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public Map<String, Long> getSource() {
        return source;
    }

    public void setSource(Map<String, Long> source) {
        this.source = source;
    }

    public Map<String, Long> getSentiment() {
        return sentiment;
    }

    public void setSentiment(Map<String, Long> sentiment) {
        this.sentiment = sentiment;
    }

    public Map<String, Long> getLanguage() {
        return language;
    }

    public void setLanguage(Map<String, Long> language) {
        this.language = language;
    }

    public Map<String, Long> getTags() {
        return tags;
    }

    public void setTags(Map<String, Long> tags) {
        this.tags = tags;
    }
}

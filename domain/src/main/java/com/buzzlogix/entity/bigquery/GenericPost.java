package com.buzzlogix.entity.bigquery;

import com.buzzlogix.utils.impl.LoggerImpl;
import com.google.api.client.util.Strings;
import com.google.api.services.bigquery.model.TableCell;
import com.google.api.services.bigquery.model.TableRow;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GenericPost implements Serializable {
    
    private static final Logger logger = new LoggerImpl().getLoggerByHandler(GenericPost.class.getName(), new ConsoleHandler());

    private String id;

    private String streamId;

    private String source;

    private String url;

    private String title;

    private String author;

    private String text;

    private String language;

    private String externalLinks;

    private String location;

    private String published;

    private String added;

    private String updated;

    private String sentiment;

    private String keywords;

    private String gender;

    private String tags;

    private String spamScore;

    private String performanceScore;

    private String imgMain;

    private String imgProfile;

    private String extras;

    public GenericPost() {
    }

    public GenericPost(TableRow row) throws IOException {
        List<TableCell> cells = row.getF();

        if (cells != null && !cells.isEmpty()) {
            String[] complex_values = new String[cells.size()];
            Integer i = 0;
            String[] schema = {"sentiment:", "imgMain:", "streamId:", "keywords:", "gender:", "imgProfile:", "added:",
                "author:", "extras:", "language:", "source:", "published:", "title:", "performanceScore:", "url:", "tags:",
                "spamScore:", "externalLinks:", "location:", "id:", "text:", "updated:"
            };
            String[] values = new String[schema.length];

            for (TableCell cell : cells) {
                complex_values[i++] = (cell.getV() instanceof String) ? (String) cell.getV() : null;
            }

            i = 0;
            for (String field : schema) {
                for (String complex_value : complex_values) {
                    if (!Strings.isNullOrEmpty(complex_value) && complex_value.startsWith(field)) {
                        values[i] = complex_value.substring(field.length());
                        break;
                    }
                }
                i++;
            }

            sentiment = values[0];
            imgMain = values[1];
            streamId = values[2];
            keywords = values[3];
            gender = values[4];
            imgProfile = values[5];
            added = values[6];
            author = values[7];
            extras = values[8];
            language = values[9];
            source = values[10];
            published = values[11];
            title = values[12];
            performanceScore = values[13];
            url = values[14];
            tags = values[15];
            spamScore = values[16];
            externalLinks = values[17];
            location = values[18];
            id = values[19];
            text = values[20];
            updated = values[21];
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStreamId() {
        return streamId;
    }

    public void setStreamId(String streamId) {
        this.streamId = streamId;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getExternalLinks() {
        return externalLinks;
    }

    public void setExternalLinks(String externalLinks) {
        this.externalLinks = externalLinks;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPublished() {
        return published;
    }

    public void setPublished(String published) {
        this.published = published;
    }

    public String getAdded() {
        return added;
    }

    public void setAdded(String added) {
        this.added = added;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getSentiment() {
        return sentiment;
    }

    public void setSentiment(String sentiment) {
        this.sentiment = sentiment;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getSpamScore() {
        return spamScore;
    }

    public void setSpamScore(String spamScore) {
        this.spamScore = spamScore;
    }

    public String getPerformanceScore() {
        return performanceScore;
    }

    public void setPerformanceScore(String performanceScore) {
        this.performanceScore = performanceScore;
    }

    public String getImgMain() {
        return this.imgMain;
    }

    public void setImgMain(String imgMain) {
        this.imgMain = imgMain;
    }

    public String getUserImage() {
        return null;
    }

    public String getUserName() {
        return null;
    }

    public String getImgProfile() {
        return imgProfile;
    }

    public void setImgProfile(String imgProfile) {
        this.imgProfile = imgProfile;
    }

    public String getExtras() {
        return extras;
    }

    public void setExtras(String extras) {
        this.extras = extras;
    }

    @Override
    public String toString() {
        return " id:" + ((!Strings.isNullOrEmpty(id)) ? id : "")
                + ", streamId:" + ((!Strings.isNullOrEmpty(streamId)) ? streamId : "")
                + ", source:" + ((!Strings.isNullOrEmpty(source)) ? source : "")
                + ", url:" + ((!Strings.isNullOrEmpty(url)) ? url : "")
                + ", title:" + ((!Strings.isNullOrEmpty(title)) ? url : "")
                + ", author:" + ((!Strings.isNullOrEmpty(author)) ? author : "")
                + ", text:" + ((!Strings.isNullOrEmpty(text)) ? text : "")
                + ", language:" + ((!Strings.isNullOrEmpty(language)) ? language : "")
                + ", externalLinks:" + ((!Strings.isNullOrEmpty(externalLinks)) ? externalLinks : "")
                + ", location:" + ((!Strings.isNullOrEmpty(location)) ? location : "")
                + ", published:" + ((!Strings.isNullOrEmpty(published)) ? published : "")
                + ", added:" + ((!Strings.isNullOrEmpty(added)) ? added : "")
                + ", updated:" + ((!Strings.isNullOrEmpty(updated)) ? updated : "")
                + ", sentiment:" + ((!Strings.isNullOrEmpty(sentiment)) ? sentiment : "")
                + ", keywords:" + ((!Strings.isNullOrEmpty(keywords)) ? keywords : "")
                + ", gender:" + ((!Strings.isNullOrEmpty(gender)) ? gender : "")
                + ", tags:" + ((!Strings.isNullOrEmpty(tags)) ? tags : "")
                + ", spamScore:" + ((!Strings.isNullOrEmpty(spamScore)) ? spamScore : "")
                + ", performanceScore:" + ((!Strings.isNullOrEmpty(performanceScore)) ? performanceScore : "")
                + ", imgMain:" + ((!Strings.isNullOrEmpty(imgMain)) ? imgMain : "")
                + ", imgProfile:" + ((!Strings.isNullOrEmpty(imgProfile)) ? imgProfile : "")
                + ", extras:" + ((!Strings.isNullOrEmpty(extras)) ? extras : "");
    }
}

package com.buzzlogix.entity;

import com.buzzlogix.dao.DatastoreModelImpl;
import com.buzzlogix.enums.ChannelTypeEnum;
import com.google.api.client.util.Strings;
import com.google.appengine.api.datastore.Key;
import com.google.gson.annotations.Expose;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Channel")
@NamedQueries({
		@NamedQuery(name = "com.buzzlogix.entity.Channel.findById", query = "SELECT c FROM Channel c WHERE c.id=:id")
})
public class Channel extends DatastoreModelImpl implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Key")
	Key key;

	@Column(name = "id")
	@Expose
	private String id;

	@Column(name = "type")
	@Enumerated(EnumType.STRING)
	@Expose
	private ChannelTypeEnum type;

	@Column(name = "authToken")
	@Expose
	private String authToken;

	@Column(name = "screenName")
	@Expose
	private String screenName;

	@Column(name = "jsonCriteria")
	@Expose
	private String jsonCriteria;

	@ManyToOne(optional = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "keyChannels", referencedColumnName = "Key")
	private Group group;

	private Channel(Builder builder) {
		id = builder.id;
		type = builder.type;
		authToken = builder.authToken;
		screenName = builder.screenName;
		jsonCriteria = builder.jsonCriteria;
	}

	private Channel(Channel channel) {
		id = channel.id;
		type = channel.type;
		authToken = channel.authToken;
		screenName = channel.screenName;
		jsonCriteria = channel.jsonCriteria;
	}

	public Channel() {
	}

	@Override
	public Key getKey() {
		return key;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public void setId(String id) {
		this.id = id;
	}

	public ChannelTypeEnum getType() {
		return this.type;
	}

	public void setType(ChannelTypeEnum type) {
		this.type = type;
	}

	public String getAuthToken() {
		return this.authToken;
	}

	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

	public String getScreenName() {
		return this.screenName;
	}

	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}

	public String getJsonCriteria() {
		return this.jsonCriteria;
	}

	public void setJsonCriteria(String jsonCriteria) {
		this.jsonCriteria = jsonCriteria;
	}

	@Override
	public Channel changeValues(final Object values) {
		if (Channel.class.isInstance(values)) {
			Channel c = (Channel) values;

			if (c.getType() != null && (this.type == null || !this.type.equals(c.getType()))) {
				this.type = c.getType();
			}
			if (!Strings.isNullOrEmpty(c.getAuthToken()) && (this.authToken == null || !this.authToken.equals(c.getAuthToken()))) {
				this.authToken = c.getAuthToken();
			}
			if (!Strings.isNullOrEmpty(c.getScreenName()) && (this.screenName == null || !this.screenName.equals(c.getScreenName()))) {
				this.screenName = c.getScreenName();
			}
			if (!Strings.isNullOrEmpty(c.getJsonCriteria()) && (this.jsonCriteria == null || !this.jsonCriteria.equals(c.getJsonCriteria()))) {
				this.jsonCriteria = c.getJsonCriteria();
			}
		}

		return this;
	}

	@Override
	public String toString() {
		return "key:" + key + ",id:" + id + ", type:" + type + ", authToken:" + authToken + ", screenName:" + screenName +
				", jsonCriteria:" + jsonCriteria;
	}

	public static class Builder {

		private String id;

		private ChannelTypeEnum type;

		private String authToken;

		private String screenName;

		private String jsonCriteria;

		public Builder setId(String id) {
			this.id = id;

			return this;
		}

		public Builder setType(ChannelTypeEnum type) {
			this.type = type;

			return this;
		}

		public Builder setAuthToken(String authToken) {
			this.authToken = authToken;

			return this;
		}

		public Builder setScreenName(String screenName) {
			this.screenName = screenName;

			return this;
		}

		public Builder setJsonCriteria(String jsonCriteria) {
			this.jsonCriteria = jsonCriteria;

			return this;
		}

		public Channel build() {
			return new Channel(this);
		}

		public Channel build(Channel channel) {
			return new Channel(channel);
		}
	}
}

package com.buzzlogix.entity;

import com.buzzlogix.dao.DatastoreModelImpl;
import com.buzzlogix.utils.strings.BuzzlogixStrings;
import com.google.api.client.util.Strings;
import com.google.appengine.api.datastore.Key;
import com.google.gson.annotations.Expose;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @author administrator
 */
@Entity
@Table(name = "Dashboard")
@NamedQueries({
		@NamedQuery(name = "com.buzzlogix.entity.Dashboard.findById", query = "SELECT d FROM Dashboard d WHERE d.id=:id")
})
public class Dashboard extends DatastoreModelImpl implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Key")
	Key key;

	@Column(name = "name")
	@Expose
	private String name;

	@Column(name = "id")
	@Expose
	private String id;

	@OneToOne(optional = true, fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "keyReport")
	@Expose
	private Report report;

	@OneToMany(targetEntity = Widget.class, fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "dashboard")
	@Expose
	private Set<Widget> widgets;

	@ManyToOne(optional = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "keyDashboards", referencedColumnName = "Key")
	private Group group;

	private Dashboard(Builder builder) {
		id = builder.id;
		name = builder.name;
		report = builder.report;
		widgets = builder.widgets;
	}

	private Dashboard(Dashboard dashboard) {
		id = dashboard.id;
		name = dashboard.name;
		report = dashboard.report;
		widgets = dashboard.widgets;
	}

	public Dashboard() {
	}

	@Override
	public Key getKey() {
		return key;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Report getReport() {
		return report;
	}

	public void setReport(Report report) {
		this.report = report;
	}

	public Set<Widget> getWidgets() {
		return widgets;
	}

	public void setWidgets(Set<Widget> widgets) {
		this.widgets = widgets;
	}

	@Override
	public Dashboard changeValues(final Object values) {
		if (Dashboard.class.isInstance(values)) {
			Dashboard d = (Dashboard) values;

			if (!Strings.isNullOrEmpty(d.getName()) && (name == null || !name.equals(d.getName()))) {
				name = d.getName();
			}
			if (d.getReport() != null && d.getReport() != null) {
				report = d.getReport();
			}
			if ((d.getWidgets() != null) && !d.getWidgets().isEmpty()) {
				if (widgets != null) {
					widgets.addAll(d.getWidgets());
				} else {
					widgets = d.getWidgets();
				}
			}
		}

		return this;
	}

	@Override
	public String toString() {
		return "key:" + key + ", name:" + name +
				",report:" + ((report != null) ? report.toString() : "") +
				",widgets:" + ((widgets != null) ? BuzzlogixStrings.listToString(widgets) : "");
	}

	public static class Builder {

		private String id;

		private String name;

		private Report report;

		private Set<Widget> widgets;

		public Builder setId(String id) {
			this.id = id;

			return this;
		}

		public Builder setName(String name) {
			this.name = name;

			return this;
		}

		public Builder setReport(Report report) {
			this.report = report;

			return this;
		}

		public Builder setWidgets(Set<Widget> widgets) {
			this.widgets = widgets;

			return this;
		}

		public Dashboard build() {
			return new Dashboard(this);
		}

		public Dashboard build(Dashboard dashboard) {
			return new Dashboard(dashboard);
		}
	}
}

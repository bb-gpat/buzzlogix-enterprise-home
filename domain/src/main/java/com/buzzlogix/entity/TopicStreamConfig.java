package com.buzzlogix.entity;

import com.buzzlogix.dao.DatastoreModelImpl;
import com.buzzlogix.enums.SourceTypeEnum;
import com.buzzlogix.utils.locale.LocaleHelper;
import com.buzzlogix.utils.strings.BuzzlogixStrings;
import com.google.appengine.api.datastore.Key;
import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.gson.annotations.Expose;
import com.webhose.domain.WebhoseQuery;
import com.webhose.domain.WebhoseQuery.SiteType;
import twitter4j.FilterQuery;
import twitter4j.Query;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "TopicStreamConfig")
@NamedQueries({
    @NamedQuery(name = "com.buzzlogix.entity.TopicStreamConfig.findById",
            query = "SELECT tsf FROM TopicStreamConfig tsf WHERE tsf.id=:id")
})
public class TopicStreamConfig extends DatastoreModelImpl implements Serializable {

    @Id
    @Column(name = "Key")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Key key;

    @Column(name = "sources")
    @Expose
    private List<SourceTypeEnum> sources;

    @Column(name = "allTerms")
    @Expose
    private List<String> allTerms;

    @Column(name = "someTerms")
    @Expose
    private List<String> someTerms;

    @Column(name = "phrase")
    @Expose
    private String phrase;

    @Column(name = "exclude")
    @Expose
    private String exclude;

    @Column(name = "languages")
    @Expose
    private List<String> languages;

    @Column(name = "locations")
    @Expose
    private List<String> locations;
    
    @Column(name = "poolTime")
    @Expose
    private BigDecimal poolTime;

    @Transient
    private WebhoseQuery queryWebhose;

    @Transient
    private Object queryTwitter;

    @Transient
    private Object queryYoutube;

    @Transient
    private Object queryInstagram;

    @Transient
    private Object queryFacebook;

    private TopicStreamConfig(Builder builder) {
        this.sources = builder.sources;
        this.allTerms = builder.allTerms;
        this.someTerms = builder.someTerms;
        this.phrase = builder.phrase;
        this.exclude = builder.exclude;
        this.languages = builder.languages;
        this.locations = builder.locations;
        this.queryWebhose = builder.queryWebhose;
        this.queryTwitter = builder.queryTwitter;
        this.queryYoutube = builder.queryYoutube;
        this.queryInstagram = builder.queryInstagram;
        this.queryFacebook = builder.queryFacebook;
    }

    public TopicStreamConfig() {
    }

    @Override
    public Key getKey() {
        return key;
    }

    public List<SourceTypeEnum> getSources() {
        return sources;
    }

    public void setSources(List<SourceTypeEnum> sources) {
        this.sources = sources;
    }

    public List<String> getAllTerms() {
        return allTerms;
    }

    public void setAllTerms(List<String> allTerms) {
        this.allTerms = allTerms;
    }

    public List<String> getSomeTerms() {
        return someTerms;
    }

    public void setSomeTerms(List<String> someTerms) {
        this.someTerms = someTerms;
    }

    public List<String> getLanguages() {
        return languages;
    }

    public void setLanguages(List<String> languages) {
        this.languages = languages;
    }

    public List<String> getLocations() {
        return locations;
    }

    public void setLocations(List<String> locations) {
        this.locations = locations;
    }

    public String getPhrase() {
        return phrase;
    }

    public void setPhrase(String phrase) {
        this.phrase = phrase;
    }

    public String getExclude() {
        return exclude;
    }

    public void setExclude(String exclude) {
        this.exclude = exclude;
    }

    public BigDecimal getPoolTime() {
        return poolTime;
    }

    public void setPoolTime(BigDecimal poolTime) {
        this.poolTime = poolTime;
    }

    public WebhoseQuery getQueryWebhose() {
        return queryWebhose;
    }

    public void setQueryWebhose(WebhoseQuery queryWebhose) {
        this.queryWebhose = queryWebhose;
    }

    public Object getQueryTwitter() {
        return queryTwitter;
    }

    public void setQueryTwitter(Object queryTwitter) {
        this.queryTwitter = queryTwitter;
    }

    public Object getQueryYoutube() {
        return queryYoutube;
    }

    public void setQueryYoutube(Object queryYoutube) {
        this.queryYoutube = queryYoutube;
    }

    public Object getQueryInstagram() {
        return queryInstagram;
    }

    public void setQueryInstagram(Object queryInstagram) {
        this.queryInstagram = queryInstagram;
    }

    public Object getQueryFacebook() {
        return queryFacebook;
    }

    public void setQueryFacebook(Object queryFacebook) {
        this.queryFacebook = queryFacebook;
    }

    @Override
    public TopicStreamConfig changeValues(final Object values) {
        if (TopicStreamConfig.class.isInstance(values)) {
            TopicStreamConfig t = (TopicStreamConfig) values;

            if (t.getSources() != null && !t.getSources().isEmpty()) {
                if (sources != null) {
                    sources.addAll(t.getSources());
                } else {
                    sources = t.getSources();
                }
            }
            if (t.getLanguages() != null && !t.getLanguages().isEmpty()) {
                if (languages != null) {
                    languages.addAll(t.getLanguages());
                } else {
                    languages = t.getLanguages();
                }
            }
            if (t.getLocations() != null && !t.getLocations().isEmpty()) {
                if (locations != null) {
                    locations.addAll(t.getLocations());
                } else {
                    locations = t.getLocations();
                }
            }
            if (t.getSomeTerms() != null && !t.getSomeTerms().isEmpty()) {
                if (someTerms != null) {
                    someTerms.addAll(t.getSomeTerms());
                } else {
                    someTerms = t.getSomeTerms();
                }
            }
            if (t.getAllTerms() != null && !t.getAllTerms().isEmpty()) {
                if (allTerms != null) {
                    allTerms.addAll(t.getAllTerms());
                } else {
                    allTerms = t.getAllTerms();
                }
            }
            if (!Strings.isNullOrEmpty(t.getPhrase()) && (phrase == null || !phrase.equals(t.getPhrase()))) {
                phrase = t.getPhrase();
            }
            if (!Strings.isNullOrEmpty(t.getExclude()) && (exclude == null || !exclude.equals(t.getExclude()))) {
                exclude = t.getExclude();
            }
            if (t.getPoolTime() != null && (poolTime == null || !poolTime.equals(t.getPoolTime()))) {
                poolTime = t.getPoolTime();
            }
        }

        return this;
    }

    @Override
    public String toString() {
        return "key:" + key
                + ", sources:" + ((sources != null) ? BuzzlogixStrings.listToString(sources) : "")
                + ", allTerms:" + ((allTerms != null) ? BuzzlogixStrings.listToString(allTerms) : "")
                + ", someTerms:" + ((someTerms != null) ? BuzzlogixStrings.listToString(someTerms) : "")
                + ", phrase:" + phrase
                + ", exclude:" + exclude
                + ", languages:" + ((languages != null) ? BuzzlogixStrings.listToString(languages) : "")
                + ", locations:" + ((locations != null) ? BuzzlogixStrings.listToString(locations) : "")
                + ", poolTime:" + ((poolTime != null) ? poolTime : "")
                + ", queryWebhose:" + ((queryWebhose != null) ? queryWebhose.toString() : "")
                + ", queryTwitter:" + ((queryTwitter != null) ? queryTwitter.toString() : "")
                + ", queryFacebook:" + ((queryFacebook != null) ? queryFacebook.toString() : "")
                + ", queryInstagram:" + ((queryInstagram != null) ? queryInstagram.toString() : "");
    }

    public WebhoseQuery toWebhoseQuery() {
        WebhoseQuery query = new WebhoseQuery();

        if (allTerms != null && !allTerms.isEmpty()) {
            query.allTerms.addAll(allTerms);
        }
        if (someTerms != null && !someTerms.isEmpty()) {
            query.someTerms.addAll(someTerms);
        }

        if (!Strings.isNullOrEmpty(phrase)) {
            query.phrase = phrase;
        }

        if (!Strings.isNullOrEmpty(exclude)) {
            query.exclude = exclude;
        }

        if (languages != null && !languages.isEmpty()) {
            //Webhose needs languages as English full strings, not codes so need to convert.
            for (String language : languages) {
                query.language.add(LocaleHelper.getEnglishDisplayLanguage(language));
            }

        }
        if (locations != null && !locations.isEmpty()) {
            query.locations.addAll(locations);
        }

        if (sources.contains(SourceTypeEnum.NEWS)) {
            query.siteTypes.add(SiteType.news);
        }

        if (sources.contains(SourceTypeEnum.BLOGS)) {
            query.siteTypes.add(SiteType.blogs);
        }

        if (sources.contains(SourceTypeEnum.DISCUSSIONS)) {
            query.siteTypes.add(SiteType.discussions);
        }

        return query;
    }

    public FilterQuery toTwitterStreamQuery() {
        FilterQuery result = new FilterQuery();
        String[] values = null;

        if (allTerms != null && !allTerms.isEmpty()) {
            values = BuzzlogixStrings.listToArray(allTerms);
            
            if (values != null && values.length > 0) {
                result.track(values);
            }
        }
        if (languages != null && !languages.isEmpty()) {
            values = BuzzlogixStrings.listToArray(languages);
            
            if (values != null && values.length > 0) {
                result.language(values);
            }
        }

        return result;
    }

    public String toTwitterQueryString() {
        List<String> terms = new ArrayList<>();

        if (allTerms != null) {
            addTerm(terms, allTerms, "AND", null);
        }
        if (someTerms != null) {
            addTerm(terms, someTerms, "OR", null);
        }
        if (phrase != null) {
            terms.add("\"" + phrase + "\"");
        }
        if (exclude != null) {
            terms.add("-(" + exclude + ")");
        }

        if (languages != null && !languages.isEmpty()) {
            addTerm(terms, languages, "OR", "language");
        }

        if (locations != null && !locations.isEmpty()) {
            addTerm(terms, locations, "OR", "location");
        }

        return Joiner.on(" AND ").join(terms);
    }

    public Query toTwitterQuery() {
        return new Query(toTwitterQueryString());
    }

    public Object toYoutubeQuery() {
        return null;
    }

    public Object toInstagramQuery() {
        return null;
    }

    public Object toFacebookQuery() {
        return null;
    }

    public boolean isWebHose() {
        if (sources.contains(SourceTypeEnum.NEWS)
                || sources.contains(SourceTypeEnum.BLOGS)
                || sources.contains(SourceTypeEnum.DISCUSSIONS)) {
            return true;
        } else {
            return false;
        }

    }

    public boolean isTwitter() {
        return sources.contains(SourceTypeEnum.TWITTER);
    }

    private void addTerm(List<String> terms, List<?> parts, String boolOp, String fieldName) {
        if (parts.isEmpty()) {
            return;
        }

        StringBuilder sb = new StringBuilder();
        sb.append("(");
        boolean first = true;
        for (Object part : parts) {
            if (first) {
                first = false;
            } else {
                sb.append(" ").append(boolOp).append(" ");
            }
            if (fieldName != null) {
                sb.append(fieldName).append(":");
            }
            if (part.toString().contains(" ")) {
                sb.append('"').append(part).append('"');
            } else {
                sb.append(part);
            }
        }
        sb.append(")");
        terms.add(sb.toString());
    }

    public static class Builder {

        private List<SourceTypeEnum> sources;

        private List<String> allTerms;

        private List<String> someTerms;

        private String phrase;

        private String exclude;

        private List<String> languages;

        private List<String> locations;
        
        private BigDecimal poolTime;

        private WebhoseQuery queryWebhose;

        private Object queryTwitter;

        private Object queryYoutube;

        private Object queryInstagram;

        private Object queryFacebook;

        public Builder setSources(List<SourceTypeEnum> sources) {
            this.sources = sources;

            return this;
        }

        public Builder setAllTerms(List<String> allTerms) {
            this.allTerms = allTerms;

            return this;
        }

        public Builder setSomeTerms(List<String> someTerms) {
            this.someTerms = someTerms;

            return this;
        }

        public Builder setPhrase(String phrase) {
            this.phrase = phrase;

            return this;
        }

        public Builder setExclude(String exclude) {
            this.exclude = exclude;

            return this;
        }

        public Builder setLanguages(List<String> languages) {
            this.languages = languages;

            return this;
        }

        public Builder setLocations(List<String> locations) {
            this.locations = locations;

            return this;
        }
        
        public Builder setPoolTime(BigDecimal poolTime) {
            this.poolTime = poolTime;

            return this;
        }

        public Builder setQueryWebhose(WebhoseQuery queryWebhose) {
            this.queryWebhose = queryWebhose;

            return this;
        }

        public Builder setQueryTwitter(Object queryTwitter) {
            this.queryTwitter = queryTwitter;

            return this;
        }

        public Builder setQueryYoutube(Object queryYoutube) {
            this.queryYoutube = queryYoutube;

            return this;
        }

        public Builder setQueryInstagram(Object queryInstagram) {
            this.queryInstagram = queryInstagram;

            return this;
        }

        public Builder setQueryFacebook(Object queryFacebook) {
            this.queryFacebook = queryFacebook;

            return this;
        }

        public TopicStreamConfig build() {
            return new TopicStreamConfig(this);
        }
    }
}

package com.buzzlogix.entity;

import com.buzzlogix.dao.DatastoreModelImpl;
import com.buzzlogix.enums.UserRoleEnum;
import com.buzzlogix.utils.impl.Security;
import com.google.api.client.util.Strings;
import com.google.appengine.api.datastore.Key;
import com.google.gson.annotations.Expose;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author administrator
 */
@Entity
@Table(name = "User")
@NamedQueries({
    @NamedQuery(name = "com.buzzlogix.entity.User.findUserByCredentials", query = "SELECT u FROM User u WHERE u.username=:username AND u.password=:password"),
    @NamedQuery(name = "com.buzzlogix.entity.User.findUserUsernameEmail", query = "SELECT u FROM User u WHERE u.username=:username AND u.email=:email"),
    @NamedQuery(name = "com.buzzlogix.entity.User.findUserByEmail", query = "SELECT u FROM User u WHERE u.email=:email"),
    @NamedQuery(name = "com.buzzlogix.entity.User.findUserByUsername", query = "SELECT u FROM User u WHERE u.username=:username"),
    @NamedQuery(name = "com.buzzlogix.entity.User.findByKeys", query = "SELECT u FROM User u WHERE u.key IN(:keys)"),
    @NamedQuery(name = "com.buzzlogix.entity.User.findById", query = "SELECT u FROM User u WHERE u.id=:id")
})
public class User extends DatastoreModelImpl implements Serializable {

    @Id
    @Column(name = "Key")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Key key;

    @Column(name = "id")
    @Expose
    private String id;

    @Column(name = "username")
    @Expose
    private String username;

    @Column(name = "password")
    @Expose
    private String password;

    @Column(name = "email")
    @Expose
    private String email;

    @Column(name = "name")
    @Expose
    private String name;

    @Column(name = "surname")
    @Expose
    private String surname;

    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    @Expose
    private UserRoleEnum role;

    @Column(name = "title")
    @Expose
    private String title;

    @Column(name = "translationLanguage")
    @Expose
    private String translationLanguage;

    @Column(name = "timeZone")
    @Expose
    private String timeZone;

    @Column(name = "appLanguage")
    @Expose
    private String appLanguage;

    @OneToOne(optional = true, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "keyAccount")
    @Expose
    private Account account;

    @Column(name = "accountId")
    @Expose
    private String accountId;

    @ManyToOne(optional = true, fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "keyUsers", referencedColumnName = "Key")
    private Group group;

    private User(Builder builder) {
        key = builder.key;
        id = builder.id;
        username = builder.username;
        password = builder.password;
        email = builder.email;
        name = builder.name;
        surname = builder.surname;
        role = builder.role;
        title = builder.title;
        translationLanguage = builder.translationLanguage;
        timeZone = builder.timeZone;
        appLanguage = builder.appLanguage;
        account = builder.account;
        accountId = builder.accountId;
    }

    private User(User user) {
        key = user.key;
        id = user.id;
        username = user.username;
        password = user.password;
        email = user.email;
        name = user.name;
        surname = user.surname;
        role = user.role;
        title = user.title;
        translationLanguage = user.translationLanguage;
        timeZone = user.timeZone;
        appLanguage = user.appLanguage;
        account = user.account;
        accountId = user.accountId;
    }

    public User() {
    }

    @Override
    public Key getKey() {
        return key;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return this.surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public UserRoleEnum getRole() {
        return this.role;
    }

    public void setRole(UserRoleEnum role) {
        this.role = role;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTranslationLanguage() {
        return translationLanguage;
    }

    public void setTranslationLanguage(String translationLanguage) {
        this.translationLanguage = translationLanguage;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getAppLanguage() {
        return appLanguage;
    }

    public void setAppLanguage(String appLanguage) {
        this.appLanguage = appLanguage;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    @Override
    public User changeValues(final Object values) {
        if (User.class.isInstance(values)) {
            User user = (User) values;

            if (!Strings.isNullOrEmpty(user.getAppLanguage()) && !appLanguage.equals(user.getAppLanguage())) {
                appLanguage = user.getAppLanguage();
            }
            if (!Strings.isNullOrEmpty(user.getEmail()) && !email.equals(user.getEmail())) {
                email = user.getEmail();
            }
            if (!Strings.isNullOrEmpty(user.getName()) && !name.equals(user.getName())) {
                name = user.getName();
            }
            if (!Strings.isNullOrEmpty(user.getSurname()) && !surname.equals(user.getSurname())) {
                surname = user.getSurname();
            }
            if (!Strings.isNullOrEmpty(user.getTimeZone()) && !timeZone.equals(user.getTimeZone())) {
                timeZone = user.getTimeZone();
            }
            if (!Strings.isNullOrEmpty(user.getTitle()) && !title.equals(user.getTitle())) {
                title = user.getTitle();
            }
            if (!Strings.isNullOrEmpty(user.getTranslationLanguage()) && !translationLanguage.equals(user.getTranslationLanguage())) {
                translationLanguage = user.getTranslationLanguage();
            }
            if (!Strings.isNullOrEmpty(user.getUsername()) && !username.equals(user.getUsername())) {
                username = user.getUsername();
            }
            if (user.getRole() != null && !role.equals(user.getRole())) {
                role = user.getRole();
            }
            if (!Strings.isNullOrEmpty(user.getPassword()) && !password.equals(Security.encryption(Security.SHA256, user.getPassword()))) {
                password = Security.encryption(Security.SHA256, user.getPassword());
            }
            if (user.getAccount() != null && user.getAccount().getKey() != null) {
                account = user.getAccount();
            }
        }

        return this;
    }

    @Override
    public String toString() {
        return "id:" + ((!Strings.isNullOrEmpty(id)) ? id : "")
                + ", username:" + ((!Strings.isNullOrEmpty(username)) ? username : "")
                + ", password:" + ((!Strings.isNullOrEmpty(password)) ? password : "")
                + ", email:" + ((!Strings.isNullOrEmpty(email)) ? email : "")
                + ", name:" + ((!Strings.isNullOrEmpty(name)) ? name : "")
                + ", surname:" + ((!Strings.isNullOrEmpty(surname)) ? surname : "")
                + ", role:" + ((!Strings.isNullOrEmpty(role.name())) ? role : "")
                + ", title:" + ((!Strings.isNullOrEmpty(title)) ? title : "")
                + ", transLang:" + ((!Strings.isNullOrEmpty(translationLanguage)) ? translationLanguage : "")
                + ", timeZone:" + ((!Strings.isNullOrEmpty(timeZone)) ? timeZone : "")
                + ", appLang:" + ((!Strings.isNullOrEmpty(appLanguage)) ? appLanguage : "")
                + ", account:" + ((account != null) ? account.toString() : "")
                + ", accountId:" + ((!Strings.isNullOrEmpty(accountId)) ? accountId : "");
    }

    public static class Builder {

        private Key key;

        private String id;

        private String username;

        private String password;

        private String email;

        private String name;

        private String surname;

        private UserRoleEnum role;

        private String title;

        private String translationLanguage;

        private String timeZone;

        private String appLanguage;

        private Account account;
        
        private String accountId;

        public Builder setKey(Key key) {
            this.key = key;

            return this;
        }

        public Builder setId(String id) {
            this.id = id;

            return this;
        }

        public Builder setUsername(String username) {
            this.username = username;

            return this;
        }

        public Builder setPassword(String password) {
            this.username = password;

            return this;
        }

        public Builder setEmail(String email) {
            this.email = email;

            return this;
        }

        public Builder setName(String name) {
            this.name = name;

            return this;
        }

        public Builder setSurname(String surname) {
            this.surname = surname;

            return this;
        }

        public Builder setRole(UserRoleEnum role) {
            this.role = role;

            return this;
        }

        public Builder setTitle(String title) {
            this.title = title;

            return this;
        }

        public Builder setTranslationLanguage(String translationLanguage) {
            this.translationLanguage = translationLanguage;

            return this;
        }

        public Builder setTimeZone(String timeZone) {
            this.timeZone = timeZone;

            return this;
        }

        public Builder setAppLanguage(String appLanguage) {
            this.appLanguage = appLanguage;

            return this;
        }

        public Builder setAccount(Account account) {
            this.account = account;

            return this;
        }
        
        public Builder setAccountId(String accountId) {
            this.accountId = accountId;

            return this;
        }

        public User build() {
            return new User(this);
        }

        public User build(User user) {
            return new User(user);
        }
    }
}

package com.buzzlogix.entity;

import com.buzzlogix.dao.DatastoreModelImpl;
import com.google.appengine.api.datastore.Key;
import com.google.gson.annotations.Expose;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "Licence")
@NamedQueries({
		@NamedQuery(name = "com.buzzlogix.entity.Licence.findById", query = "SELECT l FROM Licence l WHERE l.id=:id"),
		@NamedQuery(name = "com.buzzlogix.entity.Licence.all", query = "SELECT l.started FROM Licence l"),
		@NamedQuery(name = "com.buzzlogix.entity.Licence.findByDate", query = "SELECT l FROM Licence l WHERE started=:started")
})
public class Licence extends DatastoreModelImpl implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Key")
	Key key;

	@Column(name = "id")
	@Expose
	private String id;

	@Column(name = "started")
	@Expose
	private Date started;

	@Column(name = "expires")
	@Expose
	private Date expires;

	private Licence(Builder builder) {
		started = builder.started;
		expires = builder.expires;
	}

	private Licence(Licence licence) {
		key = licence.key;
		started = licence.started;
		expires = licence.expires;
	}

	public Licence() {
	}

	@Override
	public Key getKey() {
		return key;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public void setId(String id) {
		this.id = id;
	}

	public Date getStarted() {
		return this.started;
	}

	public void setStarted(Date started) {
		this.started = started;
	}

	public Date getExpires() {
		return this.expires;
	}

	public void setExpires(Date expires) {
		this.expires = expires;
	}

	@Override
	public Licence changeValues(final Object values) {
		if (Licence.class.isInstance(values)) {
			Licence l = (Licence) values;

			if ((l.getStarted() != null) && (this.started == null || !this.started.equals(l.getStarted()))) {
				this.started = l.getStarted();
			}
			if ((l.getExpires() != null) && (this.expires == null || !this.expires.equals(l.getExpires()))) {
				this.expires = l.getExpires();
			}
		}

		return this;
	}

	@Override
	public String toString() {
		return "key:" + key + ",id:" + id + ", started:" + started + ", expired:" + expires;
	}

	public static class Builder {

		private String id;

		private Date started;

		private Date expires;

		public Builder setId(String id) {
			this.id = id;

			return this;
		}

		public Builder setStarted(Date started) {
			this.started = started;

			return this;
		}

		public Builder setExpires(Date expires) {
			this.expires = expires;

			return this;
		}

		public Licence build() {
			return new Licence(this);
		}

		public Licence build(Licence licence) {
			return new Licence(licence);
		}
	}
}
